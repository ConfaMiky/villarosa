#ifndef _BAR_CHART_H_

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <qwt_plot_directpainter.h>
#include <qwt_painter.h>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

class QwtPlotMultiBarChart;
class QwtPlotCurve;

class BarChart: public QwtPlot
{
    Q_OBJECT

public:
    BarChart( QWidget *, QString fileName_tmp, int indexColumn, QString titolo_tmp, QString xLabel, QString yLabel );
    BarChart( QWidget *, QString fileName_tmp, int indexColumn, int indexColumn1, QString titolo_tmp, QString xLabel, QString yLabel );
    BarChart( QWidget * = NULL );
    void populate(QwtPlotMultiBarChart *d_barChartItem_tmp);
    void repopulate(QString fileName_tmp, int indexColumn, QString titolo_tmp, QString xLabel, QString yLabel);
    void repopulate(QString fileName_tmp, int indexColumn,int indexColumn1, QString titolo_tmp, QString xLabel, QString yLabel);
    ~BarChart();
public Q_SLOTS:
    void setMode( int );
    void setOrientation( int );
    void exportChart();

private:
    QFile fileInput;

    QwtPlotMultiBarChart *d_barChartItem;
};

#endif

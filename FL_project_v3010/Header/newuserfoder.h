#ifndef NEWUSERFOLDER_H
#define NEWUSERFOLDER_H

//#include <QtGui>
#include <QtWidgets>
#include <QDebug>
#include <QWidget>
#include <QDialog>
//#include <QLayout>
//#include <QDateEdit>
//#include <QLabel>
//#include <QLineEdit>
//#include <QPushButton>
class QLabel;
class QLineEdit;
class QCheckBox;
class QPushButton;
class QSpinBox;
class QGroupBox;
class QAction;

class NewUserFolder : public QDialog
{
    Q_OBJECT
public:
    explicit NewUserFolder(QWidget *parent = 0);

protected:
     void closeEvent(QCloseEvent *event);
     void updateEvent();

signals:
    void closeFolder();
    void updateFolder ( QString newPath );

public slots:

private slots:
    int createFolder();

private:
    QLabel *nameLabel;
    QLineEdit *nameInput;
    QLabel *surnameLabel;
    QLineEdit *surnameInput;
    QLabel *diseaseLabel;
    QLineEdit *diseaseInput;
    QLabel *dateLabel;
    QDateEdit *dateEdit;

    QPushButton *saveButton;
    QPushButton *closeButton;

private:
    void dataIncomplete();
    void userAlreadyExist();

};

#endif // NEWUSERFOLDER_H

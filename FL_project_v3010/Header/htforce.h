#ifndef HTFORCE_H
#define HTFORCE_H


#include <QGLWidget>
#include <GL/glu.h>
#include <math.h>
#include <QTime>
#include <QFile>
 #include <QTextStream>
#include <QMessageBox>
//#include "Salvataggio_Dati_Continuous_Motion_Task.hh"
#include "STR.h"
#include <string>
#include <iostream>
#include <sstream>
#include "target.h"
#include "textureImage.h"


using namespace std;

#define M_PI 3.14159265358979323846

//extern char savedPathDir [1000];

class HtForce : public QGLWidget {
    Q_OBJECT

public:
    HtForce(float vel, int sensoDiRotazione, float A_x, float B_x, float A_y, float B_y, double A_f, double B_f, double C_f, double D_f, double E_f, double F_f, double G_f);
    ~HtForce();

private:
    TextureImage textures[13];										// Storage For 10 Textures
    QPoint mouse;
    int mStateL;
    int frames;
    QTime time;
    QTime time_rotazione;
    Target *targetProva[2];
    Target *targetMine[10];
    Target *targetSubmarine;

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void updateHtForce();
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void changeEvent(QEvent *event);
    int inizilizza_immagini_TGA ();
    int LoadTGA ( TextureImage *texture, char filename[] );


private:
    void glCircle3i(GLint x, GLint y, GLfloat radius);
    void Object(float x0_texture,float x1_texture,float y0_texture,float y1_texture,float width,float height,GLuint texid);				// Draw Object Using Requested Width, Height And Texture
    void selectionGameHtForce();										// This Is Where Selection Is Done
    void drawHtForce();
    float rand_FloatRange(float a, float b){return ((b-a)*((float)rand()/RAND_MAX))+a;}
    int stato;

private:
    int griglia [ 2 ];  // genera il cerchio da seguire
    int binario [ 2 ];  // traiettoria del treno
    int verso;          // variabile per impostare il verso di rotazione orario o antiorario
    double livello;        // variabile in cui imposto la velocitŕ di rotazione
    int inizio_test;
    int in_out;
    float tempo_fuori_target;
    double angolo;                                               //
    int prova;
    int elapsed;

private:
    int COM_inizializzata;
    struct Touch
    {
        float x;
        float y;
        float f1;
        float f2;
        float f3;
        int mDito;
        int ID_Arduino;
        float tara_1;
        float tara_2;
        float tara_3;
    };
    Touch touch;
    float a_x, b_x, a_y, b_y;
    double a_f, b_f, c_f, d_f, e_f, f_f, g_f, h_f, i_f, j_f;
    void selectionMouseGameHtForce( int mouseInteractionType  );

public slots:
    void ChiudiCOM();
};


#endif // HTFORCE_H

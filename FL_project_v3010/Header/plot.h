#ifndef _PLOT_H_
#define _PLOT_H_

#include <qwt_plot.h>
#include <qwt_scale_div.h>
#include <qwt_series_data.h>
#include <QFile>
#include <QMessageBox>

class QwtPlotCurve;
class QwtPlotIntervalCurve;

class Plot: public QwtPlot
{
    Q_OBJECT

public:
    enum Mode
    {
        Bars,
        Tube
    };

    Plot( QWidget * = NULL, QString fileName_tmp = NULL );

public Q_SLOTS:
    void setMode( int );
    void exportPlot();

private:
    void insertCurve( const QString &title,
        const QVector<QPointF> &, const QColor & );

    void insertErrorBars( const QString &title,
        const QVector<QwtIntervalSample> &,
        const QColor &color );


    QwtScaleDiv yearScaleDiv() const;

    QwtPlotIntervalCurve *d_intervalCurve;
    QwtPlotCurve *d_curve;
};

class Temperature
{
public:
    Temperature():
        minValue( 0.0 ),
        maxValue( 0.0 ),
        averageValue( 0.0 )
    {
    }

    Temperature( double min, double max, double average ):
        minValue( min ),
        maxValue( max ),
        averageValue( average )
    {
    }

    double minValue;
    double maxValue;
    double averageValue;
};

extern Temperature friedberg2007[];
#endif



#ifndef LINE_H
#define LINE_H

#include <QGLWidget>
#include <GL/glu.h>
#include <math.h>
#include <QTime>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QtMultimedia/QSound>
#include <QDir>
//#include <string>
//#include <iostream>
//#include <sstream>
#include "target.h"
#include "textureImage.h"


using namespace std;

#define M_PI 3.14159265358979323846

//extern char savedPathDir [1000];

class Line : public QGLWidget {
    Q_OBJECT

public:
    Line(double Tc_tmp,
         int storedSignalChk_tmp,
         int storedDisturbChk_tmp,
         int durataTest_tmp,
         float* colorBackground_tmp,
         float* signalColor_tmp,
         float* disturbColor_tmp,
         float* touchColor_tmp,
         float* limitColor_tmp,
         int signalTargetWidth_tmp,
         int disturbTargetWidth_tmp,
         int touchTargetWidth_tmp,
         int limitTargetWidth_tmp,
         int disturbYN_tmp,
         QString signalFileName_tmp,
         QString disturbFileName_tmp,
         QString fileName,
         double tAtteso_tmp,
         double tAttesoDisturbo_tmp,
         int lineLFPmeasuring_tmp);

    ~Line();
QTimer *testTimer;
private:

    //TextureImage textures[13];										// Storage For 10 Textures
    GLuint texture[13];
    QPoint mouse;
    int mStateL;
    int frames;
    QTime time;
    double *signal;
    double *signalDisturb;
    int signalEnd;
    double *Posizioni;
    double *saveTime;
        double *saveSignal;
    double *saveDisturb;
    double *saveTouch;
    int Limiti_pixel[2];//
    QTime timeLine;
    QTime timeLimit;
    Target *target;
    Target *disturb;
    Target *touchTarget;
    int durataTest;//
    int TempoSequenza; // minuti
    int displayPoint;
    double Tc;//
    double TcSignal;//
    double TcDisturb;//
    double TcLimite;//
    double Ta;
    int storedSignalChk;//
    int storedDisturbChk;//
    int disturbYN;//
    int iiLimite;
    double Tempo_precLimite ;
    float backgroundColor[4];//
    float signalColor[4];//
    float disturbColor[4];//
    float touchColor[4];//
    float limitColor[4];//
    int signalTargetWidth;//
    int disturbTargetWidth;//
    int touchTargetWidth;//
    int limitTargetWidth;//
    double tAtteso;
    double tAttesoDisturbo;
    int puntoTaraturaDimension;
    int lineLFPmeasuring;
    QFile fileOutput;

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void updateLine();
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
//    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
//int inizilizza_immagini_TGA ();
    //int LoadTGA ( TextureImage *texture, char filename[] );
    int generateSignal( int w );


private:
    void drawLine();
    float rand_FloatRange(float a, float b){return ((b-a)*((float)rand()/RAND_MAX))+a;}
    double* loadSignal( QString signalFileName_tmp, double *signal_tmp );

private:
    int stato;
    void msgBox();

signals:
    void tcError(double tc_tmp, double tcSignal_tmp, double tcDisturb_tmp);

private:
    void selectionMouseGameLine( int mouseInteractionType  );
};


#endif // LINE_H

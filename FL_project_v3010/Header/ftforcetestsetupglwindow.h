#ifndef FTFORCETESTSETUPGLWINDOW_H
#define FTFORCETESTSETUPGLWINDOW_H

#include <QGLWidget>
#include <GL/glu.h>

#define M_PI 3.14159265358979323846

class FtForceTestSetupGlWindow : public QGLWidget {
    Q_OBJECT

public:
    FtForceTestSetupGlWindow( float* backgroundColor_tmp,
            float* signalColor_tmp,
                       float* disturbColor_tmp,
                       float* touchColor_tmp,
                       float* limitColor_tmp,
                       int signalWidth_tmp,
                       int disturbWidth_tmp,
                       int touchWidth_tmp,
                       int limitWidth_tmp);

    ~FtForceTestSetupGlWindow();

private:
    float backgroundColor[4];
    float signalColor[4];
    float disturbColor[4];
    float touchColor[4];
    float limitColor[4];
    int signalWidth;
    int disturbWidth;
    int touchWidth;
    int limitWidth;
    void Object(float x0_texture,float x1_texture,float y0_texture,float y1_texture,float width,float height,GLuint texid);

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();

public slots:
    void updateColorWidth( float* colorBackground_tmp,
                            float* signalColor_tmp,
                            float* disturbColor_tmp,
                            float* touchColor_tmp,
                            float* limitColor_tmp,
                            int signalWidth_tmp,
                            int disturbWidth_tmp,
                            int touchWidth_tmp,
                            int limitWidth_tmp);
};

#endif // FTFORCETESTSETUPGLWINDOW_H

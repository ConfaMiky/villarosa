#ifndef TARGET_H
#define TARGET_H
#include <QGLWidget>
#include <GL/glu.h>
#include <QTime>
#include <QTimer>
#include "math.h"

#include "textureImage.h"

#define M_PI 3.14159265358979323846

class Target: public QGLWidget
{
     Q_OBJECT
public:
    Target();
    Target( QPoint position_tmp, int dimension_tmp[2], int dimensionChk_tmp[2],int geometryType_tmp, int stateTmp, float colorTmp_arr[], float colorOkChkTmp_arr[], float colorNotOkChkTmp_arr[], int n_textureIndexTmp, int id_tmp, GLuint texture_tmp);//TextureImage texture_tmp);
    ~Target();
    const static boolean plotTargetVariable = false;
    void Initialize ( QPoint position_tmp, int dimension_tmp[2], int dimensionChk_tmp[2],int geometryType_tmp, int stateTmp, float colorTmp_arr[], float colorOkChkTmp_arr[], float colorNotOkChkTmp_arr[], int n_textureIndexTmp, int id_tmp, GLuint texture);// TextureImage texture_tmp);
private:
    QPoint position;  // actual target position
    QPoint positionOld;   //last target position
    enum targetState { normal = 0, chkOk = 1, chkNotOk = 2 };
    targetState prevState;  // state of target ( true = cheked, false = free )
    targetState state;  // state of target ( true = cheked, false = free )
    boolean inThresholdTime;
    int mouseState; // mouse state ( 0 = nothing, 1 = on target, 2 = clicked, 3 = released, 4 = double clicked, 5 = tablet input, 6 = FP input )
    float color_arr[4];   // color of target not cheked
    float colorOkChk_arr[4];    // target color checked ok
    float colorNotOkChk_arr[4];    // target color checked not ok
    int textureNumber;  // index of imeage in Textures class variable
    int id; // identification number of the target
    int dimension[2];   // target dimension ( w,h for rectangle, r1,r1 for circle )
    int dimensionChk[2]; // dimension of taget checked
    int geometryType; // geometry type ( 0 = rect, 1 = circleArea, 2 = circle empty, ... )
    //TextureImage texture;										// Storage For 10 Textures
    GLuint texture;
    QTime time; // timer that start when is checked


public:
    int setPosition ( int xPos_tmp, int yPos_tmp );
    QPoint getPosition();
    int setState ( int state_tmp);
    int getState();
    int setinThresholdTime ( boolean inThresholdTime_tmp);
    boolean getinThresholdTime();
    int getElapsedTime();
    int startTime();
    int getId();
    int setMouseState( int mouseInteractionType_tmp  );
    int getMouseState();
    int getDimension( int dimension_tmp[]);
    int setDimension( int dimension_tmp[]);
    int setColor( int *colorTmp_arr);
    int setColorOkChk( int *colorOkChkTmp_arr );
    int setColorNotOkChk( int *colorNotOkChkTmp_arr );
//    int setTextureNumber( int index_tmp );
//    int getTextureNumber();
    QTimer errorChk_timer;

private:    // variable memorized in teh taget usful for tthe elaboration
    int timeCompleteTarget; // time to complete the selection of target
    int errorNumber;    // number of times target wrong checked
    int totNumOfTarget;

public:
    int displayMouse();
    int displayForce();
    inline void setTotNumOfTarget(int ii_tmp){ totNumOfTarget = ii_tmp; }
    inline int getTotNumOfTarget(){ return totNumOfTarget; }

private:
    int rectangle(float x0_texture,float x1_texture,float y0_texture,float y1_texture);				// Draw Object Using Requested Width, Height And Texture
    int circle();
    int circleArea();

public slots:
    void resetState();
    void setPrevState();

public:
    void startErrorChk_timer( int interval_tmp );
    void connectErrorChk_timer();

};

#endif // TARGET_H

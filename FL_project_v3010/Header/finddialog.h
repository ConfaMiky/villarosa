#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QtGui>
#include <QtWidgets>
#include <QGLWidget>
#include <QSplitter>
#include <QSettings>
#include <QtSerialPort/QSerialPort>

// inserire qui l'header della classe
#include "window.h"
#include "crc.h"
#include "touch.h"
//#include "barchart.h"

#include "newuserfoder.h"
#include "line.h"
#include "linesetupglwindow.h"
#include "ftforcetest.h"
#include "ftforcetestsetupglwindow.h"
#include "illuminalacitta.h"

class QCheckBox;
class QPushButton;
class QSpinBox;
class QGroupBox;
class QAction;
class Window;

// chabge to the class name ( test name )
class NewUserFolder;
class ExempleTest;
class Line;
class LineSetupGlWindow;
class FtForceTest;
class FtForceTestSetupGlWindow;
class IlluminaLaCitta;

class FindDialog : public QWidget
{
    Q_OBJECT

public:
    FindDialog(QWidget *parent = 0);
    QTimer *timer;
    QSplitter *page;
    QGridLayout *createSelectedUserPage();
    QListWidget *contentsWidget;
    QTableWidget* m_pTableWidget;
    QLabel *nameLabel;
    QLabel *surnameLabel;
    QLabel *diseaseLabel;
    Window *window;
    BarChart *barChart;
    BarChart *barChart1;
    BarChart *barChart2;
//    void  crcInit();
//    crc   crcSlow(const char  message[], int nBytes);
//    crc   crcFast(const char  message[], int nBytes);
    Touch touch;

    // modify with test class name
    NewUserFolder *newFolder;
    Line *newLineTest;
    LineSetupGlWindow *lineSetupGlWindow;
    FtForceTest *newFtForceTest;
    FtForceTestSetupGlWindow *ftForceTestSetupGlWindow;
    IlluminaLaCitta *newIlluminaLaCittaTest;

private slots:
    void setBarChart(QString fileName_tmp);

    void updateUserLabel( QString newPath );
    void updateOnlyUserLabel( QString newPath );
    void showNewUserButton();
    void newLineTestClicked();
    void lineBackgroundColorSpinBoxChange();
    void lineWriteSetting();
    void lineReadSetting();
    void lineDefaultSetting();
    void newFtForceTestClicked();
    void ftForceTestBackgroundColorSpinBoxChange();
    void ftForceTestWriteSetting();
    void ftForceTestReadSetting();
    void ftForceTestDefaultSetting();
    void newIlluminaLaCittaClicked();
    void illuminaLaCittaWriteSetting();
    void illuminaLaCittaReadSetting();
    void illuminaLaCittaDefaultSetting();
//    void newtrovaLImmagineClicked();
//    void trovaLImmagineWriteSetting();
//    void trovaLImmagineReadSetting();
//    void trovaLImmagineDefaultSetting();


private slots:
    void open();
    void createFolder();
    bool activateButton();
    bool deactivateButton();
    void setLineSignalActiveDeative( int state_tpm);
    void setLineDisturbActiveDeative( int state_tpm);
    void lineMsgBox( double tc_tmp, double tcSignal_tmp, double tcDisturb_tmp);
    void setFtForceTestSignalActiveDeative( int state_tpm);
    void setFtForceTestDisturbActiveDeative( int state_tpm);
    void ftForceTestMsgBox( double tc_tmp, double tcSignal_tmp, double tcDisturb_tmp);

    void updateUserSelected( int row, int column );

signals:
    void lineBackgroungColorChange(float* colorBackground_tmp,
                                   float* signalColor_tmp,
                                   float* disturbColor_tmp,
                                   float* touchColor_tmp,
                                   float* limitColor_tmp,
                                   int signalWidth_tmp,
                                   int disturbWidth_tmp,
                                   int touchWidth_tmp,
                                   int limitWidth_tmp);
    void ftForceTestBackgroungColorChange(float* colorBackground_tmp,
                                   float* signalColor_tmp,
                                   float* disturbColor_tmp,
                                   float* touchColor_tmp,
                                   float* limitColor_tmp,
                                   int signalWidth_tmp,
                                   int disturbWidth_tmp,
                                   int touchWidth_tmp,
                                   int limitWidth_tmp);


    void setBarChartSignal( BarChart *barChart_tmp, BarChart *barChart_tmp1, BarChart *barChart_tmp2);

private:
    void createUserListPage();
    void createOpenOrNewUserPage(); // new user page
    void createLinePage();
    void createFtForceTestPage();
    void createIlluminaLaCittaPage();
//    void createtrovaLImmaginePage();

    QStackedLayout *selectedPage;
    QListWidget *pageList;
    QStackedLayout *selectedUser;
    QListWidget *userList;

    QWidget *newOrOpenUserPage;
    QLabel *userLabel;
    QPushButton *loadUserButton;
    QToolButton *newUserButton;

    QWidget *linePage;
    QToolButton *lineStartButton;
    QToolButton *lineStopButton;
    QToolButton *lineDefaultSettingsButton;
    QToolButton *lineSaveSettingsButton;
    QSpinBox *lineSpinBox;
    QDoubleSpinBox  *lineTempoSaltoSpinBox;
    QDoubleSpinBox  *lineTempoSaltoDisturboSpinBox;
    QSpinBox *lineTcSpinBox;
    QSpinBox *lineSignalTargetDimension;
    QSpinBox *lineDisturbTargetDimension;
    QSpinBox *lineTouchTargetDimension;
    QSpinBox *lineLimitTargetDimension;
    QSpinBox *lineBackgroundColorR_SpinBox;
    QSpinBox *lineBackgroundColorG_SpinBox;
    QSpinBox *lineBackgroundColorB_SpinBox;
    QSpinBox *lineBackgroundColorA_SpinBox;
    QSpinBox *lineSignalColorR_SpinBox;
    QSpinBox *lineSignalColorG_SpinBox;
    QSpinBox *lineSignalColorB_SpinBox;
    QSpinBox *lineSignalColorA_SpinBox;
    QSpinBox *lineDisturbColorR_SpinBox;
    QSpinBox *lineDisturbColorG_SpinBox;
    QSpinBox *lineDisturbColorB_SpinBox;
    QSpinBox *lineDisturbColorA_SpinBox;
    QSpinBox *lineTouchColorR_SpinBox;
    QSpinBox *lineTouchColorG_SpinBox;
    QSpinBox *lineTouchColorB_SpinBox;
    QSpinBox *lineTouchColorA_SpinBox;
    QSpinBox *lineLimitColorR_SpinBox;
    QSpinBox *lineLimitColorG_SpinBox;
    QSpinBox *lineLimitColorB_SpinBox;
    QComboBox *lineSignalSlectionComboBox;
    QComboBox *lineDisturbSlectionComboBox;
    QCheckBox *lineStoredNonStoredSignal;
    QCheckBox *lineStoredNonStoredDisturb;
    QCheckBox *lineDisturbYN;
    QCheckBox *lineLFPmeasuring;

    QWidget *ftForceTestPage;
    QToolButton *ftForceTestStartButton;
    QToolButton *ftForceTestStopButton;
    QToolButton *ftForceTestDefaultSettingsButton;
    QToolButton *ftForceTestSaveSettingsButton;
    QSpinBox *ftForceTestSpinBox;
    QDoubleSpinBox  *ftForceTestTempoSaltoSpinBox;
    QDoubleSpinBox  *ftForceTestTempoSaltoDisturboSpinBox;
    QSpinBox *ftForceTestTcSpinBox;
    QSpinBox *ftForceTestSignalTargetDimension;
    QSpinBox *ftForceTestDisturbTargetDimension;
    QSpinBox *ftForceTestTouchTargetDimension;
    QSpinBox *ftForceTestLimitTargetDimension;
    QSpinBox *ftForceTestBackgroundColorR_SpinBox;
    QSpinBox *ftForceTestBackgroundColorG_SpinBox;
    QSpinBox *ftForceTestBackgroundColorB_SpinBox;
    QSpinBox *ftForceTestBackgroundColorA_SpinBox;
    QSpinBox *ftForceTestSignalColorR_SpinBox;
    QSpinBox *ftForceTestSignalColorG_SpinBox;
    QSpinBox *ftForceTestSignalColorB_SpinBox;
    QSpinBox *ftForceTestSignalColorA_SpinBox;
    QSpinBox *ftForceTestDisturbColorR_SpinBox;
    QSpinBox *ftForceTestDisturbColorG_SpinBox;
    QSpinBox *ftForceTestDisturbColorB_SpinBox;
    QSpinBox *ftForceTestDisturbColorA_SpinBox;
    QSpinBox *ftForceTestTouchColorR_SpinBox;
    QSpinBox *ftForceTestTouchColorG_SpinBox;
    QSpinBox *ftForceTestTouchColorB_SpinBox;
    QSpinBox *ftForceTestTouchColorA_SpinBox;
    QSpinBox *ftForceTestLimitColorR_SpinBox;
    QSpinBox *ftForceTestLimitColorG_SpinBox;
    QSpinBox *ftForceTestLimitColorB_SpinBox;
    QComboBox *ftForceTestSignalSlectionComboBox;
    QComboBox *ftForceTestDisturbSlectionComboBox;
    QCheckBox *ftForceTestStoredNonStoredSignal;
    QCheckBox *ftForceTestStoredNonStoredDisturb;
    QCheckBox *ftForceTestDisturbYN;
    QCheckBox *ftForceTestLFPmeasuring;

    QWidget *illuminaLaCittaPage;
    QToolButton *illuminaLaCittaStartButton;
    QToolButton *illuminaLaCittaStopButton;
    QToolButton *illuminaLaCittaDefaultSettingsButton;
    QToolButton *illuminaLaCittaSaveSettingsButton;

    QCheckBox *illuminaLaCittaForceEnabled_ChkBox;
    QLabel *illuminaLaCittaFoceMantainingTime_Label;
    QSpinBox *illuminaLaCittaFoceMantainingTime_SpinBox;
    QLabel *illuminaLaCittaUpperThreshold_Label;
    QSpinBox *illuminaLaCittaUpperThreshold_SpinBox;
    QLabel *illuminaLaCittaLowerThreshold_Label;
    QSpinBox *illuminaLaCittaLowerThreshold_SpinBox;

    QSpinBox *illuminaLaCittaTargetDimension_SpinBox;

    QCheckBox *illuminaLaCittaStoredNonStoredGrid_ChkBox;

    QComboBox *illuminaLaCittaStoredNonStoredGrid_ComboBox;

    QLabel *illuminaLaCittaTotalNumberTarget_Label;
    QSpinBox *illuminaLaCittaTotalNumberTarget_SpinBox;
    QLabel *illuminaLaCittaTargetOnLeftSide_Label;
    QSpinBox *illuminaLaCittaTargetOnLeftSide_SpinBox;
    QLabel *illuminaLaCittaTargetOnRightSide_Label;
    QSpinBox *illuminaLaCittaTargetOnRightSide_SpinBox;
    QCheckBox *illuminaLaCittaSimmetry_ChkBox;

    QCheckBox *illuminaLaCittaHideTime_ChkBox;
    QLabel *illuminaLaCittaHideTime_Label;
    QSpinBox *illuminaLaCittaHideTime_SpinBox;

    QSpinBox *illuminaLaCittaBackgroundColorR_SpinBox;
    QSpinBox *illuminaLaCittaBackgroundColorG_SpinBox;
    QSpinBox *illuminaLaCittaBackgroundColorB_SpinBox;
    QSpinBox *illuminaLaCittaBackgroundColorA_SpinBox;

    QLabel *illuminaLaCittaTotalTime_Label;
    QSpinBox *illuminaLaCittaTotalTime_SpinBox;

    QComboBox *illuminaLaCittaModalitaComboBox;

    QCheckBox *illuminaLaCittaInfiniteTime_ChkBox;

///////////////////////////////
    QWidget *trovaLImmaginePage;
    QToolButton *trovaLImmagineStartButton;
    QToolButton *trovaLImmagineStopButton;
    QToolButton *trovaLImmagineDefaultSettingsButton;
    QToolButton *trovaLImmagineSaveSettingsButton;

    QCheckBox *trovaLImmagineForceEnabled_ChkBox;

    QSpinBox *trovaLImmagineTargetDimension_SpinBox;

    QCheckBox *trovaLImmagineStoredNonStoredGrid_ChkBox;
    QComboBox *trovaLImmagineStoredNonStoredGrid_ComboBox;

    QSpinBox *trovaLImmagineTotalNumberTarget_SpinBox;

    QCheckBox *trovaLImmagineHideTime_ChkBox;
    QSpinBox *trovaLImmagineHideTime_SpinBox;

    QSpinBox *trovaLImmagineBackgroundColorR_SpinBox;
    QSpinBox *trovaLImmagineBackgroundColorG_SpinBox;
    QSpinBox *trovaLImmagineBackgroundColorB_SpinBox;
    QSpinBox *trovaLImmagineBackgroundColorA_SpinBox;

    QSpinBox *trovaLImmagineTotalTime_SpinBox;

    QComboBox *trovaLImmagineModalitaComboBox;

private slots:
    void illuminaLaCittaUpdateLeftRightTargetNumber( int newValue_tmp );
    void illuminaLaCittaUpdateRightTargetNumber( int newValue_tmp );
    void illuminaLaCittaUpdateLeftTargetNumber( int newValue_tmp );
    void illuminaLaCittaUpdateTotalTargetNumber( QString newValue_tmp );
    void illuminaLaCittaUpdateTotalTargetNumberFromFile( bool gridSelectionState_tmp );
    void illuminaLaCittaUpdateTotalTargetNumberFromFile( QString fileNameSelected_tmp );

private:
    QString fileNameOutput;   // file path of test results

private:
    void drawIcon(QPainter *painter, QPoint pos);

private:
        QSerialPort *serial;
        QTimer *arduinoTimer;
//        struct Touch
//        {
//            float x;
//            float y;
//            float f1;
//            float f2;
//            float f3;
//            int mDito;
//            int ID_Arduino;
//            float tara_1;
//            float tara_2;
//            float tara_3;
//            float fTot;
//            float fTotPrev;
//            QCursor cursoreFP;
//        };
//        Touch touch;
        float A_x,B_x,A_y,B_y;  // touch
        float A_f, B_f, C_f, D_f, E_f, F_f, G_f, H_f, I_f, J_f; // load cell
        QLabel *touchLabel;
    signals:
        void touchRead(int,int,double);

private slots:
        void readData();
        void handleError(QSerialPort::SerialPortError error);
        void openSerialPort();
        void closeSerialPort();
        void disconnectIlluminaLaCitta();

};

#endif

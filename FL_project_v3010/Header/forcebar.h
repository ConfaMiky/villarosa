#ifndef FORCEBAR_H
#define FORCEBAR_H
#include <QProgressBar>
#include <QTimer>
#include <QDebug>
#include <QGLWidget>
#include <QLabel>

class QProgressBar;
class QTimer;
class QLabel;

class ForceBar: public QWidget
{
    Q_OBJECT

public:
    ForceBar( QWidget *);
    ~ForceBar();
    QProgressBar *forceBar;
    QLabel *forceUpperThresholdLabel;
    QLabel *forceLowerThresholdLabel;
    QProgressBar *forceBarInverted;
    QLabel *forceUpperThresholdInvertedLabel;
    QLabel *forceLowerThresholdInvertedLabel;
    QLabel *forceSelectionTrueFalseLabel;

private:
    QString forcebarstylesheetOK;
    QString forcebarstylesheetTime;
    QString forcebarstylesheetNO;
    QString forcebarstylesheetTimerNo;
    QString forcebarstylesheetTimerOk;
    QString forcebarstylesheetTimerWait;
    QLabel *forceTimeLabel;
    QTimer *forceTimer;
    int upperThreshold;
    int lowerThreshold;
    int width;
    int height;
    QTimer *forceTimerFeedback;

public slots:
    void setForceBar( double pressure, bool inTarget );
    void setForceUpperThreshold( int forceUpperThreshold_tmp );
    void setForceLowerThreshold( int forceLowerThreshold_tmp );
    void setForceRemainingtime( int forceRemainingtime_tmp );
    void forceMantainingTimeOk_slt();
    void forceSelectionTrueFalse_slt( bool targetSelection_tmp);
signals:
    void forceMantainingTimeOk_sng();
public:
    void forceBarShow();
    private slots:
    void resetForceTimerFeedback();
};

#endif // FORCEBAR_H

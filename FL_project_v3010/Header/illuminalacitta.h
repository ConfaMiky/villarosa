#ifndef ILLUMINALACITTA_H
#define ILLUMINALACITTA_H

#include <stdlib.h>
#include <QtWidgets>
#include <QGLWidget>
#include <GL/glu.h>
#include <math.h>
#include <QTime>
#include <QTimer>
#include <QtSerialPort/QSerialPort>
#include "crc.h"
#include <QProgressBar>

#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QtMultimedia/QSound>
#include <QTabletEvent>
#include <QDir>
#include "target.h"
#include "textureImage.h"
#include "touch.h"
#include "forcebar.h"

class ForceBar;
class QPushButton;

using namespace std;

#define M_PI 3.14159265358979323846




class IlluminaLaCitta :  public QGLWidget {
    Q_OBJECT

public:
    IlluminaLaCitta(bool illuminaLaCittaForceEnabled_tmp,
                    int illuminaLaCittaTargetDimension_tmp,
                    bool illuminaLaCittaStoredNonStoredGrid_tmp,
                    QString illuminaLaCittaStoredNonStoredGridFileName_tmp,
                    int illuminaLaCittaTotalNumberTarget_tmp,
                    int illuminaLaCittaTotalTime_tmp,
                    bool illuminaLaCittaHideTimeChk_tmp,
                    int illuminaLaCittaHideTime_tmp,
                    float* colorBackground_tmp,
                    int modalita_tmp,
                    QString fileName,
                    bool illuminaLaCittaTempoInfinito_tmp,
                    int illuminaLaCittaLeftNumberTarget_tmp,
                    int forceUpperThreshold_tmp,
                    int forceLowerThreshold_tmp,
                    int forceRemainingtime_tmp,
                    bool illuminaLaCittaSimmetry_tmp);

    ~ IlluminaLaCitta();
    QTimer *IlluminaLaCittaTimer;
    QTimer *IlluminaLaCittaClock;
    QTimer *IlluminaLaCittaAnimationTimer;
    QTimer *ErrorONo;
    Target target[56];
    void releaseVariable();
    QProgressBar *forceBar;
    ForceBar *forceBar_obj;

private:
//    TextureImage textures[13];										// Storage For 10 Textures
    GLuint texture[66];
    Touch touch;
    QPoint mouse;

    int mStateL;
    int frames;
    QTime time;
    QTime timeIlluminaLaCitta;

    Target *mouseTarget;
    Target *touchTarget;
    double pressureOffset;
    double pressureReal;
    float backgroundColor[4];//
//  Option variable
    bool forceChk;  // modalità con forza ( true = forza, false = mouse only )
    bool storedGrid; // true if user select a specific grid for target position
    QString fileStoredGrid; // file location of the grid of target position and property
    enum letterNumber { LETTER = 0, NUMBER = 1, BOTH = 2};
    letterNumber letterNumberSelection; // test modality
    int totalNumberOfTarget;    // total nmber of target to be checked
    bool illuminaLaCittaSimmetry;   // simmetry grid
    int illuminaLaCittaLeftNumberTarget;    // number of target on the left side of screen
    int totalTime; // max time to complete the test
    bool dinamicModality; // true = target green for all test, false = green only for the hideTime
    float hideTime; // time the checked target be colored [ms]
    bool illuminaLaCittaTempoInfinito;  // treu if test duration utill comelte all taget

//---
    int actualId;
    int animationFlag;

private:
    QTime *timeToGetTarget;
    QVector< QVector< int > > precision;
    int errorStep_n;   //number of error during each step
    QFile fileOutput;
    QTimer *hideTimer;

     //QPushButton *showResults;

private:
    QTimer *forceTime;
    float upperForceLimit;  // upper threshold for force
    float lowerForceLimit;  // lower threshold for force
    float forceManteiningTime; // time to be in forche thresholds to select target [ms]
    bool forceOk;
    int inizilizeImages ();
//    int loadGLTextures ( TextureImage *texture, char filename[] );
    int loadGLTextures( GLuint *texture, QString filename );

private slots:
    void setActualId();
    void setTouchPos(int touchX_tmp,int touchY_tmp, double fTot_tmp);
    void endTest();
    void closeTest();
    void IlluminaLaCittaClockStop();
    void resetAllTargetState();

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void updateIlluminaLaCitta();
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
//    int LoadTGA ( TextureImage *texture, char filename[] );

    void tabletEvent(QTabletEvent *event);

    int randInt(int low, int high) { return qrand() % ((high + 1) - low) + low;};

private:
    double fTot;
    void drawIlluminaLaCitta();
    int rectangle(float x0,float x1,float y0,float y1,float x0_texture,float x1_texture,float y0_texture,float y1_texture, GLuint texture_tmp);
    float rand_FloatRange(float a, float b){return ((b-a)*((float)rand()/RAND_MAX))+a;}
    void loadSignal( QString signalFileName_tmp, int w, int h );

private:
    void msgBox();
    bool inTarget1;

signals:
    void pressureRealChange ( double pressureReal, bool inTarget );
//    void disconnectTouch();
    void endTest(QString fileName_tmp);
    void closeTest_signal();
    void illuminaLaCittaTargetSelected( bool );


private:    
    void selectionMouseGameIlluminaLaCitta( int mouseInteractionType  );

private slots:
    void resetAnimationTimerFlag();

};

#endif // ILLUMINALACITTA_H


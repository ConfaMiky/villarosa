#include "target.h"
#include <QDebug>
Target::Target()
{
}

Target::Target ( QPoint position_tmp, int dimension_tmp[2], int dimensionChk_tmp[2],int geometryType_tmp, int stateTmp, float colorTmp_arr[], float colorOkChkTmp_arr[], float colorNotOkChkTmp_arr[],int n_textureIndexTmp, int id_tmp, GLuint texture_tmp)//TextureImage texture_tmp)
{
    position = position_tmp;
    positionOld = position_tmp;
    state = static_cast<targetState>(stateTmp);
    prevState = state;
    inThresholdTime = false;
    mouseState = 0;
    for ( int ii = 0; ii < 4 ; ii++ ) {
        color_arr[ii] = colorTmp_arr [ii];
        colorOkChk_arr[ii] = colorOkChkTmp_arr [ii];
        colorNotOkChk_arr[ii] = colorNotOkChkTmp_arr [ii];
    }
    textureNumber = n_textureIndexTmp;
    texture = texture_tmp;
    id = id_tmp;
    dimension[0] = dimension_tmp[0];
    dimension[1] = dimension_tmp[1];
    dimensionChk[0] = dimensionChk_tmp[0];
    dimensionChk[1] = dimensionChk_tmp[1];
    geometryType = geometryType_tmp;

    errorChk_timer.setInterval(5);
//    errorChk_timer.setSingleShot(true);

//    connect( &errorChk_timer, SIGNAL(timeout()), this, SLOT(resetState()));
    qDebug() << "create target";
} // end Target

Target::~Target()
{
    qDebug() << "release target" << id;
}

void Target::Initialize ( QPoint position_tmp, int dimension_tmp[2], int dimensionChk_tmp[2],int geometryType_tmp, int stateTmp, float colorTmp_arr[], float colorOkChkTmp_arr[],float colorNotOkChkTmp_arr[], int n_textureIndexTmp, int id_tmp, GLuint texture_tmp)// TextureImage texture_tmp)
{
    position = position_tmp;
    positionOld = position_tmp;

    state = static_cast<targetState>(stateTmp);
    prevState = state;
    mouseState = 0;
    for ( int ii = 0; ii < 4 ; ii++ ) {
        color_arr[ii] = colorTmp_arr [ii];
        colorOkChk_arr[ii] = colorOkChkTmp_arr [ii];
        colorNotOkChk_arr[ii] = colorNotOkChkTmp_arr [ii];
    }
    textureNumber = n_textureIndexTmp;
    texture = texture_tmp;
    id = id_tmp;
    dimension[0] = dimension_tmp[0];
    dimension[1] = dimension_tmp[1];
    dimensionChk[0] = dimensionChk_tmp[0];
    dimensionChk[1] = dimensionChk_tmp[1];
    geometryType = geometryType_tmp;

    errorChk_timer.setInterval(1000);

    errorChk_timer.setSingleShot(true);
    connect( &errorChk_timer, SIGNAL(timeout()), this, SLOT(resetState()));
//    connect( &errorChk_timer, SIGNAL(), this, SLOT(setPrevState()));
    qDebug() << "initialize target";
} // end Target

int Target::displayMouse()
{
    //if ( !errorChk_timer.isActive() ) resetState();
    glPushMatrix();
    glTranslatef(position.x(),position.y(),0);
    if ( textureNumber == -1 ) glDisable(GL_TEXTURE_2D);
    else glEnable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    if ( state == 0 ) glColor4fv(color_arr);
    else if ( state == 1 ) glColor4fv(colorOkChk_arr);
    else if ( state == 2 ) glColor4fv(colorNotOkChk_arr);
//    glColor4f(1,0,0,1);
    if ( geometryType == 0 ) rectangle(0,1,0,1);
    else if ( geometryType == 1 ) circleArea();
    else if ( geometryType == 2 ) circle();
    glPopMatrix();
    return 0;
}
int Target::displayForce()
{
//    system("PAUSE");
    //if ( !errorChk_timer.isActive() && state == 2 ) resetState();
    glPushMatrix();
    glTranslatef(position.x(),position.y(),0);
    if ( textureNumber == -1 ) glDisable(GL_TEXTURE_2D);
    else glEnable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    if ( state == 0 ) glColor4fv(color_arr);
    else if ( state == 1 ) glColor4fv(colorOkChk_arr);
    else if ( state == 2 ) glColor4fv(colorNotOkChk_arr);
    if ( geometryType == 0 ) rectangle(0,1,0,1);
    else if ( geometryType == 1 ) circleArea();
    else if ( geometryType == 2 ) circle();
    glPopMatrix();
    return 0;
}
int Target::rectangle(float x0_texture,float x1_texture,float y0_texture,float y1_texture)
{
    glBindTexture(GL_TEXTURE_2D, texture);//texture.texID);		// Select The Correct Texture
    glBegin(GL_QUADS);											// Start Drawing A Quad
    glTexCoord2f(x0_texture,y0_texture); glVertex3f(-float(dimension[0])/2,-float(dimension[1]),0.0f);	// Bottom Left
    glTexCoord2f(x1_texture,y0_texture); glVertex3f( float(dimension[0])/2,-float(dimension[1]),0.0f);	// Bottom Right
    glTexCoord2f(x1_texture,y1_texture); glVertex3f( float(dimension[0])/2, float(dimension[1]),0.0f);	// Top Right
    glTexCoord2f(x0_texture,y1_texture); glVertex3f(-float(dimension[0])/2, float(dimension[1]),0.0f);	// Top Left
    glEnd();
    return 0;// Done Drawing Quad
}

int Target::circleArea()
{
    GLUquadricObj *quadric_out;
    quadric_out = gluNewQuadric();
    gluQuadricDrawStyle(quadric_out, GLU_FILL);
    gluQuadricTexture(quadric_out, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D, texture);//texture.texID);		// Select The Correct Texture
    gluDisk(quadric_out,0,dimension[1],35,20);
    glColor3f ( 0.0, 0.0, 0.0);
    //gluDisk(quadric_out,dimension[0],dimension[1],35,20);
    return 0;
}

int Target::circle()
{
    GLUquadricObj *quadric_out;
    quadric_out = gluNewQuadric();
    gluQuadricDrawStyle(quadric_out, GLU_FILL);
    gluQuadricTexture(quadric_out, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D, texture);//texture.texID);		// Select The Correct Texture
    gluDisk(quadric_out,dimension[0],dimension[1],30,10);
    return 0;
}

int Target::setPosition ( int xPos_tmp, int yPos_tmp )
{
    positionOld.setX( position.x());
    positionOld.setY( position.y());
    position.setX(xPos_tmp);
    position.setY(yPos_tmp);
    return 0;
}

QPoint Target::getPosition()
{
    return position;
}

int Target::setState ( int state_tmp)
{
    setPrevState();
    state = static_cast<targetState>(state_tmp);
    return 0;
}

int Target::getState()
{
    return state;
}

int Target::getElapsedTime()
{
    return time.elapsed();
}

int Target::startTime()
{
    time.start();
    return 0;
}

int Target::getId()
{
    return id;
}

int Target::setMouseState( int mouseInteractionType_tmp)
{
    mouseState = mouseInteractionType_tmp;
    return 0;
}

int Target::getMouseState()
{
    return mouseState;
}

int Target::getDimension( int dimension_tmp[])
{
    dimension_tmp[0] = dimension[0];
    dimension_tmp[1] = dimension[1];
    return 0;
}

int Target::setDimension( int dimension_tmp[])
{
    dimension[0] = dimension_tmp[0];
    dimension[1] = dimension_tmp[1];
    return 0;
}

int Target::setColor( int *colorTmp_arr )
{
    for ( int ii = 0; ii <= 3 ; ii++ ) color_arr[ii] = colorTmp_arr [ii];
    return 0;
}
int Target::setColorOkChk( int *colorOkChkTmp_arr )
{
    for ( int ii = 0; ii <= 3 ; ii++ ) colorOkChk_arr[ii] = colorOkChkTmp_arr [ii];
    return 0;
}
int Target::setColorNotOkChk( int *colorNotOkChkTmp_arr )
{
    for ( int ii = 0; ii <= 3 ; ii++ ) colorNotOkChk_arr[ii] = colorNotOkChkTmp_arr [ii];
    return 0;
}
int Target::setinThresholdTime ( boolean inThresholdTime_tmp)
{
inThresholdTime = inThresholdTime_tmp;
return 0;
}

boolean Target::getinThresholdTime()
{
return inThresholdTime;
}


void Target::setPrevState()
{
    prevState = state;
        //state = static_cast<targetState>(0);
        qDebug() << " id " << id <<" reset state " << state;
}

void Target::resetState()
{
    state = prevState;
        //state = static_cast<targetState>(0);
        qDebug() << " id " << id <<" reset state " << state;
}

void Target::startErrorChk_timer( int interval_tmp )
{
    errorChk_timer.start( interval_tmp );
}

void Target::connectErrorChk_timer( )

{
    connect( &errorChk_timer, SIGNAL(timeout()), this, SLOT(resetState()));

}

//int Target::setTextureNumber( int index_tmp );
//int Target::getTextureNumber();

#include "illuminalacitta.h"
#include <QKeyEvent>
#include <QDebug>
#include <QDateTime>

// Constructor
IlluminaLaCitta::IlluminaLaCitta::IlluminaLaCitta(bool illuminaLaCittaForceEnabled_tmp,
                                                  int illuminaLaCittaTargetDimension_tmp,
                                                  bool illuminaLaCittaStoredNonStoredGrid_tmp,
                                                  QString illuminaLaCittaStoredNonStoredGridFileName_tmp,
                                                  int illuminaLaCittaTotalNumberTarget_tmp,
                                                  int illuminaLaCittaTotalTime_tmp,
                                                  bool illuminaLaCittaHideTimeChk_tmp,
                                                  int illuminaLaCittaHideTime_tmp,
                                                  float* colorBackground_tmp,
                                                  int modalita_tmp,
                                                  QString fileName,
                                                  bool illuminaLaCittaTempoInfinito_tmp,
                                                  int illuminaLaCittaLeftNumberTarget_tmp,
                                                  int forceUpperThreshold_tmp,
                                                  int forceLowerThreshold_tmp,
                                                  int forceRemainingtime_tmp,
                                                  bool illuminaLaCittaSimmetry_tmp){
//    qDebug()<< QDir::currentPath();


////////////////////////////////////////////////////
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
    IlluminaLaCittaTimer = new QTimer(this);
    connect(IlluminaLaCittaTimer, SIGNAL(timeout()), this, SLOT(updateGL()));
    IlluminaLaCittaTimer->start();
    IlluminaLaCittaClock = new QTimer(this);
    if ( illuminaLaCittaTempoInfinito_tmp == true ) connect(IlluminaLaCittaClock, SIGNAL(timeout()), this, SLOT(IlluminaLaCittaClockStop()));

    pressureOffset = 0.3;
    pressureReal = 0;
    forceTime = new QTimer(this);
    forceTime->setSingleShot(true);

    ErrorONo = new QTimer(this);
    ErrorONo->setSingleShot(true);
    ErrorONo->setInterval(500);

    animationFlag = 0;

    IlluminaLaCittaAnimationTimer = new QTimer(this);
    IlluminaLaCittaAnimationTimer->setSingleShot(true);
    IlluminaLaCittaAnimationTimer->setInterval(200);
    connect(IlluminaLaCittaAnimationTimer, SIGNAL(timeout()), this, SLOT(resetAnimationTimerFlag()));

    inTarget1 = false;

    hideTimer = new QTimer(this);
    hideTimer->setSingleShot(true);
    connect(hideTimer, SIGNAL(timeout()), this, SLOT(resetAllTargetState()));


    // impostare le variabili relative alla opzioni del test
    totalNumberOfTarget = illuminaLaCittaTotalNumberTarget_tmp;
    illuminaLaCittaLeftNumberTarget =  illuminaLaCittaLeftNumberTarget_tmp;
    forceChk = illuminaLaCittaForceEnabled_tmp;

    illuminaLaCittaSimmetry = illuminaLaCittaSimmetry_tmp;

    letterNumberSelection = static_cast<letterNumber>(modalita_tmp);
    storedGrid = illuminaLaCittaStoredNonStoredGrid_tmp;
    if ( storedGrid ==  true ) fileStoredGrid = illuminaLaCittaStoredNonStoredGridFileName_tmp;
    else if (illuminaLaCittaSimmetry)
    {
        if ( letterNumberSelection == 'LETTER' || (totalNumberOfTarget <= 26 && (illuminaLaCittaLeftNumberTarget <= 13 && ( totalNumberOfTarget - illuminaLaCittaLeftNumberTarget ) <= 13 ) ) ) fileStoredGrid = "../../IlluminaLaCitta/BasePiccolaSimmetrica.txt";
        else fileStoredGrid = "../../IlluminaLaCitta/BaseSimmetrica.txt";
    }
    else
    {
        if ( letterNumberSelection == 'LETTER' || totalNumberOfTarget <= 26 ) fileStoredGrid = "../../IlluminaLaCitta/test.txt";
        else fileStoredGrid = "../../IlluminaLaCitta/base.txt";
    }

    totalTime = illuminaLaCittaTotalTime_tmp*60*1000;
//    totalTime = 30000;
    IlluminaLaCittaClock->setSingleShot(true);
    IlluminaLaCittaClock->start(totalTime);
    upperForceLimit = forceUpperThreshold_tmp;
    lowerForceLimit = forceLowerThreshold_tmp;
    forceManteiningTime = forceRemainingtime_tmp;
    dinamicModality = illuminaLaCittaHideTimeChk_tmp;
    hideTime = illuminaLaCittaHideTime_tmp;
    hideTimer->setInterval(hideTime);
    illuminaLaCittaTempoInfinito = illuminaLaCittaTempoInfinito_tmp;

    for (  int ii = 0; ii <= 3; ii++ )
    {
        backgroundColor[ii] = float(colorBackground_tmp[ii])/100;
    }

    fileOutput.setFileName(fileName+"/"+QDateTime::currentDateTime().toString("yyyyMMddhhmmss")+".txt");
//    qDebug()<<fileOutput.fileName();
    if (!fileOutput.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("non ? possibile aprire il file %1").arg(fileOutput.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }
    QTextStream out(&fileOutput);
    out << fileOutput.fileName() << "\n";
    out << "ForceChk" << "\t" << "letterNumberSelection" << "\t" << "totalNumberOfTarget" << "\t" << "forceManteiningTime[ms]" << "\t" << "dinamicModality" << "\t" << "hideTime[ms]" << "\n";
    out << forceChk << "\t" << letterNumberSelection << "\t" << totalNumberOfTarget << "\t" << forceManteiningTime << "\t" << dinamicModality << "\t" << hideTime << "\n\n";
    out << "TagetID" << "\t" << "x_pos_target(px)"<< "\t"<< "y_pos_target(px)" << "\t" << "x_pos_touch(px)"<< "\t"<< "y_pos_touch(px)"<<  "\t" << "TimeToChk(ms)" << "\t"<< "n°ErrorStep(num)"<< "\t"<< "inserire la legenda dei parametri salvati nel test\n";

    // imposto il valore delle variabili iniziali
    actualId = 0;
    errorStep_n = 0;
    //precision = 0;
    timeToGetTarget = new QTime();

    setMouseTracking(TRUE);
    glDrawBuffer(true);
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NoSystemBackground);
    setAutoBufferSwap(true);
    setWindowTitle("IlluminaLaCitta");
    setAttribute(Qt::WA_DeleteOnClose);
    this->doneCurrent();
    int width = QApplication::desktop()->width();
    int height = QApplication::desktop()->height();
    setFixedSize(width,height);

}

// Empty destructor
IlluminaLaCitta::~IlluminaLaCitta() {
//    free ( target );
//    free (touchTarget);
//    free (mouseTarget);
    forceBar_obj->close();
    qDebug() << "release variable";
//    emit disconnectTouch();
}

void IlluminaLaCitta::releaseVariable()
{
    free (touchTarget);
    free (mouseTarget);
}

// Initialize OpenGL
void IlluminaLaCitta::initializeGL() {
    inizilizeImages();
    int width = QApplication::desktop()->width();
    int height = QApplication::desktop()->height();
    setFixedSize(width,height);

    loadSignal(fileStoredGrid, width, height);
    QPoint *pointProva = new QPoint ( 100,500);
    int dimensionTouch[2] = { 50 - 2 , 50};
    float color01[4] = {100,0,0,1};
    touchTarget = new Target( *pointProva, dimensionTouch, dimensionTouch, 2, 0, color01, color01, color01, -1, 0, texture[0]);
    mouseTarget = new Target( *pointProva, dimensionTouch, dimensionTouch, 2, 0, color01, color01, color01, -1, 0, texture[0]);
    touchTarget->startTime();
    glShadeModel(GL_SMOOTH); // Enable smooth shading
    glClearColor(1,1,1,0); // Set the clear color to a black background

    glClearDepth(1.0f); // Depth buffer setup
    glEnable(GL_DEPTH_TEST); // Enable depth Lineing
    glDepthFunc(GL_LEQUAL); // Set type of depth Line
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really nice perspective calculations

    glEnable(GL_CULL_FACE);										// Remove Back Face

    forceBar_obj = new ForceBar(this);
    forceBar_obj->setForceRemainingtime(forceManteiningTime);
    forceBar_obj->setForceUpperThreshold(upperForceLimit);
    forceBar_obj->setForceLowerThreshold(lowerForceLimit);
    connect (this, SIGNAL(pressureRealChange(double,bool)), forceBar_obj, SLOT(setForceBar(double,bool)));
    connect(forceBar_obj, SIGNAL(forceMantainingTimeOk_sng()), this, SLOT(setActualId()));
    QObject::connect(this, SIGNAL(illuminaLaCittaTargetSelected(bool)), forceBar_obj, SLOT(forceSelectionTrueFalse_slt(bool)));
    forceBar_obj->forceBarShow();
}

// This is called when the OpenGL window is resized
void IlluminaLaCitta::resizeGL(int width, int height) {
    // Prevent divide by zero (in the gluPerspective call)
    if (height == 0)
        height = 1;

    glViewport(0, 0, width, height); // Reset current viewport

    glMatrixMode(GL_PROJECTION); // Select projection matrix
    glLoadIdentity(); // Reset projection matrix

    glOrtho(0,static_cast<GLfloat>(width),0,static_cast<GLfloat>(height),-1,100);				// Set Up An Ortho Screen

    glMatrixMode(GL_MODELVIEW); // Select modelview matrix
    glLoadIdentity(); // Reset modelview matrix
    GLint	viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
//    qDebug() << QDir().currentPath() ;
    forceBar_obj->forceBarShow();

}


// Ciclo di aggiornamento variabili
void IlluminaLaCitta::updateIlluminaLaCitta(){
    for ( int ii = 0; ii < target->getTotNumOfTarget() ; ii++ )
    {
        target[ii].setMouseState(0);
        //            qDebug() << "tgNum:" << ii << "mouse inter type " << target[ii].getMouseState() << " state " << target[ii].getState();
    }
}

// Selezione del target
void IlluminaLaCitta::selectionMouseGameIlluminaLaCitta( int mouseInteractionType  )   // This Is Where Selection Is Done
{
    GLUquadricObj *quadric;
    quadric = gluNewQuadric();

    GLuint	buffer[512];										// Set Up A Selection Buffer
    GLint	hits;												// The Number Of Objects That We Selected
    // The Size Of The Viewport. [0] Is <x>, [1] Is <y>, [2] Is <length>, [3] Is <width>
    GLint	viewport[4];

    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);

    glSelectBuffer(512, buffer);								// Tell OpenGL To Use Our Array For Selection

    // Puts OpenGL In Selection Mode. Nothing Will Be Drawn.  Object ID's and Extents Are Stored In The Buffer.
    (void) glRenderMode(GL_SELECT);

    glInitNames();												// Initializes The Name Stack
    glPushName(0);												// Push 0 (At Least One Entry) Onto The Stack

    glMatrixMode(GL_PROJECTION);								// Selects The Projection Matrix
    glPushMatrix();												// Push The Projection Matrix
    glLoadIdentity();											// Resets The Matrix

    // This Creates A Matrix That Will Zoom Up To A Small Portion Of The Screen, Where The mouse Is.
    if ( mouseInteractionType == 6 || mouseInteractionType == 5 ) gluPickMatrix((GLdouble) touchTarget->getPosition().x(), (GLdouble) (touchTarget->getPosition().y()), 1.0f, 1.0f, viewport);
    else gluPickMatrix((GLdouble) mouse.x(), (GLdouble) (viewport[3]-mouse.y()), 1.0f, 1.0f, viewport);

    glOrtho(0,viewport[2],0,viewport[3],-1,300);				// Set Up An Ortho Screen
    glMatrixMode(GL_MODELVIEW);									// Select The Modelview Matrix
    for ( int ii = 0; ii < totalNumberOfTarget ; ii++ )
    {
        glLoadName(ii);
        glPushMatrix();
        if ( !forceChk ) target[ii].displayMouse();
        else target[ii].displayForce();
        glPopMatrix();
    }
    glMatrixMode(GL_PROJECTION);								// Select The Projection Matrix
    glPopMatrix();												// Restore The Old Projection Matrix
    glMatrixMode(GL_MODELVIEW);									// Select The Modelview Matrix

    hits=glRenderMode(GL_RENDER);								// Switch To Render Mode, Find Out How Many
    if ( hits > 0 )						// If There Were More Than 0 Hits
    {
        int	choose[100];
        choose[0] = buffer[3];									// Make Our Selection The First Object
        for (int loop = 1; loop < hits; loop++)					// Loop Through All The Detected Hits
        {
            choose[loop] = buffer[4*loop+3];
        }
        for ( int ii = 0; ii < target[0].getTotNumOfTarget() ; ii++ )
        {
            target[ii].setMouseState(0);
            //            qDebug() << "tgNum:" << ii << "mouse inter type " << target[ii].getMouseState() << " state " << target[ii].getState();
        }
        inTarget1 = true;
        target[choose[0]].setMouseState(mouseInteractionType);
        if ( forceChk == false && mouseInteractionType == 2 && target[choose[0]].getId() == actualId )
        {
            target[choose[0]].setState( 1 );
            if ( dinamicModality )
            {
//                qDebug() << "start timer " << hideTimer->interval();
                hideTimer->start();
            }
            QTextStream out(&fileOutput);
            out << target[choose[0]].getId() << "\t" << target[choose[0]].getPosition().x()<< "\t"<< target[choose[0]].getPosition().y() << "\t" << mouse.x()<< "\t"<< viewport[3]-mouse.y()<<  "\t" << timeToGetTarget->elapsed() << "\t"<< errorStep_n <<"\t"<< totalNumberOfTarget << "\n";
            timeToGetTarget->restart();
            errorStep_n = 0;
            animationFlag = 1;
        //    IlluminaLaCittaAnimationTimer->start();
            actualId++;
            qDebug() << QDir::currentPath();
            QSound::play(":/Sound/Sound/illuminaLaCitta/ok.wav");
            if ( actualId == totalNumberOfTarget )
            {
                IlluminaLaCittaClock->stop();
//                IlluminaLaCittaTimer->stop();
//                forceTime->stop();
                IlluminaLaCittaClockStop();
                QSound::play(":/Sound/Sound/illuminaLaCitta/end.wav");
                            actualId++;
            }
            emit illuminaLaCittaTargetSelected( true );
        }
        else if ( forceChk == false && mouseInteractionType == 2 && target[choose[0]].getId() != actualId)
        {
            if ( !target[choose[0]].errorChk_timer.isActive() )
            {
                target[choose[0]].setState(2);
                target[choose[0]].startErrorChk_timer( 1000 );
                QSound::play(":/Sound/Sound/illuminaLaCitta/no.wav");
            }
            errorStep_n++;
            animationFlag = 2;
//            IlluminaLaCittaAnimationTimer->start();
            emit illuminaLaCittaTargetSelected( false );
        }
        else if ( forceChk == true && target[choose[0]].getId() == actualId )
        {
//            qDebug() << "forceOk id ok";
            if ( fTot > lowerForceLimit && fTot < upperForceLimit && forceTime->isActive() == false )
            {
                forceTime->start(1000);
            }
            else if ( fTot < lowerForceLimit || fTot > upperForceLimit && forceTime->isActive() == true )
            {
                forceTime->stop();
            }
        }
        else if ( forceChk == true && target[choose[0]].getId() != actualId )
        {

            if ( fTot > lowerForceLimit && fTot < upperForceLimit && !forceTime->isActive() && !target[choose[0]].errorChk_timer.isActive() )
            {
//                qDebug() << "forceOk id no";
                forceTime->start(1000);
            }
            else if ( fTot < lowerForceLimit || fTot > upperForceLimit && forceTime->isActive() )
            {
                forceTime->stop();
            }
        }
    }
    else
    {
        inTarget1 = false;
        for ( int ii = 0; ii < target[0].getTotNumOfTarget() ; ii++ )
        {
            target[ii].setMouseState(0);
        }
    }
}

// Disegno l'interfaccia del test con le OPENGL
void IlluminaLaCitta::drawIlluminaLaCitta() {

    GLint	viewport[4];
    GLUquadricObj *quadric_out;
    quadric_out = gluNewQuadric();

    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(backgroundColor[0],backgroundColor[1],backgroundColor[2],backgroundColor[3]);

    glEnable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
//    if (animationFlag == 1 )
//    {
//        glDisable(GL_LIGHTING);
//        glEnable(GL_TEXTURE_2D);
//        glColor3f ( 1.0, 1.0, 1.0);
//        for ( int ii = 0; ii < totalNumberOfTarget ; ii++ )
//        {
//            int dimension[2];
//            target[ii].getDimension(dimension);
//            QString qStr2;
//            if ( letterNumberSelection == NUMBER ) qStr2 = QString( "%1" ).arg((target[ii].getId()));
//            else if ( letterNumberSelection == LETTER ) qStr2 = QString( "%1" ).arg(QChar(target[ii].getId()+65));
//            else if ( letterNumberSelection == BOTH )
//            {
//                if ( target[ii].getId()%2 != 0 ) qStr2 = QString( "%1" ).arg(QChar(int(target[ii].getId()/2) + 65 ));
//                else qStr2 = QString( "%1" ).arg(int(target[ii].getId()/2));
//            }
//            if (target[ii].getId() < 10)  renderText(target[ii].getPosition().x()-dimension[0]/2 + 15 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/10 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//            else  renderText(target[ii].getPosition().x()-dimension[0]/2 + 15 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/10 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//    //        qStr2 = QString( "%1  %2" ).arg(target[ii].getMouseState()).arg(target[ii].getState());
//    //        renderText(target[ii].getPosition().x()-dimension[0]/2 + 15*3 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/9 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//        }   //  1           3               5               7
//        // 1 + 65 - 1. 3 + 65 - 2 66. 5 + 65 - 3 67. 7 + 65 - 4 68. 9 + 65 - 5 69. 11 + 65 - 6 70.
//        if ( !illuminaLaCittaTempoInfinito )
//        {
//            QString qStr2 = QString("Error n°:%4" ).arg(errorStep_n);
//            renderText(viewport[2]/20, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );
//    //        renderText(viewport[2]-viewport[2]/20, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );
//            }
//        else
//        {
//            QString qStr2 = QString("Time:%1" ).arg(IlluminaLaCittaClock->remainingTime()/1000);
//            renderText(viewport[2]/12, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );

//             QString qStr3= QString("Errori: %1").arg(errorStep_n);
//            renderText(viewport[2]/12, viewport[3]/8, qStr3,QFont("Arial", 15, QFont::Bold, false) );
//        }

//        glClearColor(0,1,0,0.1);
//    }
//    else if (animationFlag == 2 )
//    {
//        glDisable(GL_LIGHTING);
//        glEnable(GL_TEXTURE_2D);
//        glColor3f ( 1.0, 1.0, 1.0);
//        for ( int ii = 0; ii < totalNumberOfTarget ; ii++ )
//        {
//            int dimension[2];
//            target[ii].getDimension(dimension);
//            QString qStr2;
//            if ( letterNumberSelection == NUMBER ) qStr2 = QString( "%1" ).arg((target[ii].getId()));
//            else if ( letterNumberSelection == LETTER ) qStr2 = QString( "%1" ).arg(QChar(target[ii].getId()+65));
//            else if ( letterNumberSelection == BOTH )
//            {
//                if ( target[ii].getId()%2 != 0 ) qStr2 = QString( "%1" ).arg(QChar(int(target[ii].getId()/2) + 65 ));
//                else qStr2 = QString( "%1" ).arg(int(target[ii].getId()/2));
//            }
//            if (target[ii].getId() < 10)  renderText(target[ii].getPosition().x()-dimension[0]/2 + 15 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/10 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//            else  renderText(target[ii].getPosition().x()-dimension[0]/2 + 15 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/10 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//    //        qStr2 = QString( "%1  %2" ).arg(target[ii].getMouseState()).arg(target[ii].getState());
//    //        renderText(target[ii].getPosition().x()-dimension[0]/2 + 15*3 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/9 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//        }   //  1           3               5               7
//        // 1 + 65 - 1. 3 + 65 - 2 66. 5 + 65 - 3 67. 7 + 65 - 4 68. 9 + 65 - 5 69. 11 + 65 - 6 70.
//        if ( !illuminaLaCittaTempoInfinito )
//        {
//            QString qStr2 = QString("Error n°:%4" ).arg(errorStep_n);
//            renderText(viewport[2]/20, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );
//    //        renderText(viewport[2]-viewport[2]/20, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );
//            }
//        else
//        {
//            QString qStr2 = QString("Time:%1" ).arg(IlluminaLaCittaClock->remainingTime()/1000);
//            renderText(viewport[2]/12, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );

//             QString qStr3= QString("Errori: %1").arg(errorStep_n);
//            renderText(viewport[2]/12, viewport[3]/8, qStr3,QFont("Arial", 15, QFont::Bold, false) );
//        }

//        glClearColor(1,0,0,0.1);
//    }
//    else
//    {
        glColor4f ( 1.0, 1.0, 1.0, 1.0);
    if ( actualId != totalNumberOfTarget+1 )
    {
        rectangle(0, viewport[2], 0, viewport[3],0,1,0,1, texture[5]);
        rectangle(0, viewport[2], 50, viewport[3]/6+50,0,1,0,1, texture[6+53/(totalNumberOfTarget)*actualId]);
    }
    else rectangle(0, viewport[2], 0, viewport[3],0,1,0,1, texture[0]);

    for ( int ii = 0; ii < totalNumberOfTarget ; ii++ )
    {
        if ( !forceChk ) target[ii].displayMouse();
        else target[ii].displayForce();
    }

    if ( !forceChk )
    {
//        touchTarget->displayMouse();
        mouseTarget->displayMouse();

    }
    else
    {
          touchTarget->displayForce();
//        mouseTarget->displayForce();

    }

    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glColor3f ( 1.0, 1.0, 1.0);
    for ( int ii = 0; ii < totalNumberOfTarget ; ii++ )
    {
        int dimension[2];
        target[ii].getDimension(dimension);
        QString qStr2;
        if ( letterNumberSelection == NUMBER ) qStr2 = QString( "%1" ).arg((target[ii].getId()));
        else if ( letterNumberSelection == LETTER ) qStr2 = QString( "%1" ).arg(QChar(target[ii].getId()+65));
        else if ( letterNumberSelection == BOTH )
        {
            if ( target[ii].getId()%2 != 0 ) qStr2 = QString( "%1" ).arg(QChar(int(target[ii].getId()/2) + 65 ));
            else qStr2 = QString( "%1" ).arg(int(target[ii].getId()/2));
        }
        if (target[ii].getId() < 10)  renderText(target[ii].getPosition().x()-dimension[0]/2 + 15 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/10 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
        else  renderText(target[ii].getPosition().x()-dimension[0]/2 + 15 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/10 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
//        qStr2 = QString( "%1  %2" ).arg(target[ii].getMouseState()).arg(target[ii].getState());
//        renderText(target[ii].getPosition().x()-dimension[0]/2 + 15*3 , viewport[3] - ( target[ii].getPosition().y()-dimension[1]/9 ), qStr2,QFont("Arial", 15, QFont::Bold, false) );
    }   //  1           3               5               7
    // 1 + 65 - 1. 3 + 65 - 2 66. 5 + 65 - 3 67. 7 + 65 - 4 68. 9 + 65 - 5 69. 11 + 65 - 6 70.
    if ( !illuminaLaCittaTempoInfinito )
    {
        QString qStr2 = QString("Error n°:%4" ).arg(errorStep_n);
        renderText(viewport[2]/20, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );
//        renderText(viewport[2]-viewport[2]/20, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );
        }
    else
    {
        QString qStr2 = QString("Time:%1" ).arg(IlluminaLaCittaClock->remainingTime()/1000);
        renderText(viewport[2]/12, viewport[3]/12, qStr2,QFont("Arial", 15, QFont::Bold, false) );

         QString qStr3= QString("Errori: %1").arg(errorStep_n);
        renderText(viewport[2]/12, viewport[3]/8, qStr3,QFont("Arial", 15, QFont::Bold, false) );
    }    
//    }
    forceBar_obj->forceBarShow();
}

// OpenGL painting code goes here
void IlluminaLaCitta::paintGL() {
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear Screen And Depth Buffer
    //    glLoadIdentity();
    // Reset The Modelview Matrix
    drawIlluminaLaCitta();
    forceBar_obj->forceBarShow();
}

// Key handler
void IlluminaLaCitta::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
    case Qt::Key_Escape:
        fileOutput.close();
        close(); // Quit on Escape
        qDebug() << "Escape illumina ";
        break;
    case Qt::Key_F1:
        setWindowState(windowState() ^ Qt::WindowFullScreen); // Toggle fullscreen on F1
        break;
    case Qt::Key_F2:
        break;
    case Qt::Key_Up:
        for ( int ii = 0; ii < target[0].getTotNumOfTarget() ; ii++ ) qDebug() << "tgNum:" << ii << "mouse inter type " << target[ii].getMouseState() << " state " << target[ii].getState();
        break;
    case Qt::Key_Down:
        break;
    case Qt::Key_Left:
        pressureOffset+=0.0;//0.01;
        break;
    case Qt::Key_Right:
        pressureOffset-=0.0;//0.01;
        break;
    default:
        QGLWidget::keyPressEvent(event); // Let base class handle the other keys
    }
}

// Mouse Move handler
void IlluminaLaCitta::mouseMoveEvent(QMouseEvent *event) {
    if ( !forceChk )
    {
        GLint	viewport[4];
        selectionMouseGameIlluminaLaCitta( 1 );
        // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
        glGetIntegerv(GL_VIEWPORT, viewport);
        mouse = event->pos();
        mouseTarget->setPosition(mouse.x(),viewport[3]-mouse.y());
        //    qDebug() << touchTarget->getPosition().x() << "  " << touchTarget->getPosition().y();
        paintGL();
    }
}

void IlluminaLaCitta::mousePressEvent(QMouseEvent *event){
    if ( !forceChk )
    {
        selectionMouseGameIlluminaLaCitta( 2 );
        GLint	viewport[4];
        // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
        glGetIntegerv(GL_VIEWPORT, viewport);
        mouse = event->pos();
        mouseTarget->setPosition(mouse.x(),viewport[3]-mouse.y());
        paintGL();
    }
    else for ( int ii = 0; ii < target[0].getTotNumOfTarget() ; ii++ ) qDebug() << "tgNum:" << ii << "mouse inter type " << target[ii].getMouseState() << " state " << target[ii].getState();
}

void IlluminaLaCitta::tabletEvent(QTabletEvent *event)
{
    GLint	viewport[4];
    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);
    mouse = event->pos();
    touchTarget->setPosition(mouse.x(),viewport[3]-mouse.y());
    pressureReal = event->pressure();
    fTot = pressureReal;
    emit pressureRealChange(pressureReal, inTarget1);
    if ( event->pressure() > pressureOffset )
    {
        int dimension[2] = {(event->pressure())*(event->pressure()) * viewport[3]/2 - 10, (event->pressure())*(event->pressure()) * viewport[3]/2};
//        touchTarget->setDimension(dimension);
    }
    else
    {
        int dimension[2] = {pressureOffset * pressureOffset * viewport[3]/2 - 10, pressureOffset * pressureOffset * viewport[3]/2};
//        touchTarget->setDimension(dimension);
    }
//    qDebug()<< event->type();
    selectionMouseGameIlluminaLaCitta( 5 );
    //    update();
}

void IlluminaLaCitta::loadSignal( QString signalFileName_tmp, int w, int h )
{
    glClearColor(backgroundColor[0],backgroundColor[1],backgroundColor[2],backgroundColor[3]);
    GLint	viewport[4];
    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);

    QFile file(signalFileName_tmp);
    qDebug()<< QDir::currentPath()<< "file segnale  "<< signalFileName_tmp;
    if (!file.open(QIODevice::ReadOnly ))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }
    QTextStream in(&file);
    QString line = in.readLine();
    int ii = 0;
    int totalLineNumber = 0;
    //signalEnd = 0;

    while (!line.isNull()) {
        totalLineNumber++;
        line = in.readLine();
    }
    qDebug() << "total line " << totalLineNumber;
    file.close();

    int arrayIdCasuali[totalNumberOfTarget];
    int arrayIdCasualiLeft[totalLineNumber/2];




    qDebug() << " tot array casuale ID_left" << illuminaLaCittaLeftNumberTarget;

    for (int ii=0; ii<totalLineNumber/2; ii++){
        bool ok = false;
        while(!ok){
            arrayIdCasualiLeft[ii] = rand() % (totalLineNumber/2);
            ok = true;
            for(int jj=0; jj<ii; jj++){
                if(arrayIdCasualiLeft[ii] == arrayIdCasualiLeft[jj]) ok = false;
            }
        }
        qDebug() << " array casuale ID_left" << arrayIdCasualiLeft[ii];
    }

    for (int ii=0; ii<totalNumberOfTarget; ii++){
        bool ok = false;
        while(!ok){
            arrayIdCasuali[ii] = rand() % totalNumberOfTarget;
            ok = true;
            for(int jj=0; jj<ii; jj++){
                if(arrayIdCasuali[ii] == arrayIdCasuali[jj]) ok = false;
            }
        }
        qDebug() << " array casuale ID" << arrayIdCasuali[ii];
    }
    for (int ii=0; ii<totalLineNumber/2; ii++){
        qDebug() << " array casuale left ID" << arrayIdCasualiLeft[ii];
    }

    if ( storedGrid)
    {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox msgBox;
            msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
            msgBox.setInformativeText("");
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
        }

        QTextStream in1(&file);
        QString line1 = in1.readLine();

        qDebug() << "Leggo dalla griglia";
        while (!line1.isNull() || ii < totalNumberOfTarget ) {
        QStringList  fields = line1.split("\t");
        int dimension[2] = {fields[2].toDouble()*w,fields[3].toDouble()*w};
        int dimensionChk[2] = {fields[4].toDouble()*w,fields[5].toDouble()*w};
        float color[4] = {fields[8].toFloat(),fields[9].toFloat(),fields[10].toFloat(),fields[11].toFloat()};
        float colorOkChk[4] = {fields[12].toFloat(),fields[13].toFloat(),fields[14].toFloat(),fields[15].toFloat()};
        float colorNotOkChk[4] = {fields[16].toFloat(),fields[17].toFloat(),fields[18].toFloat(),fields[19].toFloat()};
        //        target[ii] = new Target();

        target[ii].Initialize( QPoint(int(fields[0].toDouble()*w),int(fields[1].toDouble()*h)), dimension,dimensionChk, fields[6].toInt(), 0, color, colorOkChk, colorNotOkChk, fields[20].toInt() , fields[21].toInt(), texture[fields[22].toInt()]);
        target[ii].startTime();
        target->setTotNumOfTarget(ii+1);
        //        exit(9876);
//        qDebug()<< target->getTotNumOfTarget() << " global ";
        ii++;
        line1 = in1.readLine();
    }
        file.close();
}
    else if ( illuminaLaCittaSimmetry )
    {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox msgBox;
            msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
            msgBox.setInformativeText("");
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
        }

        QTextStream in1(&file);
        QString line1 = in1.readLine();

        qDebug() << "Posizioni casuali";
        int jj = 0;
        float position[totalLineNumber][2];
        ii = 0;
        while (!line1.isNull() ) {

            QStringList  fields = line1.split("\t");
            if ( fields[0].toDouble() <= 0.5 )
            {

            position[ii][0] = fields[0].toDouble();
            position[ii][1] = fields[1].toDouble();
            qDebug() << "Leggo le posizioni sx" << ii << "  " << jj << " " << position[ii][0] << " " << position[ii][1];
            ii++;
            }
            else
            {
                position[totalLineNumber/2 + jj][0] = fields[0].toDouble();
                position[totalLineNumber/2 + jj][1] = fields[1].toDouble();
                qDebug() << "Leggo le posizioni dx" << ii << "  " << jj << " " << position[totalLineNumber/2 + jj][0] << " " << position[totalLineNumber/2 + jj][1];

            jj++;
            }
            line1 = in1.readLine();
            qDebug() << " posizioni " << ii+jj;
        }
        file.close();
        ii=0;
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox msgBox;
            msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
            msgBox.setInformativeText("");
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
        }

        QTextStream in2(&file);
        QString line2 = in2.readLine();
        while ( ii < totalNumberOfTarget)
        {
            qDebug() << "Leggo le posizioni" << ii;
            QStringList  fields = line2.split("\t");
            int dimension[2] = {fields[2].toDouble()*w,fields[3].toDouble()*w};
            int dimensionChk[2] = {fields[4].toDouble()*w,fields[5].toDouble()*w};
            float color[4] = {fields[8].toFloat(),fields[9].toFloat(),fields[10].toFloat(),fields[11].toFloat()};
            float colorOkChk[4] = {fields[12].toFloat(),fields[13].toFloat(),fields[14].toFloat(),fields[15].toFloat()};
            float colorNotOkChk[4] = {fields[16].toFloat(),fields[17].toFloat(),fields[18].toFloat(),fields[19].toFloat()};
           if ( ii < illuminaLaCittaLeftNumberTarget)
           {
               qDebug() << "A sinistra" << arrayIdCasualiLeft[ii];
           target[ii].Initialize( QPoint(int(position[arrayIdCasualiLeft[ii]][0]*w),int(position[arrayIdCasualiLeft[ii]][1]*h)), dimension,dimensionChk, fields[6].toInt(), 0, color, colorOkChk, colorNotOkChk, fields[20].toInt() , fields[21].toInt(), texture[fields[22].toInt()]);
           target[ii].startTime();
           target->setTotNumOfTarget(ii+1);
           qDebug() << arrayIdCasualiLeft[ii] << " " << target[ii].getPosition().x() << " " << target[ii].getPosition().y();
           }
           else
           {
               qDebug() << "A destra" ;
               target[ii].Initialize( QPoint(int(position[totalLineNumber/2 + arrayIdCasualiLeft[ii - illuminaLaCittaLeftNumberTarget]][0]*w),int(position[totalLineNumber/2 +arrayIdCasualiLeft[ ii - illuminaLaCittaLeftNumberTarget]][1]*h)), dimension,dimensionChk, fields[6].toInt(), 0, color, colorOkChk, colorNotOkChk, fields[20].toInt() , fields[21].toInt(), texture[fields[22].toInt()]);
               target[ii].startTime();
               target->setTotNumOfTarget(ii+1);
               qDebug() << totalLineNumber/2 + arrayIdCasualiLeft[ii - illuminaLaCittaLeftNumberTarget] << " " << target[ii].getPosition().x() << " " << target[ii].getPosition().y();
           }
           ii++;
           line2 = in2.readLine();
        }
    }
    else
    {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QMessageBox msgBox;
            msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
            msgBox.setInformativeText("");
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
        }

        QTextStream in1(&file);
        QString line1 = in1.readLine();

        int kk = 0;

        qDebug() << "Leggo dalla griglia";
        while (!line1.isNull() && kk < totalNumberOfTarget ) {
        QStringList  fields = line1.split("\t");
        int dimension[2] = {fields[2].toDouble()*w,fields[3].toDouble()*w};
        int dimensionChk[2] = {fields[4].toDouble()*w,fields[5].toDouble()*w};
        float color[4] = {fields[8].toFloat(),fields[9].toFloat(),fields[10].toFloat(),fields[11].toFloat()};
        float colorOkChk[4] = {fields[12].toFloat(),fields[13].toFloat(),fields[14].toFloat(),fields[15].toFloat()};
        float colorNotOkChk[4] = {fields[16].toFloat(),fields[17].toFloat(),fields[18].toFloat(),fields[19].toFloat()};
        //        target[ii] = new Target();

        target[kk].Initialize( QPoint(int(fields[0].toDouble()*w),int(fields[1].toDouble()*h)), dimension,dimensionChk, fields[6].toInt(), 0, color, colorOkChk, colorNotOkChk, fields[20].toInt() , arrayIdCasuali[kk], texture[fields[22].toInt()]);
        target[kk].startTime();
        qDebug()<< target->getTotNumOfTarget() << " global " << arrayIdCasuali[kk] << " " << totalNumberOfTarget << "i " << kk;
        target->setTotNumOfTarget(kk+1);
        //        exit(9876);

        kk++;
        line1 = in1.readLine();
    }
        file.close();
    }
    file.close();
    timeToGetTarget->start();
}

void IlluminaLaCitta::setActualId()
{
    for ( int ii = 0; ii < totalNumberOfTarget ; ii++ )
    {             qDebug() << "a item " << ii << " --- state " <<  target[ii].getState();
        if ( target[ii].getId() == actualId && ( target[ii].getMouseState() == 5 || target[ii].getMouseState() == 6 ) )
        {
            target[ii].setState(1);
            QTextStream out(&fileOutput);
            out << target[ii].getId() << "\t" << target[ii].getPosition().x()<< "\t"<< target[ii].getPosition().y() << "\t" << touchTarget->getPosition().x()<< "\t"<< touchTarget->getPosition().y()<<  "\t" << timeToGetTarget->elapsed() << "\t"<< errorStep_n <<"\t"<< totalNumberOfTarget << "\n";
            fileOutput.flush();
            timeToGetTarget->restart();
            errorStep_n = 0;
            actualId++;
            animationFlag = 1;
//            IlluminaLaCittaAnimationTimer->start();
            QSound::play(":/Sound/Sound/illuminalacitta/ok.wav");
            qDebug() << "b item selected " <<  target[ii].getState() << " actId " << actualId;
            if ( dinamicModality )
            {
                qDebug() << "start timer " << hideTimer->interval();
                hideTimer->start();
            }
            emit illuminaLaCittaTargetSelected( true );
        }
        else
        {
            if ( target[ii].getMouseState() == 5 || target[ii].getMouseState() == 6 )
            {
                animationFlag = 2;
//                IlluminaLaCittaAnimationTimer->start();
                errorStep_n++;
                QSound::play(":/Sound/Sound/illuminalacitta/no.wav");
                target[ii].setState(2);
                if ( !target[ii].errorChk_timer.isActive() ) target[ii].startErrorChk_timer( 1000 );
                for ( int uu = 0; uu < target[0].getTotNumOfTarget() ; uu++ )
                {
                    target[uu].setMouseState(0);
                }
                emit illuminaLaCittaTargetSelected( false );
            }
        }
    }
    if ( actualId == totalNumberOfTarget )
    {
        QSound::play(":/Sound/Sound/illuminalacitta/end.wav");
        IlluminaLaCittaClock->stop();
        IlluminaLaCittaClockStop();
        actualId++;
        }
    qDebug() << "actual id" << actualId << "   " << totalNumberOfTarget;
//         system("pause");
}

int IlluminaLaCitta::loadGLTextures( GLuint *textures, QString filename )//TextureImage *texture, char filename[] )				// Loads A TGA File Into Memory
{
    // Load image
    glEnable(GL_TEXTURE_2D);
    textures[0] = bindTexture(QImage(filename), GL_TEXTURE_2D);

    // Set nearest filtering mode for texture minification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // Set bilinear filtering mode for texture magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Wrap texture coordinates by repeating
    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    return TRUE;
}

int IlluminaLaCitta::inizilizeImages (){
    qDebug()<<QDir::currentPath();
    if ((!loadGLTextures( &texture[0],"../../Image/illuminalacitta/houses/houses_53.png")) ||			// Load The BlueFace Texture
            (!loadGLTextures( &texture[1],"../../Image/illuminalacitta/normal.png")) ||			// Load The Bucket Texture
            (!loadGLTextures( &texture[2],"../../Image/illuminalacitta/hover.png")) ||			// Load The Target Texture
            (!loadGLTextures( &texture[3],"../../Image/illuminalacitta/pressed.png")) ||			// Load The Ground Texture
            (!loadGLTextures( &texture[4],"../../Image/illuminalacitta/wrong.png")) ||				// Load The Vase Texture
            (!loadGLTextures( &texture[5],"../../Image/illuminalacitta/background.png")))				// Load The Crosshair Texture
    {
        return FALSE;											// If Loading Failed, Return False
    }
    for ( int ii = 0 ; ii < 54; ii++ )
    {
        QString fileName = QString("../../Image/illuminalacitta/houses/houses_%1.png").arg(ii);
        if (!loadGLTextures ( &texture[ii+6], fileName )) return FALSE;
    }

}

int IlluminaLaCitta::rectangle(float x0,float x1,float y0,float y1,float x0_texture,float x1_texture,float y0_texture,float y1_texture, GLuint texture_tmp)
{
    glBindTexture(GL_TEXTURE_2D, texture_tmp);//texture.texID);		// Select The Correct Texture
    glBegin(GL_QUADS);											// Start Drawing A Quad
    glTexCoord2f(x0_texture,y0_texture); glVertex3f(x0,y0,0.0f);	// Bottom Left
    glTexCoord2f(x1_texture,y0_texture); glVertex3f( x1,y0,0.0f);	// Bottom Right
    glTexCoord2f(x1_texture,y1_texture); glVertex3f( x1, y1,0.0f);	// Top Right
    glTexCoord2f(x0_texture,y1_texture); glVertex3f(x0, y1,0.0f);	// Top Left
    glEnd();
    return 0;// Done Drawing Quad
}

void IlluminaLaCitta::setTouchPos(int touchX_tmp,int touchY_tmp, double fTot_tmp)
{
    GLint	viewport[4];
    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);
    touchTarget->setPosition(touchX_tmp, viewport[3] - touchY_tmp);
    pressureReal = fTot_tmp;
    fTot = fTot_tmp*fTot_tmp;
    emit pressureRealChange(pressureReal * pressureReal, inTarget1);
    if ( pressureReal > pressureOffset )
    {
        int dimension[2] = {(pressureReal*pressureReal) * viewport[3]/2 - 10, (pressureReal*pressureReal) * viewport[3]/2};
//        touchTarget->setDimension(dimension);
    }
    else
    {
        int dimension[2] = {pressureOffset * pressureOffset * viewport[3]/2 - 10, pressureOffset * pressureOffset * viewport[3]/2};
//        touchTarget->setDimension(dimension);
    }
    selectionMouseGameIlluminaLaCitta( 6 );

    updateGL();
}

void IlluminaLaCitta::endTest()
{
    emit endTest(fileOutput.fileName());
    qDebug() << "emitted";
}

void IlluminaLaCitta::closeTest()
{
    emit closeTest_signal();
    qDebug() << "emitted";
}

void IlluminaLaCitta::IlluminaLaCittaClockStop()
{
    GLint	viewport[4];

    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);

    fileOutput.flush();
    fileOutput.close();
    qDebug() << viewport[2] << viewport[3];
    QFont endTestFont( "Arial", 25, QFont::Bold);

    QPushButton *showResults = new QPushButton(tr("showResults"),this);
    showResults->setFont(endTestFont);
    showResults->setDefault(true);
    showResults->setEnabled(true);
    showResults->setGeometry(viewport[2]/2 - 240,viewport[3]/2,220,100);
    connect( showResults, SIGNAL( clicked () ),
             this, SLOT( endTest() ));
    showResults->show();
    QPushButton *endTest = new QPushButton(tr("endTest"),this);
    endTest->setFont(endTestFont);
    endTest->setDefault(true);
    endTest->setEnabled(true);
    endTest->setGeometry(viewport[2]/2 + 40,viewport[3]/2,200,100);
    connect( endTest, SIGNAL( clicked () ),
             this, SLOT( closeTest() ));
    endTest->show();
}

void IlluminaLaCitta::resetAllTargetState()
{
    for ( int ii = 0; ii <= totalNumberOfTarget; ii++ ) target[ii].resetState();
}

void IlluminaLaCitta:: resetAnimationTimerFlag()
{
    animationFlag = 0;
}

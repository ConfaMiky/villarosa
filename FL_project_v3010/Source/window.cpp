/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>

#include "window.h"

//! [0]
Window::Window()
{
//    forcebarstylesheetOK = "QProgressBar {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
//            "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1F8E0, stop: 1 #74DF00)}";

//    forcebarstylesheetTime = "QProgressBar {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
//            "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F9FF30, stop: 1 #F7FF00)}";

//    forcebarstylesheetNO =  "QProgressBar  {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
//            "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F5A9A9, stop: 1 #FF0000)}";

//    forceBar = new QProgressBar();
//    forceBar->setStyleSheet(forcebarstylesheetOK);
//    forceBar->setAlignment(Qt::AlignVCenter);

//    qDebug()<< "h "<< forceBar->height();
//    forceUpperThresholdLabel = new QLabel(forceBar);
//    forceUpperThresholdLabel->setGeometry(QRect(0,0,20,20));
//    forceUpperThresholdLabel->setStyleSheet("QLabel {color : red} ");    //    forceUpperThresholdLabel->setPixmap(QPixmap("C:/Users/John/Documents/FL_project_vBase/Image/General/forcethreshold.png"));
//    forceUpperThresholdLabel->setPixmap(QPixmap("../../Image/General/forcethreshold.png"));
//    forceUpperThresholdLabel->show();
//    forceLowerThresholdLabel = new QLabel(forceBar);
//    forceLowerThresholdLabel->setGeometry(QRect(0,0,20,20));
//    forceLowerThresholdLabel->setStyleSheet("QLabel {color : red} ");    //    forceLowerThresholdLabel->setPixmap(QPixmap("C:/Users/John/Documents/FL_project_vBase/Image/General/forcethreshold.png"));
//    forceLowerThresholdLabel->setPixmap(QPixmap("../../Image/General/forcethreshold.png"));
//    forceLowerThresholdLabel->show();

////    forceTimeClock = new ShapedClock(this);
////    forceTimeClock->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
////    //forceTimeClock->show();
//    forceTimer = new QTimer(this);
//    forceTimer->setSingleShot(true);
//    QObject::connect(forceTimer, SIGNAL(timeout()), this, SLOT(forceMantainingTimeOk_slt()));
//    forceTimeLabel = new QLabel();
//    forceTimeLabel->setFrameStyle(QFrame::Panel | QFrame::Plain);
//    QFont labelFont( "Arial", 15, QFont::Bold);
//    forceTimeLabel->setFont(labelFont);
//    forceTimeLabel->setMinimumWidth(this->size().width()*0.1);
//    forceTimeLabel->setText(QString::number(float(forceTimer->remainingTime())/1000,'f',2));
//    forceBar->setFixedWidth(1000 - forceTimeLabel->minimumWidth());
//    forceBar->setFixedHeight(30);
//    setForceUpperThresholdLabel(forceBar->width()*0.8);
//    setForceLowerThresholdLabel(forceBar->width()*0.5);

//    //! [0]

//    //! [1]
//    //!
//    QHBoxLayout *forceLayout = new QHBoxLayout();
//    forceLayout->addWidget(forceBar);
//    forceLayout->addWidget(forceTimeLabel);
    //!



    int width = QApplication::desktop()->width();
    int height = QApplication::desktop()->height();
    setFixedSize(width,height);
    setAttribute(Qt::WA_DeleteOnClose);
}
//! [1]
Window::~Window()
{
        qDebug() << "relased window " ;
}
void Window::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        emit disconnectTouch();
        close();
    }
    else
        QWidget::keyPressEvent(e);
}

void Window::setWindowLayout( IlluminaLaCitta *test){
    mainLayout = new QGridLayout;
    //mainLayout->addLayout(forceLayout,0,0);
    mainLayout->setHorizontalSpacing(0);
    mainLayout->setContentsMargins (0, 1, 0, 0);
    mainLayout->setHorizontalSpacing(0);
    mainLayout->setOriginCorner(Qt::TopLeftCorner);
    mainLayout->addWidget(test);

    setLayout(mainLayout);

//    setForceUpperThresholdLabel(forceBar->width());
}

void Window::setForceBar( double pressure, bool inTarget )
{
    forceBar->setValue(pressure*100);
    if ( pressure*100 < lowerThreshold )
    {
        forceBar->setStyleSheet(forcebarstylesheetTime);
        if (forceTimer->isActive()) forceTimer->stop();
    }
    else if ( pressure*100 > upperThreshold )
    {
        forceBar->setStyleSheet(forcebarstylesheetNO);
        if (forceTimer->isActive()) forceTimer->stop();
    }
    else
    {
        forceBar->setStyleSheet(forcebarstylesheetOK);
        if (!forceTimer->isActive() && inTarget ) forceTimer->start();
    }
    if (forceTimer->isActive()) forceTimeLabel->setText(QString::number(float(forceTimer->remainingTime())/1000,'f',2));
    else forceTimeLabel->setText(QString::number(float(forceTimer->interval())/1000,'f',2));
}

void Window::setForceUpperThresholdLabel( int forceUpperThreshold_tmp )
{
    upperThreshold = forceUpperThreshold_tmp;
    forceUpperThresholdLabel->setGeometry(QRect(forceUpperThreshold_tmp,100,20,20));
}

void Window::setForceLowerThresholdLabel( int forceLowerThreshold_tmp )
{
    lowerThreshold = forceLowerThreshold_tmp;
    forceLowerThresholdLabel->setGeometry(QRect(forceLowerThreshold_tmp,100,20,20));
}

void Window::setForceRemainingtime( int forceRemainingtime_tmp )
{
    qDebug() << forceTimer->remainingTime() << "  " << forceRemainingtime_tmp;
    forceTimer->setInterval(forceRemainingtime_tmp);
    qDebug() << forceTimer->interval();
    forceTimeLabel->setText(QString::number(float(forceTimer->interval())/1000,'f',2));
}

void Window::setBarChart( BarChart *barChart_tmp, BarChart *barChart_tmp1, BarChart *barChart_tmp2)
{
        mainLayout->addWidget(barChart_tmp,1,0);
        mainLayout->addWidget(barChart_tmp1,1,1);
        mainLayout->addWidget(barChart_tmp2,2,0);
        QFont endTestFont( "Arial", 25, QFont::Bold);

        QPushButton *endTest = new QPushButton(tr("endTest"),this);
        endTest->setFont(endTestFont);
        endTest->setDefault(true);
        endTest->setEnabled(true);
        endTest->setGeometry(this->width()/2 + 40,this->height()/2,200,100);
        connect( endTest, SIGNAL( clicked () ),
                 this, SLOT( closeTest() ));
        mainLayout->addWidget(endTest,4,0);
}

void Window::forceMantainingTimeOk_slt()
{
    emit forceMantainingTimeOk_sng();
}

void Window::closeTest()
{
    emit disconnectTouch();
    close();
}

void Window::forceSelectionTrueFalse_slt( bool state_tmp )
{
    emit forceSelectionTrueFalseTargetSelected(state_tmp);
}

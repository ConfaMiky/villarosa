#include "plot.h"
#include <qwt_plot_zoomer.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_intervalcurve.h>
#include <qwt_legend.h>
#include <qwt_interval_symbol.h>
#include <qwt_symbol.h>
#include <qwt_series_data.h>
#include <qwt_text.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_renderer.h>
#include <qdatetime.h>

class Grid: public QwtPlotGrid
{
public:
    Grid()
    {
        enableXMin( true );
        setMajorPen( Qt::white, 0, Qt::DotLine );
        setMinorPen( Qt::gray, 0, Qt::DotLine );
    }

    virtual void updateScaleDiv( const QwtScaleDiv &xScaleDiv,
        const QwtScaleDiv &yScaleDiv )
    {
        QwtScaleDiv scaleDiv( xScaleDiv.lowerBound(), 
            xScaleDiv.upperBound() );

        scaleDiv.setTicks( QwtScaleDiv::MinorTick,
            xScaleDiv.ticks( QwtScaleDiv::MinorTick ) );
        scaleDiv.setTicks( QwtScaleDiv::MajorTick,
            xScaleDiv.ticks( QwtScaleDiv::MediumTick ) );

        QwtPlotGrid::updateScaleDiv( scaleDiv, yScaleDiv );
    }
};

class YearScaleDraw: public QwtScaleDraw
{
public:
    YearScaleDraw()
    {
        setTickLength( QwtScaleDiv::MajorTick, 0 );
        setTickLength( QwtScaleDiv::MinorTick, 0 );
        setTickLength( QwtScaleDiv::MediumTick, 6 );

        setLabelRotation( -60.0 );
        setLabelAlignment( Qt::AlignLeft | Qt::AlignVCenter );

        setSpacing( 15 );
    }

    virtual QwtText label( double value ) const
    {
        return QDate::longMonthName( int( value / 30 ) + 1 );
    }
};

Plot::Plot( QWidget *parent,QString fileName_tmp ):
    QwtPlot( parent )
{
    setObjectName( "FriedbergPlot" );
    setTitle( "Temperature of Friedberg/Germany" );

//    setAxisTitle( QwtPlot::xBottom, "2007" );
//    setAxisScaleDiv( QwtPlot::xBottom, yearScaleDiv() );
//    setAxisScaleDraw( QwtPlot::xBottom, new YearScaleDraw() );

//    setAxisTitle( QwtPlot::yLeft,
//        QString( "Temperature [%1C]" ).arg( QChar( 0x00B0 ) ) );

    QwtPlotCanvas *canvas = new QwtPlotCanvas();
    canvas->setPalette( Qt::darkGray );
    canvas->setBorderRadius( 10 );

    setCanvas( canvas );

    // grid
    QwtPlotGrid *grid = new Grid;
    grid->attach( this );

    insertLegend( new QwtLegend(), QwtPlot::RightLegend );

    QFile file(fileName_tmp);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        exit(2345);
        QMessageBox msgBox;
        msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }

    QTextStream in(&file);
    QString line;

    int ii = 0;
    in.seek(0);
    line = in.readLine();
    while (!line.isNull())
    {
        QStringList  fields = line.split("\t");
        ii++;
        line = in.readLine();
        qDebug() << "line n° " << ii;
    }
    double precision[ii-5];
    double timeToChkTarget[ii-5];
    double numberOfError[ii-5];
    int totNumberOfTarget;

    ii = 0;
    in.seek(0);
    line = in.readLine();
    while (!line.isNull())
    {
        if ( ii >= 5 )
        {
            QStringList  fields = line.split("\t");
            precision[ii-5] = sqrt( pow((fields[1].toDouble()-fields[3].toDouble()),2.0) + pow((fields[2].toDouble()-fields[4].toDouble()),2.0));
            timeToChkTarget[ii-5] = fields[5].toDouble();
            numberOfError[ii-5] = fields[6].toDouble();
            totNumberOfTarget = fields[7].toDouble();
            qDebug() << "parametri := " << precision[ii-5] << " " << timeToChkTarget[ii-5] << " " << numberOfError[ii-5] << " " << totNumberOfTarget;
    }
    ii++;
        line = in.readLine();

    }
    file.close();

    double precisionMean = 0;
    double timeToChkTargetMean = 0;
    double numberOfErrorMean = 0;
    double precisionMax = 0;
    double timeToChkTargetMax = 0;
    double numberOfErrorMax = 0;
    double precisionMin = precision[0];
    double timeToChkTargetMin = timeToChkTarget[0];
    double numberOfErrorMin = numberOfError[0];

    double totNumberOfTargetPerc = double(totNumberOfTarget)/ii;

    for ( int jj = 0; jj < ii-5 ; jj++)
    {
        if ( precision[jj] > precisionMax ) precisionMax = precision[jj];
        if ( precision[jj] < precisionMin ) precisionMin = precision[jj];
        if ( timeToChkTarget[jj] > timeToChkTargetMax ) timeToChkTargetMax = timeToChkTarget[jj];
        if ( timeToChkTarget[jj] < timeToChkTargetMin ) timeToChkTargetMin = timeToChkTarget[jj];
        if ( numberOfError[jj] > numberOfErrorMax ) numberOfErrorMax = numberOfError[jj];
        if ( numberOfError[jj] < numberOfErrorMin ) numberOfErrorMin = numberOfError[jj];
        precisionMean = precisionMean + precision[jj];
        timeToChkTargetMean = timeToChkTargetMean + timeToChkTarget[jj];
        numberOfErrorMean = numberOfErrorMean + numberOfError[jj];
    }
    precisionMean = precisionMean/(ii-5);
    timeToChkTargetMean =  timeToChkTargetMean/(ii-5);
    numberOfErrorMean = numberOfErrorMean / (ii-5);

    precisionMean = (precisionMean - 0)/(50-0)*(1-0)+0;
    precisionMax = (precisionMax - 0)/(50-0)*(1-0)+0;
    precisionMin = (precisionMin - 0)/(50-0)*(1-0)+0;

    timeToChkTargetMean = (timeToChkTargetMean - 1000)/(10000-1000)*(1-0)+0;
    timeToChkTargetMax = (timeToChkTargetMax - 1000)/(10000-1000)*(1-0)+0;
    timeToChkTargetMin = (timeToChkTargetMin - 1000)/(10000-1000)*(1-0)+0;

    numberOfErrorMean = (numberOfErrorMean - 0)/(10-0)*(1-0)+0;
    numberOfErrorMax = (numberOfErrorMax - 0)/(10-0)*(1-0)+0;
    numberOfErrorMin = (numberOfErrorMin - 0)/(10-0)*(1-0)+0;

    qDebug() << "parametri precision := " << precisionMean << " " << precisionMax << " " << precisionMin;
    qDebug() << "parametri time to get target:= " << timeToChkTargetMean << " " << timeToChkTargetMax << " " << timeToChkTargetMin;
    qDebug() << "parametri error n°:= " << numberOfErrorMean << " " << numberOfErrorMax << " " << numberOfErrorMin;

    QVector<QPointF> averageData( 4 );
    QVector<QwtIntervalSample> rangeData( 4 );

    averageData[0] = QPointF( double( 0 ),precisionMean );
    rangeData[0] = QwtIntervalSample( double( 0 ),
        QwtInterval( precisionMin, precisionMax ) );
    averageData[1] = QPointF( double( 1 ),timeToChkTargetMean );
    rangeData[1] = QwtIntervalSample( double( 1 ),
        QwtInterval( timeToChkTargetMin, timeToChkTargetMax ) );
    averageData[2] = QPointF( double( 2 ),numberOfErrorMean );
    rangeData[2] = QwtIntervalSample( double( 2 ),
        QwtInterval( numberOfErrorMin, numberOfErrorMax ) );
    averageData[3] = QPointF( double( 3 ),totNumberOfTargetPerc );
    rangeData[3] = QwtIntervalSample( double( 3 ),
        QwtInterval( 0, 0 ) );

    insertCurve( "Average", averageData, Qt::black );
    insertErrorBars( "Range", rangeData, Qt::blue );

    // LeftButton for the zooming
    // MidButton for the panning
    // RightButton: zoom out by 1
    // Ctrl+RighButton: zoom out to full size

    QwtPlotZoomer* zoomer = new QwtPlotZoomer( canvas );
    zoomer->setRubberBandPen( QColor( Qt::black ) );
    zoomer->setTrackerPen( QColor( Qt::black ) );
    zoomer->setMousePattern( QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlModifier );
    zoomer->setMousePattern( QwtEventPattern::MouseSelect3,
        Qt::RightButton );

    QwtPlotPanner *panner = new QwtPlotPanner( canvas );
    panner->setMouseButton( Qt::MidButton );
}

QwtScaleDiv Plot::yearScaleDiv() const
{
    const int days[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    QList<double> mediumTicks;
    mediumTicks += 0.0;
    for ( uint i = 0; i < sizeof( days ) / sizeof( days[0] ); i++ )
        mediumTicks += mediumTicks.last() + days[i];

    QList<double> minorTicks;
    for ( int i = 1; i <= 365; i += 7 )
        minorTicks += i;

    QList<double> majorTicks;
    for ( int i = 0; i < 12; i++ )
        majorTicks += i * 30 + 15;

    QwtScaleDiv scaleDiv( mediumTicks.first(), mediumTicks.last() + 1, 
        minorTicks, mediumTicks, majorTicks );
    return scaleDiv;
}

void Plot::insertCurve( const QString& title,
    const QVector<QPointF>& samples, const QColor &color )
{
    d_curve = new QwtPlotCurve( title );
    d_curve->setRenderHint( QwtPlotItem::RenderAntialiased );
    d_curve->setStyle( QwtPlotCurve::NoCurve );
    d_curve->setLegendAttribute( QwtPlotCurve::LegendShowSymbol );

    QwtSymbol *symbol = new QwtSymbol( QwtSymbol::XCross );
    symbol->setSize( 4 );
    symbol->setPen( color );
    d_curve->setSymbol( symbol );

    d_curve->setSamples( samples );
    d_curve->attach( this );
}

void Plot::insertErrorBars(
    const QString &title,
    const QVector<QwtIntervalSample>& samples,
    const QColor &color )
{
    d_intervalCurve = new QwtPlotIntervalCurve( title );
    d_intervalCurve->setRenderHint( QwtPlotItem::RenderAntialiased );
    d_intervalCurve->setPen( Qt::white );

    QColor bg( color );
    bg.setAlpha( 150 );
    d_intervalCurve->setBrush( QBrush( bg ) );
    d_intervalCurve->setStyle( QwtPlotIntervalCurve::Tube );
    setMode(1);

    d_intervalCurve->setSamples( samples );
    d_intervalCurve->attach( this );
}

void Plot::setMode( int style )
{
    if ( style == Tube )
    {
        d_intervalCurve->setStyle( QwtPlotIntervalCurve::Tube );
        d_intervalCurve->setSymbol( NULL );
        d_intervalCurve->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    }
    else
    {
        d_intervalCurve->setStyle( QwtPlotIntervalCurve::NoCurve );

        QColor c( d_intervalCurve->brush().color().rgb() ); // skip alpha

        QwtIntervalSymbol *errorBar =
            new QwtIntervalSymbol( QwtIntervalSymbol::Bar );
        errorBar->setWidth( 8 ); // should be something even
        errorBar->setPen( c );

        d_intervalCurve->setSymbol( errorBar );
        d_intervalCurve->setRenderHint( QwtPlotItem::RenderAntialiased, false );
    }

    replot();
}

void Plot::exportPlot()
{
    QwtPlotRenderer renderer;
    renderer.exportTo( this, "friedberg.pdf" );
}

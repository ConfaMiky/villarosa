#include "barchart.h"
#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_multi_barchart.h>
#include <qwt_column_symbol.h>
#include <qwt_plot_layout.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>


BarChart::BarChart( QWidget *parent,QString fileName_tmp, int indexColumn, QString titolo_tmp, QString xLabel, QString yLabel ):
    QwtPlot( parent )
{
    setAutoFillBackground( true );

    setPalette( Qt::white );
    //    canvas()->setPalette( QColor(255, 0, 255, 100) );

    //    setStyleSheet("background:transparent;");

    setTitle( "Bar Chart" );

    setAxisTitle( QwtPlot::yLeft, "Whatever" );
    setAxisTitle( QwtPlot::xBottom, "Target" );

    d_barChartItem = new QwtPlotMultiBarChart( "Bar Chart " );
    d_barChartItem->setLayoutPolicy( QwtPlotMultiBarChart::AutoAdjustSamples );
    d_barChartItem->setSpacing( 20 );
    d_barChartItem->setMargin( 3 );


    d_barChartItem->attach( this );



    insertLegend( new QwtLegend() );

    repopulate( fileName_tmp, indexColumn, titolo_tmp,  xLabel,  yLabel);

    setOrientation( 0 );

    setAutoReplot( true );
}

BarChart::BarChart( QWidget *parent,QString fileName_tmp, int indexColumn,int indexColumn1, QString titolo_tmp, QString xLabel, QString yLabel ):
    QwtPlot( parent )
{
    setAutoFillBackground( true );

    setPalette( Qt::white );
    //    canvas()->setPalette( QColor(255, 0, 255, 100) );

    //    setStyleSheet("background:transparent;");

    setTitle( "Bar Chart" );

    setAxisTitle( QwtPlot::yLeft, "Whatever" );
    setAxisTitle( QwtPlot::xBottom, "Target" );

    d_barChartItem = new QwtPlotMultiBarChart( "Bar Chart " );
    d_barChartItem->setLayoutPolicy( QwtPlotMultiBarChart::AutoAdjustSamples );
    d_barChartItem->setSpacing( 20 );
    d_barChartItem->setMargin( 3 );


    d_barChartItem->attach( this );



    insertLegend( new QwtLegend() );

    repopulate( fileName_tmp, indexColumn,indexColumn1, titolo_tmp,  xLabel,  yLabel);

    setOrientation( 0 );

    setAutoReplot( true );
}

BarChart::BarChart( QWidget *parent ):
    QwtPlot( parent )
{
    setAutoFillBackground( true );

    //setPalette( Qt::green );
    canvas()->setPalette( QColor(0, 0, 255, 127) );

    setTitle( "Bar Chart" );

    setAxisTitle( QwtPlot::yLeft, "Whatever" );
    setAxisTitle( QwtPlot::xBottom, "Whatever" );

    d_barChartItem = new QwtPlotMultiBarChart( "Bar Chart " );
    d_barChartItem->setLayoutPolicy( QwtPlotMultiBarChart::AutoAdjustSamples );
    d_barChartItem->setSpacing( 20 );
    d_barChartItem->setMargin( 3 );

    d_barChartItem->attach( this );

    insertLegend( new QwtLegend() );

    populate(d_barChartItem);
    setOrientation( 0 );

    setAutoReplot( true );
}

BarChart::~BarChart()
{
    qDebug() << "relased bar chart " ;
}

void BarChart::populate(QwtPlotMultiBarChart *d_barChartItem_tmp)
{
    static const char *colors[] = { "DarkOrchid" };

    const int numSamples = 3;
    const int numBars = sizeof( colors ) / sizeof( colors[0] );

    QList<QwtText> titles;
    for ( int i = 0; i < numBars; i++ )
    {
        QString title("Bar %1");
        titles += title.arg( i );
    }
    d_barChartItem_tmp->setBarTitles( titles );
    d_barChartItem_tmp->setLegendIconSize( QSize( 10, 14 ) );

    for ( int i = 0; i < numBars; i++ )
    {
        QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
        symbol->setLineWidth( 2 );
        symbol->setFrameStyle( QwtColumnSymbol::Raised );
        symbol->setPalette( QPalette( QColor(0, 0, 255, 127) ) );

        d_barChartItem_tmp->setSymbol( i, symbol );
    }
    
    QVector< QVector<double> > series;
    for ( int i = 0; i < numSamples; i++ )
    {
        QVector<double> values;
        for ( int j = 0; j < numBars; j++ )
            values += ( 2 + qrand() % 8 );

        series += values;
    }
    d_barChartItem_tmp->setSamples( series );
    this->replot();
}

void BarChart::setMode( int mode )
{
    if ( mode == 0 )
    {
        d_barChartItem->setStyle( QwtPlotMultiBarChart::Grouped );
    }
    else
    {
        d_barChartItem->setStyle( QwtPlotMultiBarChart::Stacked );
    }
}

void BarChart::setOrientation( int orientation )
{
    QwtPlot::Axis axis1, axis2, axis3;

    if ( orientation == 0 )
    {
        axis1 = QwtPlot::xBottom;
        axis2 = QwtPlot::yLeft;
        axis3 = QwtPlot::yLeft;

        d_barChartItem->setOrientation( Qt::Vertical );
    }
    else
    {
        axis1 = QwtPlot::yLeft;
        axis2 = QwtPlot::xBottom;
        axis3 = QwtPlot::xBottom;

        d_barChartItem->setOrientation( Qt::Horizontal );
    }

    setAxisScale( axis1, 0, d_barChartItem->dataSize() - 1, 1.0 );
    setAxisAutoScale( axis2 );
    setAxisAutoScale( axis3 );


    QwtScaleDraw *scaleDraw1 = axisScaleDraw( axis1 );
    scaleDraw1->enableComponent( QwtScaleDraw::Backbone, false );
    scaleDraw1->enableComponent( QwtScaleDraw::Ticks, false );

    QwtScaleDraw *scaleDraw2 = axisScaleDraw( axis2 );
    scaleDraw2->enableComponent( QwtScaleDraw::Backbone, true );
    scaleDraw2->enableComponent( QwtScaleDraw::Ticks, true );

    QwtScaleDraw *scaleDraw3 = axisScaleDraw( axis3 );
    scaleDraw2->enableComponent( QwtScaleDraw::Backbone, true );
    scaleDraw2->enableComponent( QwtScaleDraw::Ticks, true );

    plotLayout()->setAlignCanvasToScale( axis1, true );
    plotLayout()->setAlignCanvasToScale( axis2, true );
    plotLayout()->setAlignCanvasToScale( axis3, true );

    plotLayout()->setCanvasMargin( 0 );
    updateCanvasMargins();

    replot();
}

void BarChart::exportChart()
{
    QwtPlotRenderer renderer;
    renderer.exportTo( this, "barchart.pdf" );
}

void BarChart::repopulate(QString fileName_tmp, int indexColumn, QString titolo_tmp, QString xLabel, QString yLabel)
{
    QFile file(fileName_tmp);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        exit(2345);
        QMessageBox msgBox;
        msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }

    QTextStream in(&file);
    QString line;
    QVector< QVector<double> > series;

    int ii = 0;
    line = in.readLine();
    while (!line.isNull())
    {
        if ( ii >= 5 && !line.isNull())
        {
            QStringList  fields = line.split("\t");
            QVector<double> values;
            values += fields[indexColumn].toDouble();
            series += values;
        }
        ii++;
        line = in.readLine();
        qDebug() << line;
    }
    file.close();

    this->setAxisTitle( QwtPlot::yLeft, yLabel );
    setAxisTitle( QwtPlot::xBottom, xLabel );
    this->setTitle(titolo_tmp);

    static const char *colors[] = { "DarkOrchid", "SteelBlue" };

    const int numBars = 1;//sizeof( colors ) / sizeof( colors[0] );

    QList<QwtText> titles;
    for ( int i = 0; i < numBars; i++ )
    {
        QString title("Paziente %1");
        titles += title.arg( i );
    }
    d_barChartItem->setBarTitles( titles );
    d_barChartItem->setLegendIconSize( QSize( 10, 14 ) );

    for ( int i = 0; i < numBars; i++ )
    {
        QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
        symbol->setLineWidth( 2 );
        symbol->setFrameStyle( QwtColumnSymbol::Raised );
        symbol->setPalette( QPalette( QColor(0, 0, 255, 120) ) );
        d_barChartItem->setSymbol( i, symbol );
    }
    d_barChartItem->setSamples( series );
    this->replot();
}

void BarChart::repopulate(QString fileName_tmp, int indexColumn, int indexColumn1, QString titolo_tmp, QString xLabel, QString yLabel)
{
    QFile file(fileName_tmp);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        exit(2345);
        QMessageBox msgBox;
        msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }

    QTextStream in(&file);
    QString line;
    QVector< QVector<double> > series;

    int ii = 0;
    line = in.readLine();
    while (!line.isNull())
    {
        double value = 0;
        if ( ii >= 5 && !line.isNull())
        {
            QStringList  fields = line.split("\t");
            value = fields[indexColumn1].toDouble();
        }
        ii++;
        line = in.readLine();
        if ( line.isNull() )
        {
            QVector<double> values;
            values += ii-5;
            series += values;
            values.clear();
            values += value;
            series += values;
        }
    }
    file.close();

//    this->setAxisTitle( 0, yLabel );
//    this->setAxisTitle( 1, "yLabel" );
//    this->setAxisTitle( 2, "y234" );
//    setAxisTitle( QwtPlot::xBottom, xLabel );
    this->setTitle(titolo_tmp);

    static const char *colors[] = { "DarkOrchid", "SteelBlue" };

    const int numBars = 1;//sizeof( colors ) / sizeof( colors[0] );

    QList<QwtText> titles;
    for ( int i = 0; i < numBars; i++ )
    {
        QString title("Paziente %1");
        titles += title.arg( i );
    }
    d_barChartItem->setBarTitles( titles );
    d_barChartItem->setLegendIconSize( QSize( 10, 14 ) );

    for ( int i = 0; i < numBars; i++ )
    {
        QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
        symbol->setLineWidth( 2 );
        symbol->setFrameStyle( QwtColumnSymbol::Raised );
        symbol->setPalette( QPalette( QColor(0, 0, 255, 120) ) );
        d_barChartItem->setSymbol( i, symbol );
    }

    d_barChartItem->setSamples( series );
    this->replot();
}

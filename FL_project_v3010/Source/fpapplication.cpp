#include <QtWidgets>
#include "fpapplication.h"
#include "QMessageBox"


FILE *stream_Coeff_touch,*stream_Coeff_forza, *fopen();

FpApplication::FpApplication(int &argv, char **args)
    : QApplication(argv, args)
{
    finestra = new QWidget();
    finestra->setGeometry(0,0,130,20);

    qDebug()<<QDir::currentPath();
    if ( ( stream_Coeff_touch = fopen ( "TaraturaTocco/coefficenti.txt", "r" ) ) == NULL ) exit(100);//QMessageBox::warning(this, tr("Warning"),tr("File dei coefficenti del touch non trovato."));
    while ( !feof ( stream_Coeff_touch ) )
    {
        char str [100] ;
        fgets ( str, 100, stream_Coeff_touch) ;
        sscanf ( str, "%f\t%f\t%f\t%f\n", &A_x, &B_x, &A_y, &B_y ) ;
    }

    if ( ( stream_Coeff_forza = fopen ( "TaraturaForze/coefficienti.txt", "r" ) ) == NULL ) exit(101);//QMessageBox::warning(this, tr("Warning"),tr("File dei coefficenti delle celle di carico non trovato."));
    while ( !feof ( stream_Coeff_forza ) )
    {
        char str [1000] ;
        fgets ( str, 1000, stream_Coeff_forza) ;
        sscanf ( str, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", &A_f, &B_f, &C_f, &D_f, &E_f, &F_f, &G_f, &H_f, &I_f, &J_f ) ;
    }

    forcetriggercounter = 0;
    defaultCountdown = 1;
countdown = defaultCountdown;
countdownstring.sprintf("%3.2f",countdown);
    forcebarstylesheetOK = "QProgressBar {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
            "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1F8E0, stop: 1 #74DF00)}";

    forcebarstylesheetNO = "QProgressBar {color: white; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
            "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F5A9A9, stop: 1 #FF0000)}";
    maxForceTsh = 80;
    minForceTsh = 30;
    forceProgressBar = new QProgressBar();
    // force bar set up
    forceProgressBar->setValue(0);
    forceProgressBar->setGeometry(25,0,finestra->width()*0.8,finestra->height()*0.5);
    forceProgressBar->setMaximum(100);
    forceProgressBar->setStyleSheet(forcebarstylesheetNO);
    forceProgressBar->show();

    // force threshold draw
    // min
    QLabel* minforcethreshold = new QLabel();
    minforcethreshold->setGeometry(forceProgressBar->width()/100*minForceTsh,0,22,19);
    minforcethreshold->setStyleSheet("background-image: url(:/resources/forcethreshold.png)");
    minforcethreshold->setParent(finestra);
    minforcethreshold->show();
    // max
    QLabel* maxforcethreshold = new QLabel();
    maxforcethreshold->setGeometry(forceProgressBar->width()/100*maxForceTsh,0,22,19);
    maxforcethreshold->setStyleSheet("background-image: url(:/resources/forcethreshold.png)");
    maxforcethreshold->setParent(finestra);
    maxforcethreshold->show();

    countdownlabel = new QLabel();
    countdownlabel->setGeometry(0,0,25,finestra->height()*0.5);
    countdownlabel->setText(countdownstring);
    countdownlabel->setStyleSheet("color: black");

    COM_inizializzata = init_COM();
    QString COM_Init;
    COM_Init = COM_Init.setNum(COM_inizializzata);
    if (!COM_inizializzata)
    {
        FPtimer = new QTimer(this);
        QObject::connect(FPtimer, SIGNAL(timeout()), this, SLOT(updateFP()));
        QObject::connect(this, SIGNAL(close()), this, SLOT(ChiudiCOM()) );
        FPtimer->start();
    }
    touch.tara_1 = 0;
    touch.tara_2 = 0;
    touch.tara_3 = 0;
    if (!COM_inizializzata)
    {

        for ( int ii = 0; ii < 100; ii++ )
        {
            lettura(&touch.x,&touch.y,&touch.f1,&touch.f2,&touch.f3,&touch.mDito,&touch.ID_Arduino, A_x, B_y, A_y, B_y);
            touch.tara_1 += touch.f1;
            touch.tara_2 += touch.f2;
            touch.tara_3 += touch.f3;
        }
        touch.tara_1 = touch.tara_1/100;
        touch.tara_2 = touch.tara_2/100;
        touch.tara_3 = touch.tara_3/100;
        qDebug()<<touch.tara_1 << "   " <<touch.tara_2 << "   " <<touch.tara_3 << "   " ;
        qDebug()<<touch.tara_1-touch.f1 << "   " <<touch.tara_2-touch.f2 << "   " <<touch.tara_3-touch.f3 << "   " ;
        //exit(900);
        //QApplication::sendEvent(this, &event);
    }
    // create a grid layot where draw the GUI
    QGridLayout *mainLayout = new QGridLayout;
    //mainLayout->setColumnStretch(0, 1);
    mainLayout->setColumnStretch(1, 3);
    mainLayout->addWidget(forceProgressBar,0,0);
    mainLayout->addWidget(countdownlabel,1,0);
    finestra->setLayout(mainLayout);
    finestra->show();
    finestra->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

}
void FpApplication::updateFP()
{
    //int width = 1000;
    lettura(&touch.x,&touch.y,&touch.f1,&touch.f2,&touch.f3,&touch.mDito,&touch.ID_Arduino, A_x, B_y, A_y, B_y);

    if ( touch.mDito ){
        QPoint pos;
        pos.setX(touch.x);
        pos.setY(touch.y);
        touch.cursoreFP.setPos(pos);
        QPoint posFinestra;
        posFinestra.setX(touch.x-finestra->width()/2);
        posFinestra.setY(touch.y-finestra->height()*2);

        finestra->move(posFinestra);
        touch.fTot = (A_f + B_f * touch.x + C_f * touch.y + D_f * touch.x*touch.x + E_f *touch.x*touch.y + F_f * touch.y*touch.y + G_f * (touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) + H_f * (touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) * touch.x + I_f * (touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) * touch.y + J_f *(touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) * touch.y * touch.x)/18*100*forceProgressBar->width()/100 ;
        qDebug()<<touch.fTot<<" " << maxForceTsh*10 << " " << minForceTsh*10;
        // set the force value to the force bar
//        forceProgressBar->setGeometry(0,0,finestra->width(),finestra->height()*0.5);

        forceProgressBar->setValue( touch.fTot );
        //SetWindowPos(forceProgressBar,HWND_TOPMOST,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
        finestra->show();
        if( forceProgressBar->value()<maxForceTsh && forceProgressBar->value()>minForceTsh ){ // se la forza è tra le due soglie
            forceProgressBar->setStyleSheet(forcebarstylesheetOK);
        }else{                                                                                      // se la forza è fuori soglia
            forceProgressBar->setStyleSheet(forcebarstylesheetNO);                                      // setto lo stile della barra della forza come rosso
        }
        if(touch.fTot>minForceTsh && touch.fTot<maxForceTsh ){// the user has just touched the panel -> simulate a mouse press event through windows library (http://msdn.microsoft.com/en-us/library/windows/desktop/ms646260%28v=vs.85%29.aspx)
            mouse_event(MOUSEEVENTF_LEFTDOWN,touch.x, touch.y, 0,0);
            forcetriggercounter += 10;                                                              // incremento il contatore di tenuta della forza
            countdownstring.sprintf("%3.2f",(countdown-float(forcetriggercounter)/1000)); // stringa del countdown che si ferma allo 0
            countdownlabel->setText(countdownstring);
            if (forcetriggercounter > countdown*1000)
            {
                //exit(900);
                mouse_event(MOUSEEVENTF_LEFTUP,touch.x, touch.y, 0,0);
                countdown = defaultCountdown;                                                 // resetto il countdown
                countdownstring.sprintf("%3.2f",countdown);
                countdownlabel->setText(countdownstring);
                forcetriggercounter = 0;

            }

        }else if(touch.fTot<minForceTsh*10 || touch.fTot>maxForceTsh*10 ){ // the user just release the finger from the panel, release the simulated click
            //mouse_event(MOUSEEVENTF_LEFTUP,touch.x, touch.y, 0,0);
            countdown = defaultCountdown;                                                 // resetto il countdown
            countdownstring.sprintf("%3.2f",countdown);
            countdownlabel->setText(countdownstring);
            forcetriggercounter = 0;

        }
    }
    else
    {
        forceProgressBar->setValue(0);
        countdown = defaultCountdown;                                                 // resetto il countdown
        countdownstring.sprintf("%3.2f",countdown);
        countdownlabel->setText(countdownstring);
        forcetriggercounter = 0;
    }
}

void FpApplication::ChiudiCOM(){
    close_COM_File();
}

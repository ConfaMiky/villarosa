#include "Header/forcebar.h"

ForceBar::ForceBar(QWidget *parent)
{
    forcebarstylesheetOK = "QProgressBar {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
                           "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1F8E0, stop: 1 #74DF00)}";

    forcebarstylesheetTime = "QProgressBar {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
                             "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F9FF30, stop: 1 #F7FF00)}";

    forcebarstylesheetNO =  "QProgressBar  {color: black; border: 0px solid black; border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #424242, stop: 1 black)}"
                            "QProgressBar::chunk {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F5A9A9, stop: 1 #FF0000)}";

    forcebarstylesheetTimerNo = "QLabel {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F5A9A9, stop: 1 #FF0000)}";
    forcebarstylesheetTimerOk = "QLabel {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1F8E0, stop: 1 #74DF00)}";
    forcebarstylesheetTimerWait = "QLabel {border-radius: 7px; background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F9FF30, stop: 1 #F7FF00)}";

    forceBar = new QProgressBar(parent);
    forceBar->setStyleSheet(forcebarstylesheetOK);
    forceBar->setGeometry(parent->width()*0.5 + 30 ,parent->height()*0.01,parent->width()*0.5 - 30,parent->height()*0.05);

    forceBarInverted = new QProgressBar(parent);
    forceBarInverted->setStyleSheet(forcebarstylesheetOK);
    forceBarInverted->setGeometry(0 ,parent->height()*0.01,parent->width()*0.5 - 30,parent->height()*0.05);
    forceBarInverted->setInvertedAppearance(true);

    forceUpperThresholdLabel = new QLabel(parent);
    forceUpperThresholdLabel->setGeometry(QRect(parent->width()*0.5 + 30,parent->height()*0.015,22,22));
    forceUpperThresholdLabel->setStyleSheet("QLabel {color : red} ");
    forceUpperThresholdLabel->setPixmap(QPixmap("C:/Users/JohnMancio/Documents/DOTTORATO/VillaRosa/FL-project_v0309/debug/Image/General/forcethreshold.png"));
    //    forceUpperThresholdLabel->setPixmap(QPixmap("../../Image/General/forcethreshold.png"));
    forceUpperThresholdLabel->show();
    forceLowerThresholdLabel = new QLabel(parent);
    forceLowerThresholdLabel->setGeometry(QRect(parent->width()*0.5 + 30,parent->height()*0.015,22,22));
    forceLowerThresholdLabel->setStyleSheet("QLabel {color : red} ");
    //    forceLowerThresholdLabel->setPixmap(QPixmap("C:/Users/John/Documents/FL_project_vBase/Image/General/forcethreshold.png"));
    forceLowerThresholdLabel->setPixmap(QPixmap("../../Image/General/forcethreshold.png"));
    forceLowerThresholdLabel->show();

    forceUpperThresholdInvertedLabel = new QLabel(parent);
    forceUpperThresholdInvertedLabel->setGeometry(QRect(parent->width()*0.5 - 30 - 22,parent->height()*0.015,22,22));
    forceUpperThresholdInvertedLabel->setStyleSheet("QLabel {color : red} ");
    forceUpperThresholdInvertedLabel->setPixmap(QPixmap("C:/Users/JohnMancio/Documents/DOTTORATO/VillaRosa/FL-project_v0309/debug/Image/General/forcethreshold.png"));
    //    forceUpperThresholdLabel->setPixmap(QPixmap("../../Image/General/forcethreshold.png"));
    forceUpperThresholdInvertedLabel->show();
    forceLowerThresholdInvertedLabel = new QLabel(parent);
    forceLowerThresholdInvertedLabel->setGeometry(QRect(parent->width()*0.5 - 30 - 22,parent->height()*0.015,22,22));
    forceLowerThresholdInvertedLabel->setStyleSheet("QLabel {color : red} ");
    //    forceLowerThresholdLabel->setPixmap(QPixmap("C:/Users/John/Documents/FL_project_vBase/Image/General/forcethreshold.png"));
    forceLowerThresholdInvertedLabel->setPixmap(QPixmap("../../Image/General/forcethreshold.png"));
    forceLowerThresholdInvertedLabel->show();

    forceTimer = new QTimer(this);
    forceTimer->setSingleShot(true);
    QObject::connect(forceTimer, SIGNAL(timeout()), this, SLOT(forceMantainingTimeOk_slt()));

    forceTimerFeedback = new QTimer(this);
    forceTimerFeedback->setSingleShot(true);
    forceTimerFeedback->setInterval(500);
    QObject::connect(forceTimerFeedback, SIGNAL(timeout()), this, SLOT(resetForceTimerFeedback()));

    forceTimeLabel = new QLabel(parent);
    //forceTimeLabel->setFrameStyle(QFrame::Panel | QFrame::Plain);
    QFont labelFont( "Arial", 18, QFont::Bold);
    forceTimeLabel->setFont(labelFont);
    //forceTimeLabel->setMinimumWidth(this->size().width()*0.1);
    forceTimeLabel->setGeometry(QRect(parent->width()*0.5-25,parent->height()*0.01,50,40));
    forceTimeLabel->setText(QString::number(float(forceTimer->remainingTime())/1000,'f',2));
    forceTimeLabel->setStyleSheet(forcebarstylesheetTimerNo);

    forceSelectionTrueFalseLabel = new QLabel(parent);
    //forceTimeLabel->setFrameStyle(QFrame::Panel | QFrame::Plain);
    forceSelectionTrueFalseLabel->setFont(labelFont);
    //forceTimeLabel->setMinimumWidth(this->size().width()*0.1);
    forceSelectionTrueFalseLabel->setGeometry(QRect(parent->width()*0.5-24,forceBar->height()+30,48,48));
    forceSelectionTrueFalseLabel->setPixmap(QPixmap("../../Image/General/force_Wait.png"));
    forceSelectionTrueFalseLabel->setStyleSheet(forcebarstylesheetTimerWait);

    width = parent->width();
    height = parent->height();

    setAttribute(Qt::WA_DeleteOnClose);
}

ForceBar::~ForceBar()
{
    qDebug() << "force bar released";
}

void ForceBar::setForceBar( double pressure, bool inTarget )
{
    forceBar->setValue(pressure*100);
    forceBarInverted->setValue(pressure*100);
    if ( pressure*100 < lowerThreshold )
    {
        forceBar->setStyleSheet(forcebarstylesheetTime);
        forceBarInverted->setStyleSheet(forcebarstylesheetTime);
        if (forceTimer->isActive()) forceTimer->stop();
        forceTimeLabel->setStyleSheet(forcebarstylesheetTimerNo);
    }
    else if ( pressure*100 > upperThreshold )
    {
        forceBar->setStyleSheet(forcebarstylesheetNO);
        forceBarInverted->setStyleSheet(forcebarstylesheetNO);
        if (forceTimer->isActive()) forceTimer->stop();
        forceTimeLabel->setStyleSheet(forcebarstylesheetTimerNo);
    }
    else
    {
        forceBar->setStyleSheet(forcebarstylesheetOK);
        forceBarInverted->setStyleSheet(forcebarstylesheetOK);
        if (!forceTimer->isActive() && inTarget ) forceTimer->start();
        forceTimeLabel->setStyleSheet(forcebarstylesheetTimerOk);
    }
    forceTimeLabel->clear();
    if (forceTimer->isActive()) forceTimeLabel->setText(QString::number(float(forceTimer->remainingTime())/1000,'f',2));
    else forceTimeLabel->setText(QString::number(float(forceTimer->interval())/1000,'f',2));
}

void ForceBar::forceBarShow()
{
    forceBar->show();
    forceBarInverted->show();
}

void ForceBar::setForceUpperThreshold( int forceUpperThreshold_tmp )
{
    upperThreshold = forceUpperThreshold_tmp;
    forceUpperThresholdLabel->setGeometry(QRect((width/2)+30+float(forceUpperThreshold_tmp)/100*(width/2-30)-11,768*0.015,22,22));
    forceUpperThresholdInvertedLabel->setGeometry(QRect((width/2)-30-22-float(forceUpperThreshold_tmp)/100*(width/2-30)-11,768*0.015,22,22));
}

void ForceBar::setForceLowerThreshold( int forceLowerThreshold_tmp )
{
    lowerThreshold = forceLowerThreshold_tmp;
    forceLowerThresholdLabel->setGeometry(QRect((width/2)+30+float(forceLowerThreshold_tmp)/100*(width/2-30)-11,768*0.015,22,22));
    forceLowerThresholdInvertedLabel->setGeometry(QRect((width/2)-30-22-float(forceLowerThreshold_tmp)/100*(width/2-30)-11,768*0.015,22,22));
}

void ForceBar::setForceRemainingtime( int forceRemainingtime_tmp )
{
    qDebug() << forceTimer->remainingTime() << "  " << forceRemainingtime_tmp;
    forceTimer->setInterval(forceRemainingtime_tmp);
    qDebug() << forceTimer->interval();
    forceTimeLabel->setText(QString::number(float(forceTimer->interval())/1000,'f',2));
}

void ForceBar::forceMantainingTimeOk_slt()
{
    emit forceMantainingTimeOk_sng();
}

void ForceBar::forceSelectionTrueFalse_slt( bool targetSelection_tmp)
{
    if ( targetSelection_tmp )
    {
        forceSelectionTrueFalseLabel->setStyleSheet(forcebarstylesheetTimerOk);
        forceSelectionTrueFalseLabel->setPixmap(QPixmap("../../Image/General/force_OK.png"));
        forceTimerFeedback->start();
    }
    else
    {
        forceSelectionTrueFalseLabel->setStyleSheet(forcebarstylesheetTimerNo);
        forceSelectionTrueFalseLabel->setPixmap(QPixmap("../../Image/General/force_NO.png"));
        forceTimerFeedback->start();
    }
}

void ForceBar::resetForceTimerFeedback()
{
    forceSelectionTrueFalseLabel->setStyleSheet(forcebarstylesheetTimerWait);
    forceSelectionTrueFalseLabel->setPixmap(QPixmap("../../Image/General/force_Wait.png"));
}

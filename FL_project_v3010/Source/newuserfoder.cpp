#include "newuserfoder.h"

NewUserFolder::NewUserFolder(QWidget *parent) :
    QDialog(parent)
{
    QDir::setCurrent(QCoreApplication::applicationDirPath());
    qDebug()<< "Apro la finestra la directory ?: " <<QDir::currentPath();
    nameLabel = new QLabel(tr("NOME:"));
    nameInput = new QLineEdit;
    nameLabel->setBuddy(nameInput);
    surnameLabel = new QLabel(tr("COGNOME:"));
    surnameInput = new QLineEdit;
    surnameLabel->setBuddy(nameInput);
    diseaseLabel = new QLabel(tr("DISABILITA':"));
    diseaseInput = new QLineEdit;
    diseaseLabel->setBuddy(diseaseInput);
    dateLabel = new QLabel();
    dateEdit = new QDateEdit(QDate::currentDate());
    dateLabel->setText(tr("Data di nascita: "));

    saveButton = new QPushButton(tr("&Save"));
    saveButton->setEnabled(true);
    saveButton->setMaximumWidth(105);

    closeButton = new QPushButton(tr("&Close"));
    closeButton->setEnabled(true);
    closeButton->setMaximumWidth(105);

    QHBoxLayout *topLeftLayout = new QHBoxLayout;
    topLeftLayout->addWidget(nameLabel);
    topLeftLayout->addWidget(nameInput);
    topLeftLayout->addWidget(surnameLabel);
    topLeftLayout->addWidget(surnameInput);
    topLeftLayout->addWidget(diseaseLabel);
    topLeftLayout->addWidget(diseaseInput);
    topLeftLayout->addWidget(dateLabel);
    topLeftLayout->addWidget(dateEdit);

    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->addLayout(topLeftLayout);

    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->addWidget(saveButton);
    rightLayout->addWidget(closeButton);
    rightLayout->addStretch();

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);
    setLayout(mainLayout);
    QObject::connect(saveButton, SIGNAL(clicked()), this, SLOT(createFolder()) );
    QObject::connect(closeButton, SIGNAL(clicked()), this, SLOT(close()) );
}

int NewUserFolder::createFolder()
{
    qDebug()<< "lancio createFolder() la directory ?: " <<QDir::currentPath();
    if ( nameInput->text().isEmpty() == false && surnameInput->text().isEmpty() == false )
    {
        int newSubjectId = 0;
        QDirIterator directories("User", QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot );
        while(directories.hasNext())
        {
            directories.next();
            newSubjectId++;
            qDebug() << directories.fileName();
            if ( nameInput->text() + surnameInput->text() == directories.fileName() )
            {
                userAlreadyExist();
            //exit(88);
            return 1;
            }
        }
        newSubjectId++;
        QDir dir = QDir::root();
        std::string stringa = dir.currentPath().toStdString();
        QString stringa_tmp = ( "User/"+ nameInput->text()+ surnameInput->text());
        stringa.append ("/User/" + nameInput->text().toStdString()+ surnameInput->text().toStdString());
        dir.mkdir(stringa.c_str());
        QDir dir_tmp = (QDir::currentPath() + "/" + stringa_tmp);
        if ( QDir::setCurrent(dir_tmp.absolutePath()) == true )
        {
            QFile file("Anagrafe.txt");
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return 0;

            QTextStream out(&file);
            out << "Nome:"<< "\t" << nameInput->text() << "\n";
            out << "Cognome:"<< "\t" << surnameInput->text() << "\n";
            out << "Anno di nascita:"<< "\t" << dateEdit->text() << "\n";
            out << "Disabilita:"<< "\t" << diseaseInput->text() << "\n";
        }
        stringa_tmp = QDir::currentPath();
        stringa_tmp.remove(stringa_tmp.length() - 16, 16 );
        if ( QDir::setCurrent( stringa_tmp) == true ) qDebug() << "tronati alla condizione precedente " << dir.currentPath() << "\n";
        updateEvent();
    }
    else dataIncomplete();
}

void NewUserFolder::closeEvent(QCloseEvent *event)
{
    int newSubjectId = 0;
    qDebug()<< "lancio createFolder() la directory ?: " <<QDir::currentPath();
    QDirIterator directories("User", QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot );
    while(directories.hasNext())
    {
        directories.next();
        newSubjectId++;
    }
    qDebug()<<newSubjectId;
    QString stringa_tmp = "Subject000"+QString::number(newSubjectId);
    if (newSubjectId < 10 )
    {
        stringa_tmp = ( "Subject000"+ QString::number(newSubjectId));
    }
    else if ( newSubjectId < 100 )
    {
        stringa_tmp = ( "Subject00"+ QString::number(newSubjectId));
    }
    else if ( newSubjectId < 1000 )
    {
        stringa_tmp = ( "Subject0"+ QString::number(newSubjectId));

    }
    else
    {
        stringa_tmp = ( "Subject"+ QString::number(newSubjectId));
    }
    emit closeFolder();
}


void NewUserFolder::updateEvent()
{
    int newSubjectId = 0;
    qDebug()<< "Lancio updateEvent() la directory ?: " <<QDir::currentPath();
    QDirIterator directories("./User", QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot );
    while(directories.hasNext())
    {
        directories.next();
        newSubjectId++;
    }
    if ( nameInput->text().isEmpty() == false && surnameInput->text().isEmpty() == false ) emit updateFolder(nameInput->text() + surnameInput->text());
}

void NewUserFolder::dataIncomplete()
{
    QMessageBox msgBox;
    msgBox.setText("Dati incompleti.Inserire almeno nome e cognome");
    msgBox.setInformativeText("");
    msgBox.setStandardButtons(QMessageBox::Ok );
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Ok:
        break;
    case QMessageBox::Discard:
        break;
    case QMessageBox::Cancel:
        break;
    default:
        break;
    }
}

void NewUserFolder::userAlreadyExist()
{
    QMessageBox msgBox;
    msgBox.setText("L'utente è gia stato memorizzato.");
    msgBox.setInformativeText("");
    msgBox.setStandardButtons(QMessageBox::Ok );
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Ok:
        break;
    case QMessageBox::Discard:
        break;
    case QMessageBox::Cancel:
        break;
    default:
        break;
    }
}

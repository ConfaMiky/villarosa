#include "ftforcetest.h"
#include <QKeyEvent>
#include <QDebug>
#include <QDateTime>

// Constructor
FtForceTest::FtForceTest(double Tc_tmp, int storedSignalChk_tmp,int storedDisturbChk_tmp,
                         int durataTest_tmp,
                         float* colorBackground_tmp,
                         float* signalColor_tmp,
                         float* disturbColor_tmp,
                         float* touchColor_tmp,
                         float* limitColor_tmp,
                         int signalTargetWidth_tmp,
                         int disturbTargetWidth_tmp,
                         int touchTargetWidth_tmp,
                         int limitTargetWidth_tmp,
                         int disturbYN_tmp, QString signalFileName_tmp ,QString disturbFileName_tmp,QString fileName,
                         double tAtteso_tmp,
                         double tAttesoDisturbo_tmp,
                         int LFPmeasuring_tmp){

    qsrand(QDateTime::currentMSecsSinceEpoch());
    testTimer = new QTimer(this);
    connect(testTimer, SIGNAL(timeout()), this, SLOT(updateGL()));
    testTimer->start(4);

    pressureOffset = 0;
    pressureReal = 0;
    // impostare le variabili relative alla opzioni del test
    tAtteso = tAtteso_tmp;
    tAttesoDisturbo = tAttesoDisturbo_tmp,
            durataTest = durataTest_tmp;
    LFPmeasuring = LFPmeasuring_tmp;
    displayPoint = 0;
    TempoSequenza = 10 ; // minuti
    Tc = Tc_tmp ;
    signalTargetWidth = signalTargetWidth_tmp;
    disturbTargetWidth = disturbTargetWidth_tmp;
    limitTargetWidth = limitTargetWidth_tmp;
    touchTargetWidth = touchTargetWidth_tmp;
    puntoTaraturaDimension = 40;
    for (  int ii = 0; ii <= 3; ii++ )
    {
        backgroundColor[ii] = float(colorBackground_tmp[ii])/100;
        signalColor[ii] = float(signalColor_tmp[ii])/100;
        disturbColor[ii] = float(disturbColor_tmp[ii])/100;
        touchColor[ii] = float(touchColor_tmp[ii])/100;
        limitColor[ii] = float(limitColor_tmp[ii])/100;
    }
    //    backgroundColor[3] = 1;
    //    signalColor[3]= 0.5;
    //    disturbColor[3] = 1 ;
    //    touchColor[3] = 1;
    //    limitColor[3] = 1;
    TcLimite = 0;
    TcDisturb = 0;
    TcSignal = 0;
    Tempo_precLimite = 0;
    iiLimite = 1;

    disturbYN = disturbYN_tmp;
    //    Ta = Ta_tmp;
    storedSignalChk = storedSignalChk_tmp;
    storedDisturbChk = storedDisturbChk_tmp;

    if ( storedSignalChk == 1 )
    {
        signal = loadSignal(signalFileName_tmp, signal);
        TcSignal = signal[0];
    }
    else
    {
        int N_campioni = TempoSequenza * 60 / (Tc/1000) ;
        signal = (double *) malloc(N_campioni * sizeof(double));
        signalEnd = N_campioni;
    }
    if ( storedDisturbChk == 1 )
    {
        signalDisturb = loadSignal(disturbFileName_tmp, signalDisturb);
        TcDisturb = signalDisturb[0];
    }
    else
    {
        int N_campioni = TempoSequenza * 60 / (Tc/1000) ;
        signalDisturb = (double *) malloc(N_campioni * sizeof(double));
    }
    fileOutput.setFileName(fileName+"/"+QDateTime::currentDateTime().toString("yyyyMMddhhmmss")+".txt");
    qDebug()<<fileOutput.fileName();
    if (!fileOutput.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("non � possibile aprire il file %1").arg(fileOutput.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }
    // imposto il valore delle variabili iniziali
    stato = 0;
    Limiti_pixel[0] = 0;

    setMouseTracking(TRUE);
    glDrawBuffer(true);
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NoSystemBackground);
    setAutoBufferSwap(true);
    timeLimit.start();
    setWindowTitle("FORCE TEST");
    setAttribute(Qt::WA_DeleteOnClose);
    this->doneCurrent();
}

// Empty destructor
FtForceTest::~FtForceTest() {
    free (signal);
    free (signalDisturb);
}

// Initialize OpenGL
void FtForceTest::initializeGL() {
    QPoint *pointProva = new QPoint ( signal[1],500);
    int dimensionTarget[2] = { 50 - signalTargetWidth , 50};
    int dimensionTargetFill[2] = { 50 - signalTargetWidth, 50 - signalTargetWidth  };
    int dimensionDisturb[2] = { 50 - disturbTargetWidth , 50};
    int dimensionTouch[2] = { 50 - touchTargetWidth , 50};
    float color[3] = {0.5,1,1};
    float color_chk[3] = {0.5,1,0};
    target = new Target( *pointProva, dimensionTarget, dimensionTarget, 2, false, signalColor, signalColor, signalColor, -1, 0, texture[0]);
    dimensionTarget[1] = signalTargetWidth;
    targetFill = new Target( *pointProva, dimensionTargetFill, dimensionTargetFill, 1, false, signalColor, signalColor, signalColor, -1, 0, texture[0]);
    target->startTime();
    float color01[3] = {1,0,0};
    float color_chk01[3] = {1,0,0};
    disturb = new Target( *pointProva, dimensionDisturb, dimensionDisturb, 2, false, disturbColor, disturbColor, disturbColor, -1, 0, texture[0]);
    disturb->startTime();
    int dimension[2] = { 20,20 };
    touchTarget = new Target( *pointProva, dimensionTouch, dimensionTouch, 2, false, touchColor, touchColor, touchColor, -1, 0, texture[0]);
    touchTarget->startTime();
    glShadeModel(GL_SMOOTH); // Enable smooth shading
    glClearColor(1,1,1,0); // Set the clear color to a black background

    glClearDepth(1.0f); // Depth buffer setup
    glEnable(GL_DEPTH_TEST); // Enable depth Lineing
    glDepthFunc(GL_LEQUAL); // Set type of depth Line
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really nice perspective calculations

    glEnable(GL_CULL_FACE);										// Remove Back Face
    Limiti_pixel[0] = 0;
}

// This is called when the OpenGL window is resized
void FtForceTest::resizeGL(int width, int height) {
    // Prevent divide by zero (in the gluPerspective call)
    if (height == 0)
        height = 1;

    glViewport(0, 0, width, height); // Reset current viewport

    glMatrixMode(GL_PROJECTION); // Select projection matrix
    glLoadIdentity(); // Reset projection matrix

    glOrtho(0,static_cast<GLfloat>(width),0,static_cast<GLfloat>(height),-1,100);				// Set Up An Ortho Screen

    glMatrixMode(GL_MODELVIEW); // Select modelview matrix
    glLoadIdentity(); // Reset modelview matrix
    updateFtForceTest();
    GLint	viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
}

int FtForceTest::generateSignal( int w )
{
    int N_campioni = TempoSequenza * 60 / (Tc/1000) ;
    double lambda;
    lambda = 0.004 / tAtteso;
    Limiti_pixel[0] = pressureOffset * w+w/10;
    Limiti_pixel[1] = w-w/10 ; // [inferiore superiore]
    Posizioni = new double[N_campioni];
    if (storedSignalChk == 0 ){
        int Posizione = w/2;
        Posizioni[0] = Posizione;
        signal[0] = Posizioni[0];
        int spostamento = 0;
        int count = 1/lambda;
        for ( int ii = 1 ; ii< N_campioni; ii++ )
        {
            count=max(0,count-1);
            if ( rand_FloatRange(0,1) < lambda && count==0)
            {
                count=1/lambda;
                spostamento = double(rand_FloatRange(-1,1)* (w/2));
                if ( Posizione + spostamento > Limiti_pixel[1] ) spostamento = - spostamento;
                if ( Posizione + spostamento < Limiti_pixel[0] ) spostamento = - spostamento;
                Posizione = Posizione + spostamento;
                if ( Posizione > Limiti_pixel[1] ) Posizione = Limiti_pixel[1]; //Fix excess size
                if ( Posizione < Limiti_pixel[0] ) Posizione = Limiti_pixel[0];
            }
            Posizioni[ii] = Posizione;
            signal[ii] = Posizioni[ii];
        }
    }
    if ( storedDisturbChk == 0 )
    {
        int Posizione = w/2 + 50;
        Posizioni[0] = Posizione;
        signalDisturb[0] = Posizioni[0];
        int spostamento = 0;
        double lambda;
        lambda = 0.004 / tAttesoDisturbo;
        int count = 1/lambda;
        for ( int ii = 1 ; ii< N_campioni; ii++ )
        {
            Posizioni[ii] =  Posizione ;
            signalDisturb[ii] = Posizioni[ii];
            count=max(0,count-1);
            if ( rand_FloatRange(0,1) < lambda && count==0)
            {
                count=1/lambda;
                spostamento = double(rand_FloatRange(-1,1)* (w/2));
                if ( signal[ii] + spostamento > Limiti_pixel[1] ) spostamento = - spostamento;
                if ( signal[ii] + spostamento < Limiti_pixel[0] ) spostamento = - spostamento;
                Posizione = signal[ii] + spostamento;
                if ( Posizione > Limiti_pixel[1] ) Posizione = Limiti_pixel[1]; //Fix excess size
                if ( Posizione < Limiti_pixel[0] ) Posizione = Limiti_pixel[0];
            }
            Posizioni[ii] = Posizione;
            signalDisturb[ii] = Posizioni[ii];
        }
    }
    return 0;
}

// Ciclo di aggiornamento variabili
void FtForceTest::updateFtForceTest(){
    static int iiSignal = 0;
    static int iiDisturb = 0;
    static int initilizeSignal = 1;
    if ( iiLimite != -1 && iiLimite > 20 )
    {
        TcLimite = TcLimite + timeLimit.elapsed() - Tempo_precLimite;
        Tempo_precLimite = timeLimit.elapsed();
        iiLimite++;
    }
    else if ( iiLimite !=-1 )
    {
        iiLimite++;
        Tempo_precLimite = timeLimit.elapsed();
    }
    if ( iiLimite > 50 )
    {
        TcLimite = TcLimite / (iiLimite-20);
        iiLimite = -1;
        Tc = TcLimite;
        if ( (storedSignalChk && TcLimite > TcSignal ) || ( disturbYN == 1 && storedDisturbChk && TcLimite > TcDisturb ) )
        {
            emit tcError( TcLimite, TcSignal, TcDisturb );
            close();
        }
        if ( storedSignalChk == 0 ) TcSignal = Tc;
        if ( storedDisturbChk == 0 ) TcDisturb = Tc;
        int N_campioni = TempoSequenza * 60 / (Tc/1000) ;
        saveTime = new double [N_campioni];
        saveSignal = new double [N_campioni];
        saveDisturb = new double [N_campioni];
        saveTouch = new double [N_campioni];
        initilizeSignal = 0;
    }
    static int Tempo_precSignal = 0;
    static int Tempo_precDisturb = 0;
    GLint	viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    if ( stato == 1 )
    {
        saveTime[iiSignal] = timeFtForceTest.elapsed();
        saveSignal[iiSignal] = signal[iiSignal];
        saveDisturb[iiSignal] = signalDisturb[iiDisturb];
        int dimension[2];
        touchTarget->getDimension(dimension);
        saveTouch[iiSignal] = dimension[1];

        if ( timeFtForceTest.elapsed() - Tempo_precSignal > int(TcSignal) )
        {
            Tempo_precSignal = timeFtForceTest.elapsed();
            int targetDimension[2] = {signal[iiSignal]-signalTargetWidth,signal[iiSignal] };
            int disturbDimension[2] = {signalDisturb[iiSignal]-disturbTargetWidth,signalDisturb[iiSignal] };
            int targetFillDimension[2] = {signal[iiSignal]-signalTargetWidth,signal[iiSignal] };
            target->setDimension(targetDimension);
            targetFill->setDimension(targetFillDimension);
            disturb->setDimension(disturbDimension );

            if ( iiSignal != 1 && signal[iiSignal] != signal[iiSignal-1])
            {
                displayPoint = 1;
                // QSound ::play(":/Sound1/misc003.wav");
            }
            // else displayPoint = 0;
            iiSignal++;
            if ( iiSignal > signalEnd || timeFtForceTest.elapsed() > durataTest*1000 )
            {
                QTextStream out(&fileOutput);
                for ( int ii = 1; ii <= iiSignal-1; ii++ )
                {
                    if ( disturbYN == 1 )out << saveTime[ii] << "\t" << saveSignal[ii] << "\t" << saveDisturb[ii] << "\t" << saveTouch[ii] << "\n";
                    else out << saveTime[ii] << "\t" << saveSignal[ii] << "\t" << 0 << "\t" << saveTouch[ii] << "\n";
                }
                fileOutput.close();
                iiSignal = 0;
                iiDisturb = 0;
                Tempo_precSignal = 0;
                Tempo_precDisturb = 0;
                close();//ii = 0;
            }
        }
    }
    else if ( stato == 0)
    {
        iiSignal = 1;
        iiDisturb = 1;
        Tempo_precSignal = 0;
        Tempo_precDisturb = 0;
        if ( initilizeSignal == 0 )
        {
            generateSignal( int(viewport[3]/2) );
            initilizeSignal = 1;
        }
        int targetDimension[2] = {signal[iiSignal]-signalTargetWidth,signal[iiSignal] };
        int disturbDimension[2] = {signalDisturb[iiSignal]-disturbTargetWidth,signalDisturb[iiSignal] };
        int targetFillDimension[2] = {signal[iiSignal]-signalTargetWidth,signal[iiSignal] };
        //target->setPosition( signal[iiSignal],viewport[3]/2 );
        target->setDimension(targetDimension);
        targetFill->setDimension(targetFillDimension);
        disturb->setDimension(disturbDimension );
    }
}

// Selezione del target
void FtForceTest::selectionMouseGameFtForceTest( int mouseInteractionType  )   // This Is Where Selection Is Done
{
    GLUquadricObj *quadric;
    quadric = gluNewQuadric();

    GLuint	buffer[512];										// Set Up A Selection Buffer
    GLint	hits;												// The Number Of Objects That We Selected
    // The Size Of The Viewport. [0] Is <x>, [1] Is <y>, [2] Is <length>, [3] Is <width>
    GLint	viewport[4];

    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);

    glSelectBuffer(512, buffer);								// Tell OpenGL To Use Our Array For Selection

    // Puts OpenGL In Selection Mode. Nothing Will Be Drawn.  Object ID's and Extents Are Stored In The Buffer.
    (void) glRenderMode(GL_SELECT);

    glInitNames();												// Initializes The Name Stack
    glPushName(0);												// Push 0 (At Least One Entry) Onto The Stack

    glMatrixMode(GL_PROJECTION);								// Selects The Projection Matrix
    glPushMatrix();												// Push The Projection Matrix
    glLoadIdentity();											// Resets The Matrix

    // This Creates A Matrix That Will Zoom Up To A Small Portion Of The Screen, Where The mouse Is.
    gluPickMatrix((GLdouble) mouse.x(), (GLdouble) (viewport[3]-mouse.y()), 1.0f, 1.0f, viewport);

    glOrtho(0,viewport[2],0,viewport[3],-1,300);				// Set Up An Ortho Screen
    glMatrixMode(GL_MODELVIEW);									// Select The Modelview Matrix
    glLoadName(targetFill->getId());
    glPushMatrix();
    targetFill->displayMouse();
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);								// Select The Projection Matrix
    glPopMatrix();												// Restore The Old Projection Matrix
    glMatrixMode(GL_MODELVIEW);									// Select The Modelview Matrix

    hits=glRenderMode(GL_RENDER);								// Switch To Render Mode, Find Out How Many
    if ( hits > 0 )						// If There Were More Than 0 Hits
    {
        int	choose[100];
        choose[0] = buffer[3];									// Make Our Selection The First Object
        for (int loop = 1; loop < hits; loop++)					// Loop Through All The Detected Hits
        {
            choose[loop] = buffer[4*loop+3];
        }
        if ( choose[0] == target->getId() )
        {
            target->setMouseState ( mouseInteractionType );
            if ( target->getMouseState() == 1 && target->getState() == false && stato == 0  )
            {
                target->setState(true);
                stato = 1;
                timeFtForceTest.start();
                //                timeFtForceTest.restart();
                QTextStream out(&fileOutput);
                out << fileOutput.fileName() << "\n";
                out << "total time[s]" << "\t" << "Tc file [ms]" << "\t"<< "TcSignal [ms]" << "\t"<< "TcDisturb [ms]" << "\t"<< "TcLim ( max Tc fo PC )[ms]" << "\t"<< "disturbYN (1=yes; 0=no)" << "\t" << "Use stored signal ( 1=yes; 0=no )" <<"\t" << "Use disturb signal ( 1=yes; 0=no )" << "\t"<< "Min left position of line [pixel]" << "\t" << "Max right position of line [pixel]"<< "\t" << "RGBAlpha background"<< "\t"<< "RGBAlpha signal"<< "\t"<< "RGBAlpha disturb"<< "\t"<< "RGBAlpha touch"<< "\t"<< "RGBAlpha limit"<< "\t"<< "Signal width[pixel]"<< "\t"<< "disturb width[pixel]"<< "\t"<< "touch width[pixel]"<< "\t"<< "limit width[pixel]"<< "\n" ;
                out << durataTest << "\t" << Tc << "\t"<< TcSignal << "\t"<< TcDisturb << "\t"<< TcLimite << "\t"<< disturbYN << "\t" << storedSignalChk <<"\t" << storedDisturbChk << "\t"<< Limiti_pixel[0] << "\t" << Limiti_pixel[1]<< "\t" ;
                out << tAtteso << "\t" << tAttesoDisturbo << "\t";
                for (int ii = 0; ii < 4; ii++ )
                {
                    out << backgroundColor[ii] << "\t";
                }
                for (int ii = 0; ii < 4; ii++ )
                {
                    out << signalColor[ii] << "\t";
                }
                for (int ii = 0; ii < 4; ii++ )
                {
                    out << disturbColor[ii] << "\t";
                }
                for (int ii = 0; ii < 4; ii++ )
                {
                    out << touchColor[ii] << "\t";
                }
                for (int ii = 0; ii < 4; ii++ )
                {
                    out << limitColor[ii] << "\t";
                }
                out << signalTargetWidth << "\t" << disturbTargetWidth << "\t" << touchTargetWidth << "\t" << limitTargetWidth << "\n\n";

            }
        }
    }
    else
    {
        target->setMouseState ( 0 );
    }
}

// Disegno l'interfaccia del test con le OPENGL
void FtForceTest::drawFtForceTest() {

    GLint	viewport[4];
    GLUquadricObj *quadric_out;
    quadric_out = gluNewQuadric();

    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(backgroundColor[0],backgroundColor[1],backgroundColor[2],backgroundColor[3]);

    touchTarget->displayMouse();

    if ( disturbYN == 1 )
    {
        disturb->displayMouse();
    }
    target->displayMouse();
    if ( displayPoint > 0 && LFPmeasuring != 0 )
    {
        glColor3f ( 0.0, 0.0, 0.0);
        glPushMatrix();
        glTranslatef(puntoTaraturaDimension,viewport[3] - puntoTaraturaDimension,0);
        glBegin(GL_QUADS);											// Start Drawing A Quad
        glVertex3f(-puntoTaraturaDimension,-puntoTaraturaDimension,0.0f);	// Bottom Left
        glVertex3f( puntoTaraturaDimension,-puntoTaraturaDimension,0.0f);	// Bottom Right
        glVertex3f( puntoTaraturaDimension, puntoTaraturaDimension,0.0f);	// Top Right
        glVertex3f(-puntoTaraturaDimension, puntoTaraturaDimension,0.0f);	// Top Left
        glEnd();
        glPopMatrix();
        displayPoint++;
        if ( displayPoint > 20 )displayPoint = 0;
    }

    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glColor3f ( 0.0, 0.0, 0.0);

    glColor3f ( 0.0, 0.0, 0.0);
    QString qStr2 = QString("Time:%1/%2  pressureOffset:= %3   realPressure:= %4" ).arg(timeFtForceTest.elapsed()/1000).arg(durataTest).arg(pressureOffset).arg(pressureReal);
    renderText(Limiti_pixel[1] + 10, viewport[3]/10, qStr2,QFont("Arial", 12, QFont::Bold, false) );
}

// OpenGL painting code goes here
void FtForceTest::paintGL() {
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear Screen And Depth Buffer

    updateFtForceTest();
    //    glLoadIdentity();											// Reset The Modelview Matrix
    drawFtForceTest();
}

// Key handler
void FtForceTest::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
    case Qt::Key_Escape:
        fileOutput.close();
        close(); // Quit on Escape
        break;
    case Qt::Key_F1:
        setWindowState(windowState() ^ Qt::WindowFullScreen); // Toggle fullscreen on F1
        break;
    case Qt::Key_F2:
        break;
    case Qt::Key_Up:
        puntoTaraturaDimension++;
        break;
    case Qt::Key_Down:
        puntoTaraturaDimension--;
        break;
    case Qt::Key_Left:
        pressureOffset+=0.0;//0.01;
        break;
    case Qt::Key_Right:
        pressureOffset-=0.0;//0.01;
        break;
    default:
        QGLWidget::keyPressEvent(event); // Let base class handle the other keys
    }
}

// Mouse Move handler
void FtForceTest::mouseMoveEvent(QMouseEvent *event) {
    if ( stato == 0 && iiLimite == -1 ) selectionMouseGameFtForceTest( 1 );
    GLint	viewport[4];
    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);
    mouse = event->pos();
    touchTarget->setPosition(mouse.x(),viewport[3]-mouse.y());
    target->setPosition(mouse.x(),viewport[3]-mouse.y());
    targetFill->setPosition(mouse.x(),viewport[3]-mouse.y());
    disturb->setPosition(mouse.x(),viewport[3]-mouse.y());
    paintGL();
}

void FtForceTest::mousePressEvent(QMouseEvent *event){
    //    mStateL = 1;
    //    selectionMouseGameFtForceTest( 2 );
    //    mouse = event->pos();
    //    //    selectionGameFtForceTest( );
    //    paintGL();
}

double* FtForceTest::loadSignal( QString signalFileName_tmp, double *signal_tmp )
{
    QFile file(signalFileName_tmp);
    qDebug()<< QDir::currentPath()<< "file segnale  "<< signalFileName_tmp;
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }
    QTextStream in(&file);
    QString line = in.readLine();
    int ii = 0;
    signalEnd = 0;

    while (!line.isNull()) {
        ii++;
        signalEnd++;
        line = in.readLine();
    }
    qDebug()<<"signalEnd vale: " << ii * sizeof(double);
    signal_tmp = (double *) malloc(ii * sizeof(double));

    file.close();
    //        exit(100);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QMessageBox msgBox;
        msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
    }

    QTextStream in1(&file);
    QString line1 = in1.readLine();
    ii=0;
    while (!line1.isNull()) {
        QStringList  fields = line1.split("\t");

        signal_tmp[ii]=  (fields[0].toDouble());
        //        exit(9876);
        //        qDebug()<< signal_tmp[ii] << " global ";

        ii++;
        line1 = in1.readLine();
    }
    file.close();
    return signal_tmp;
}

void FtForceTest::tabletEvent(QTabletEvent *event)
{
    GLint	viewport[4];
    // This Sets The Array <viewport> To The Size And Location Of The Screen Relative To The Window
    glGetIntegerv(GL_VIEWPORT, viewport);
    mouse = event->pos();
    touchTarget->setPosition(mouse.x(),viewport[3]-mouse.y());
    target->setPosition(mouse.x(),viewport[3]-mouse.y());
    targetFill->setPosition(mouse.x(),viewport[3]-mouse.y());
    disturb->setPosition(mouse.x(),viewport[3]-mouse.y());
    pressureReal = event->pressure();
    if ( event->pressure() > pressureOffset )
    {
        int dimension[2] = {(event->pressure())*(event->pressure()) * viewport[3]/2 - touchTargetWidth, (event->pressure())*(event->pressure()) * viewport[3]/2};
        touchTarget->setDimension(dimension);
    }
    else
    {
        int dimension[2] = {pressureOffset * pressureOffset * viewport[3]/2 - touchTargetWidth, pressureOffset * pressureOffset * viewport[3]/2};
        touchTarget->setDimension(dimension);
    }
    update();
}

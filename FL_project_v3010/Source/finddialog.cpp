#include "finddialog.h"
#include <string>
#include <iostream>
#include <sstream>



using namespace std;
FILE *stream_Coeff_touch,*stream_Coeff_forza, *fopen();

QString stileBottone = "QPushButton { flat = true; color: white;"
        "font: bold 5pt;"
        "height: 60px;"
        "width: 60px;"
        "qproperty-iconSize: 64px 64px;"
        "border-style:none;}"
        "QPushButton {text-align: bottom;}"
        "QPushButton:enabled:hover { color: red; }";
//{border-style: solid;"
//        "border-color: gray;"
//        "border-width: 2px;"
//        "border-radius: 5px;} ";
QString stileSelezioneSoggetto ("QTableView {selection-background-color: red;}");
QString stileGroupBox = ("QGroupBox { background-color:  qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #E0E0E0, stop: 1 #FFFFFF);"
                         "font-size: 15px;"
                         "border-style: solid;"
                         "border-color: gray;"
                         "border-width: 1px;"
                         "border-radius: 2px;"
                         "margin-top: 2ex;} "
                         "QGroupBox::title {subcontrol-origin: margin;"
                         "subcontrol-position: top center;"
                         "padding: 5 30px;"
                         "font-size: 12px;"
                         "font-weight: bold;}");
QString stileWidgetSetup = ("QWidget { border-style: solid;"
                            "border-color: gray;"
                            "border-width: 1px;"
                            "border-radius: 2px;"
                            "margin-top: 5ex;} "
                            "QWidget {height:50;}");
//                            "QWidget {background-color:blue;}");
QString stileSpinBox = ( "QSpinBox {font: bold 10pt;"
                         "height: 30px;"
                         "width: 600px;"
                         "border-style:solid}"
                         "QSpinBox:up-button { width: 30px;}"
                         "QSpinBox:down-button { width: 30px;}");

FindDialog::FindDialog(QWidget *parent)
    : QWidget(parent)
{

    serial = new QSerialPort(this);

    crcInit();                                              //crea tabella per velocizzare il calcolo del CRC
    openSerialPort();
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));


//! [2]
    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));
//! [2]

    page = new QSplitter();
    fileNameOutput = QDir().currentPath() ;


    // recall the function to create the pages ( User, tests option page )
    createOpenOrNewUserPage();
    createLinePage();
    createFtForceTestPage();
    createIlluminaLaCittaPage();

    // connect left menu with realative rigth side page
    selectedPage = new QStackedLayout;
    selectedPage->addWidget(linePage);
    selectedPage->addWidget(ftForceTestPage);
    selectedPage->addWidget(illuminaLaCittaPage);
    linePage->setStyleSheet("background:transparent;");
    linePage->setAttribute(Qt::WA_TranslucentBackground);
    linePage->setWindowFlags(Qt::FramelessWindowHint);
    ftForceTestPage->setStyleSheet("background:transparent;");
    ftForceTestPage->setAttribute(Qt::WA_TranslucentBackground);
    ftForceTestPage->setWindowFlags(Qt::FramelessWindowHint);

    contentsWidget = new QListWidget;
    contentsWidget->setViewMode(QListView::IconMode);
    contentsWidget->setIconSize(QSize(120, 84));
    contentsWidget->setMovement(QListView::Static);
    contentsWidget->setMinimumWidth(130);
    contentsWidget->setMaximumWidth(200);
    contentsWidget->setSpacing(0);
    contentsWidget->setStyleSheet("background:transparent;");
    contentsWidget->setAttribute(Qt::WA_TranslucentBackground);
    contentsWidget->setWindowFlags(Qt::FramelessWindowHint);
    contentsWidget->setLineWidth(0);
    contentsWidget->setStyleSheet("border-style:none");

    QListWidgetItem *ftPositionTestButton = new QListWidgetItem(contentsWidget);
    ftPositionTestButton->setIcon(QIcon("Image/General/ftPositionTest.png"));
    ftPositionTestButton->setText(tr("FT-Position Test"));
    ftPositionTestButton->setTextAlignment(Qt::AlignHCenter);
    ftPositionTestButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QListWidgetItem *ftForceTestButton = new QListWidgetItem(contentsWidget);
    ftForceTestButton->setIcon(QIcon("Image/General/ftForceTest.png"));
    ftForceTestButton->setText(tr("FT-Force Test"));
    ftForceTestButton->setTextAlignment(Qt::AlignHCenter);
    ftForceTestButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QListWidgetItem *illuminaLaCittaButton = new QListWidgetItem(contentsWidget);
    illuminaLaCittaButton->setIcon(QIcon("Image/General/illuminaLaCitta.png"));
    illuminaLaCittaButton->setText(tr("Illumina La Città Test"));
    illuminaLaCittaButton->setTextAlignment(Qt::AlignHCenter);
    illuminaLaCittaButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QVBoxLayout *selectedListLayout = new QVBoxLayout;

    // connect the page selection button with the right page option display
    connect(contentsWidget, SIGNAL(currentRowChanged(int)),
            selectedPage, SLOT(setCurrentIndex(int)));
    //createSelectedUserPage();
    // create a grid layot where draw the GUI
    QGridLayout *mainLayout = new QGridLayout;
//    mainLayout->setColumnStretch(0, 1);
    mainLayout->setColumnStretch(1, 3);
    mainLayout->addLayout(createSelectedUserPage(),0,1);
    //selectedListLayout->addLayout(createSelectedUserPage());
    selectedListLayout->addLayout(selectedPage);
    createUserListPage();


    //////////////////
    QVBoxLayout *userListLayout = new QVBoxLayout;
    userListLayout->addWidget(m_pTableWidget);
    userListLayout->addWidget(newOrOpenUserPage);

    QSpacerItem *spacer = new QSpacerItem(40, 50, QSizePolicy::Fixed, QSizePolicy::Fixed);
    QVBoxLayout *spacerLayout = new QVBoxLayout;
    spacerLayout->addSpacerItem(spacer);


    //    // create a grid layot where draw the GUI
    //    QGridLayout *mainLayout = new QGridLayout;
    //    //mainLayout->setColumnStretch(0, 1);
    mainLayout->setColumnStretch(1, 3);
    mainLayout->addWidget(contentsWidget, 1, 0);
    mainLayout->addLayout(createSelectedUserPage(),0,1);
    mainLayout->addLayout(selectedListLayout, 1, 1);
    mainLayout->addLayout(userListLayout, 1, 2);
    mainLayout->addLayout(spacerLayout, 2, 2);
    mainLayout->setColumnMinimumWidth(0,130);
    mainLayout->setColumnMinimumWidth(2,150);


    setLayout(mainLayout);
    int width = 1024;
    int height = 768;
    setFixedSize(1024,768);
    contentsWidget->setCurrentRow(2);


}

QGridLayout *FindDialog::createSelectedUserPage()
{
    QFont pageListFont( "Arial", 50, QFont::Bold);
    QPixmap image = QPixmap("Image/General/questionMark.jpg");
    QLabel *imageLabel = new QLabel();
    imageLabel->setMaximumSize(100,100);

    imageLabel->setPixmap(image);
    // connect left menu with realative rigth side page
    QGridLayout *selectedUser = new QGridLayout;
    nameLabel = new QLabel(tr("SOGGETTO:"));
    nameLabel->setFont(pageListFont);
    surnameLabel = new QLabel(tr("COGNOME:"));
    surnameLabel->setFont(pageListFont);
    diseaseLabel = new QLabel(tr("NOME:"));
    diseaseLabel->setFont(pageListFont);
    nameLabel->setStyleSheet("background-color: #abb8b8 ");
    QVBoxLayout *dataListLayout = new QVBoxLayout;
    dataListLayout->addWidget(nameLabel);
    //    dataListLayout->addWidget(surnameLabel);
    //    dataListLayout->addWidget(diseaseLabel);
    selectedUser->addLayout(dataListLayout,0,1);
    //    selectedUser->addWidget(imageLabel,0,0);
    selectedUser->setHorizontalSpacing(50);

    return selectedUser;
}

void FindDialog::createUserListPage()
{
    QFont userListPageFont( "Arial", 15, QFont::Bold);
    m_pTableWidget = new QTableWidget(this);
    m_pTableWidget->setFont(userListPageFont);
    QStringList m_TableHeader;
    m_pTableWidget->setRowCount(1000);
    m_pTableWidget->setColumnCount(1);
    m_TableHeader<<"LISTA";
    m_pTableWidget->horizontalHeader()->setFont(userListPageFont);
    m_pTableWidget->setHorizontalHeaderLabels(m_TableHeader);
    m_pTableWidget->verticalHeader()->setVisible(false);
    m_pTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_pTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_pTableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    m_pTableWidget->setShowGrid(false);
    m_pTableWidget->setStyleSheet(stileSelezioneSoggetto);
    //    m_pTableWidget->setRowHeight(0,100);
    m_pTableWidget->setColumnWidth(0,150);
    m_pTableWidget->setStyleSheet("border-style:none");
    //    m_pTableWidget->setStyleSheet("QHeaderView::section { background-color:#abb8b8 }");

    QStringList all_dirs;
    all_dirs << "User";
    QDirIterator directories("User", QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot );
    do {
        m_pTableWidget->setRowCount(all_dirs.count());
        directories.next();
        QStringList entries = directories.filePath().split("/");
        m_pTableWidget->setItem( all_dirs.count()-1, 0, new QTableWidgetItem(entries.last()));
        all_dirs << directories.filePath();
    } while(directories.hasNext());

    if ( m_pTableWidget->item(0,0)->text().isEmpty() == false ) m_pTableWidget->setRowCount(m_pTableWidget->rowCount()+1);
    qDebug()<<m_pTableWidget->rowCount() << all_dirs.count();
    connect( m_pTableWidget, SIGNAL( cellClicked (int, int) ),
             this, SLOT( updateUserSelected( int, int ) ));
}

void FindDialog::createOpenOrNewUserPage(){

    newOrOpenUserPage = new QWidget;

    QFont labelFont( "Arial", 30, QFont::Bold);
    QFont buttonFont( "Arial", 20, QFont::Bold);

    userLabel = new QLabel(tr("Utente:"));
    userLabel->setFont( labelFont);
    qDebug()<< "createOpenOrNewUserPage current folder: " << QDir::currentPath();
    loadUserButton = new QPushButton(tr("Apri"));
    loadUserButton->setDefault(true);
    loadUserButton->setEnabled(true);
    QPixmap pixmap = QPixmap("Image/General/Create.png");
    QIcon icon(pixmap);
    loadUserButton->setIcon(icon);
    loadUserButton->setFlat(true);
    loadUserButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    loadUserButton->setMinimumWidth(150);
    loadUserButton->setMinimumHeight(150);
    loadUserButton->setFont(buttonFont);

    newUserButton = new QToolButton;
    newUserButton->setIcon(icon);
    newUserButton->setIconSize(QSize(80,80));
    newUserButton->setEnabled(true);
    newUserButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    newUserButton->setAutoRaise(true);
    newUserButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                 "border-style:none; }"
                                 "QToolButton{font:12pt;color:white}");

    QPixmap image = QPixmap("Image/General/Sfondo.tga");
    QLabel *imageLabel = new QLabel();
    imageLabel->setPixmap(image);
    QVBoxLayout *LoadUserLayout = new QVBoxLayout;
    LoadUserLayout->addWidget(imageLabel,0,Qt::AlignCenter);
    LoadUserLayout->addWidget(userLabel,0,Qt::AlignCenter);
    QHBoxLayout *LoadUserHLayout = new QHBoxLayout;
    LoadUserHLayout->addWidget(newUserButton,0,Qt::AlignCenter);
    QGridLayout *MainLoadUserLayout = new QGridLayout;
    MainLoadUserLayout->addLayout(LoadUserHLayout,2,0,0);
    newOrOpenUserPage->setLayout(MainLoadUserLayout);
    connect(newUserButton, SIGNAL(clicked()),
            this, SLOT(createFolder()));
}

// Modify this function in according with your option parameter
void FindDialog::createLinePage(){

    // inizializzazione della immagini da usare nella pagina del test
    QPixmap pixmapStart = QPixmap("Image/General/Play.png");
    QIcon iconStart(pixmapStart);
    QPixmap pixmapStop = QPixmap("Image/General/Stop.png");
    QIcon iconStop(pixmapStop);
    QPixmap pixmapSave = QPixmap("Image/General/Save.png");
    QIcon iconSave(pixmapSave);
    QPixmap pixmapRestore = QPixmap("Image/General/Restore.png");
    QIcon iconRestore(pixmapRestore);
    QPixmap image = QPixmap("Image/LineTest/initialImage.png");
    QLabel *imageLabel = new QLabel();
    imageLabel->setStyleSheet("color: blue;"
                              "height = 10px"
                              "background-color: white;"
                              );
    imageLabel->setPixmap(image);

    linePage = new QWidget;

    // inizialzzazione di dei pulsanti spinbox etc da usare nella pagina del test
    lineStartButton = new QToolButton;
    lineStartButton->setIcon(iconStart);
    lineStartButton->setIconSize(QSize(80,80));
    lineStartButton->setEnabled(true);
    lineStartButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    lineStartButton->setAutoRaise(true);
    lineStartButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                   "border-style:none; }"
                                   "QToolButton{font:12pt;color:white}");

    lineStopButton = new QToolButton;
    lineStopButton->setIcon(iconStop);
    lineStopButton->setIconSize(QSize(80,80));
    lineStopButton->setEnabled(true);
    lineStopButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    lineStopButton->setAutoRaise(true);
    lineStopButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                  "border-style:none; }"
                                  "QToolButton{font:12pt;color:white}");
    lineStopButton->setVisible(false);

    lineDefaultSettingsButton = new QToolButton;
    lineDefaultSettingsButton->setIcon(iconRestore);
    lineDefaultSettingsButton->setIconSize(QSize(80,80));
    lineDefaultSettingsButton->setEnabled(true);
    lineDefaultSettingsButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    lineDefaultSettingsButton->setAutoRaise(true);
    lineDefaultSettingsButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                             "border-style:none; }"
                                             "QToolButton{font:12pt;color:white}");

    lineSaveSettingsButton = new QToolButton;
    lineSaveSettingsButton->setIcon(iconSave);
    lineSaveSettingsButton->setIconSize(QSize(80,80));
    lineSaveSettingsButton->setEnabled(true);
    lineSaveSettingsButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    lineSaveSettingsButton->setAutoRaise(true);
    lineSaveSettingsButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                          "border-style:none; }"
                                          "QToolButton{font:12pt;color:white}");

    lineSpinBox = new QSpinBox () ;
    lineSpinBox->setRange(0, 5000);
    lineSpinBox->setValue(150);
    //    lineSpinBox->setPrefix("Total time ( max 5 min )   ");
    lineSpinBox->setSuffix(" [s]");
    //lineSpinBox->setMaximumWidth(105);
    lineSpinBox->setStyleSheet(stileSpinBox);
    QLabel *totalTimeLabel = new QLabel(tr("TOTAL TIME:"));

    lineTempoSaltoSpinBox = new QDoubleSpinBox () ;
    lineTempoSaltoSpinBox->setRange(0, 5);
    lineTempoSaltoSpinBox->setValue(1.4);
    //    lineSpinBox->setPrefix("Total time ( max 5 min )   ");
    lineTempoSaltoSpinBox->setSuffix(" [s]");
    //lineSpinBox->setMaximumWidth(105);
    lineTempoSaltoSpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineTempoSaltoLabel = new QLabel(tr("Mean Jump Time (s):"));

    lineTempoSaltoDisturboSpinBox = new QDoubleSpinBox () ;
    lineTempoSaltoDisturboSpinBox->setRange(0, 5);
    lineTempoSaltoDisturboSpinBox->setValue(1.4);
    //    lineSpinBox->setPrefix("Total time ( max 5 min )   ");
    lineTempoSaltoDisturboSpinBox->setSuffix(" [s]");
    //lineSpinBox->setMaximumWidth(105);
    lineTempoSaltoDisturboSpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineTempoSaltoDsiturboLabel = new QLabel(tr("Mean Jump Time Disturbance (s):"));

    lineTcSpinBox = new QSpinBox () ;
    lineTcSpinBox->setValue(16);
    lineTcSpinBox->setRange(0, 1000);
    //    lineTcSpinBox->setPrefix("sampling time");
    //lineTcSpinBox->setMaximumWidth(105);
    lineTcSpinBox->setSuffix(" [ms]");
    lineTcSpinBox->setStyleSheet(stileSpinBox);
    QLabel *tcLabel = new QLabel(tr("SAMPLING TIME:"));

    QLabel *signalLabel = new QLabel(tr("SIGNAL:"));
    QLabel *disturbDataLabel = new QLabel(tr("DISTURB:"));
    lineSignalSlectionComboBox = new QComboBox();
    lineDisturbSlectionComboBox = new QComboBox();
    lineDisturbSlectionComboBox = new QComboBox();
    QDir path("Data");
    QStringList all_dirs = path.entryList(QDir::Files);
    lineSignalSlectionComboBox->addItems(all_dirs );
    lineDisturbSlectionComboBox->addItems(all_dirs );

    QLabel *inputSignalLabel = new QLabel(tr("STORED SIGNAL (CHK =TRUE)"));
    lineStoredNonStoredSignal = new QCheckBox();
    lineStoredNonStoredSignal->setChecked(false);
    connect(lineStoredNonStoredSignal, SIGNAL(	stateChanged(int)),
            this, SLOT(setLineSignalActiveDeative(int)));

    QLabel *inputDisturbLabel = new QLabel(tr("STORED SIGNAL (CHK =TRUE)"));
    lineStoredNonStoredDisturb = new QCheckBox();
    lineStoredNonStoredDisturb->setChecked(false);
    connect(lineStoredNonStoredDisturb, SIGNAL(	stateChanged(int)),
            this, SLOT(setLineDisturbActiveDeative(int)));

    QLabel *disturbLabel = new QLabel(tr("DISTURB (CHK =TRUE)"));
    lineDisturbYN = new QCheckBox();
    lineDisturbYN->setChecked(true);

    QLabel *lineLFPmeasuringLabel = new QLabel(tr("LFP measuring (CHK =TRUE)"));
    lineLFPmeasuring = new QCheckBox();
    lineLFPmeasuring->setChecked(false);

    lineSignalTargetDimension = new QSpinBox () ;
    lineSignalTargetDimension->setValue(8);
    lineSignalTargetDimension->setRange(2, 50);
    lineSignalTargetDimension->setSuffix(" [px]");
    //lineSignalTargetDimension->setMaximumWidth(105);
    lineSignalTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *lineSignalTargetLabel = new QLabel(tr("Signal target dimension:"));

    lineDisturbTargetDimension = new QSpinBox () ;
    lineDisturbTargetDimension->setValue(8);
    lineDisturbTargetDimension->setRange(2, 50);
    lineDisturbTargetDimension->setSuffix(" [px]");
    //lineDisturbTargetDimension->setMaximumWidth(105);
    lineDisturbTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *lineDisturbTargetLabel = new QLabel(tr("Disturb target dimension:"));

    lineTouchTargetDimension = new QSpinBox () ;
    lineTouchTargetDimension->setValue(2);
    lineTouchTargetDimension->setRange(2, 50);
    lineTouchTargetDimension->setSuffix(" [px]");
    //lineTouchTargetDimension->setMaximumWidth(105);
    lineTouchTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *lineTouchTargetLabel = new QLabel(tr("Touch target dimension:"));

    lineLimitTargetDimension = new QSpinBox () ;
    lineLimitTargetDimension->setValue(2);
    lineLimitTargetDimension->setRange(2, 50);
    lineLimitTargetDimension->setSuffix(" [px]");
    // lineLimitTargetDimension->setMaximumWidth(105);
    lineLimitTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *lineLimitTargetLabel = new QLabel(tr("Limit target dimension:"));

    lineBackgroundColorR_SpinBox = new QSpinBox () ;
    lineBackgroundColorR_SpinBox->setValue(100);
    lineBackgroundColorR_SpinBox->setRange(0, 100);
    //lineBackgroundColorR_SpinBox->setMaximumWidth(105);
    lineBackgroundColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineBackgroundColorRLabel = new QLabel(tr("R:"));

    lineBackgroundColorG_SpinBox = new QSpinBox () ;
    lineBackgroundColorG_SpinBox->setValue(100);
    lineBackgroundColorG_SpinBox->setRange(0, 100);
    //lineBackgroundColorG_SpinBox->setMaximumWidth(105);
    lineBackgroundColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineBackgroundColorGLabel = new QLabel(tr("G:"));

    lineBackgroundColorB_SpinBox = new QSpinBox () ;
    lineBackgroundColorB_SpinBox->setValue(100);
    lineBackgroundColorB_SpinBox->setRange(0, 100);
    //lineBackgroundColorB_SpinBox->setMaximumWidth(105);
    lineBackgroundColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineBackgroundColorBLabel = new QLabel(tr("B:"));

    lineSignalColorR_SpinBox = new QSpinBox () ;
    lineSignalColorR_SpinBox->setValue(0);
    lineSignalColorR_SpinBox->setRange(0, 100);
    // lineSignalColorR_SpinBox->setMaximumWidth(105);
    lineSignalColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineSignalColorRLabel = new QLabel(tr("R:"));

    lineSignalColorG_SpinBox = new QSpinBox () ;
    lineSignalColorG_SpinBox->setValue(100);
    lineSignalColorG_SpinBox->setRange(0, 100);
    //lineSignalColorG_SpinBox->setMaximumWidth(105);
    lineSignalColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineSignalColorGLabel = new QLabel(tr("G:"));

    lineSignalColorB_SpinBox = new QSpinBox () ;
    lineSignalColorB_SpinBox->setValue(100);
    lineSignalColorB_SpinBox->setRange(0, 100);
    //lineSignalColorB_SpinBox->setMaximumWidth(105);
    lineSignalColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineSignalColorBLabel = new QLabel(tr("B:"));

    lineDisturbColorR_SpinBox = new QSpinBox () ;
    lineDisturbColorR_SpinBox->setValue(100);
    lineDisturbColorR_SpinBox->setRange(0, 100);
    //lineDisturbColorR_SpinBox->setMaximumWidth(105);
    lineDisturbColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineDisturbColorRLabel = new QLabel(tr("R:"));

    lineDisturbColorG_SpinBox = new QSpinBox () ;
    lineDisturbColorG_SpinBox->setValue(0);
    lineDisturbColorG_SpinBox->setRange(0, 100);
    //lineDisturbColorG_SpinBox->setMaximumWidth(105);
    lineDisturbColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineDisturbColorGLabel = new QLabel(tr("G:"));

    lineDisturbColorB_SpinBox = new QSpinBox () ;
    lineDisturbColorB_SpinBox->setValue(0);
    lineDisturbColorB_SpinBox->setRange(0, 100);
    //lineDisturbColorB_SpinBox->setMaximumWidth(105);
    lineDisturbColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineDisturbColorBLabel = new QLabel(tr("B:"));

    lineTouchColorR_SpinBox = new QSpinBox () ;
    lineTouchColorR_SpinBox->setValue(0);
    lineTouchColorR_SpinBox->setRange(0, 100);
    //lineTouchColorR_SpinBox->setMaximumWidth(105);
    lineTouchColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineTouchColorRLabel = new QLabel(tr("R:"));

    lineTouchColorG_SpinBox = new QSpinBox () ;
    lineTouchColorG_SpinBox->setValue(0);
    lineTouchColorG_SpinBox->setRange(0, 100);
    //lineTouchColorG_SpinBox->setMaximumWidth(105);
    lineTouchColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineTouchColorGLabel = new QLabel(tr("G:"));

    lineTouchColorB_SpinBox = new QSpinBox () ;
    lineTouchColorB_SpinBox->setValue(0);
    lineTouchColorB_SpinBox->setRange(0, 100);
    //lineTouchColorB_SpinBox->setMaximumWidth(105);
    lineTouchColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineTouchColorBLabel = new QLabel(tr("B:"));

    lineLimitColorR_SpinBox = new QSpinBox () ;
    lineLimitColorR_SpinBox->setValue(0);
    lineLimitColorR_SpinBox->setRange(0, 100);
    //lineLimitColorR_SpinBox->setMaximumWidth(105);
    lineLimitColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineLimitColorRLabel = new QLabel(tr("R:"));

    lineLimitColorG_SpinBox = new QSpinBox () ;
    lineLimitColorG_SpinBox->setValue(0);
    lineLimitColorG_SpinBox->setRange(0, 100);
    //lineLimitColorG_SpinBox->setMaximumWidth(105);
    lineLimitColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineLimitColorGLabel = new QLabel(tr("G:"));

    lineLimitColorB_SpinBox = new QSpinBox () ;
    lineLimitColorB_SpinBox->setValue(0);
    lineLimitColorB_SpinBox->setRange(0, 100);
    //lineLimitColorB_SpinBox->setMaximumWidth(105);
    lineLimitColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *lineLimitColorBLabel = new QLabel(tr("B:"));

    QGroupBox *lineColorGroupBox = new QGroupBox();
    QGridLayout *lineColorLayout = new QGridLayout;

    QGroupBox *lineBackgroundColorGroupBox = new QGroupBox();
    QGridLayout *lineBackgroundColorLayout = new QGridLayout;
    lineBackgroundColorLayout->addWidget(lineBackgroundColorRLabel,0,0);
    lineBackgroundColorLayout->addWidget(lineBackgroundColorR_SpinBox,0,1);
    lineBackgroundColorLayout->addWidget(lineBackgroundColorGLabel,1,0);
    lineBackgroundColorLayout->addWidget(lineBackgroundColorG_SpinBox,1,1);
    lineBackgroundColorLayout->addWidget(lineBackgroundColorBLabel,2,0);
    lineBackgroundColorLayout->addWidget(lineBackgroundColorB_SpinBox,2,1);

    lineBackgroundColorGroupBox->setTitle("Background color");
    lineBackgroundColorGroupBox->setLayout(lineBackgroundColorLayout);

    QGroupBox *lineSignalColorGroupBox = new QGroupBox();
    QGridLayout *lineSignalColorLayout = new QGridLayout;
    lineSignalColorLayout->addWidget(lineSignalColorRLabel,0,0);
    lineSignalColorLayout->addWidget(lineSignalColorR_SpinBox,0,1);
    lineSignalColorLayout->addWidget(lineSignalColorGLabel,1,0);
    lineSignalColorLayout->addWidget(lineSignalColorG_SpinBox,1,1);
    lineSignalColorLayout->addWidget(lineSignalColorBLabel,2,0);
    lineSignalColorLayout->addWidget(lineSignalColorB_SpinBox,2,1);

    lineSignalColorGroupBox->setTitle("Signal color");
    lineSignalColorGroupBox->setLayout(lineSignalColorLayout);

    QGroupBox *lineDisturbColorGroupBox = new QGroupBox();
    QGridLayout *lineDisturbColorLayout = new QGridLayout;
    lineDisturbColorLayout->addWidget(lineDisturbColorRLabel,0,0);
    lineDisturbColorLayout->addWidget(lineDisturbColorR_SpinBox,0,1);
    lineDisturbColorLayout->addWidget(lineDisturbColorGLabel,1,0);
    lineDisturbColorLayout->addWidget(lineDisturbColorG_SpinBox,1,1);
    lineDisturbColorLayout->addWidget(lineDisturbColorBLabel,2,0);
    lineDisturbColorLayout->addWidget(lineDisturbColorB_SpinBox,2,1);

    lineDisturbColorGroupBox->setTitle("Disturb color");
    lineDisturbColorGroupBox->setLayout(lineDisturbColorLayout);

    QGroupBox *lineTouchColorGroupBox = new QGroupBox();
    QGridLayout *lineTouchColorLayout = new QGridLayout;
    lineTouchColorLayout->addWidget(lineTouchColorRLabel,0,0);
    lineTouchColorLayout->addWidget(lineTouchColorR_SpinBox,0,1);
    lineTouchColorLayout->addWidget(lineTouchColorGLabel,1,0);
    lineTouchColorLayout->addWidget(lineTouchColorG_SpinBox,1,1);
    lineTouchColorLayout->addWidget(lineTouchColorBLabel,2,0);
    lineTouchColorLayout->addWidget(lineTouchColorB_SpinBox,2,1);

    lineTouchColorGroupBox->setTitle("Touch color");
    lineTouchColorGroupBox->setLayout(lineTouchColorLayout);

    QGroupBox *lineLimitColorGroupBox = new QGroupBox();
    QGridLayout *lineLimitColorLayout = new QGridLayout;
    lineLimitColorLayout->addWidget(lineLimitColorRLabel,0,0);
    lineLimitColorLayout->addWidget(lineLimitColorR_SpinBox,0,1);
    lineLimitColorLayout->addWidget(lineLimitColorGLabel,1,0);
    lineLimitColorLayout->addWidget(lineLimitColorG_SpinBox,1,1);
    lineLimitColorLayout->addWidget(lineLimitColorBLabel,2,0);
    lineLimitColorLayout->addWidget(lineLimitColorB_SpinBox,2,1);

    lineLimitColorGroupBox->setTitle("Limit color");
    lineLimitColorGroupBox->setLayout(lineLimitColorLayout);

    // raccogliere le opzioni simili in gruop box
    QGroupBox *lineSignalGroupBox = new QGroupBox();
    QGridLayout *lineSignalLayout = new QGridLayout;

    lineSignalLayout->addWidget(signalLabel,2,0);
    lineSignalLayout->addWidget(lineStoredNonStoredSignal,2,1);
    //    lineSpinBoxLayout->addWidget(tcLabel,1,0);
    //    lineSpinBoxLayout->addWidget(lineTcSpinBox,1,1);
    lineSignalLayout->addWidget(totalTimeLabel,0,0);
    lineSignalLayout->addWidget(lineSpinBox,0,1);
    lineSignalLayout->addWidget(inputSignalLabel,3,0);
    lineSignalLayout->addWidget(lineSignalSlectionComboBox,3,1);
    lineSignalLayout->addWidget(lineTempoSaltoLabel,4,0);
    lineSignalLayout->addWidget(lineTempoSaltoSpinBox,4,1);
    lineSignalLayout->addWidget(lineLFPmeasuringLabel,5,0);
    lineSignalLayout->addWidget(lineLFPmeasuring,5,1);
    lineSignalGroupBox->setStyleSheet(stileGroupBox);
    lineSignalGroupBox->setTitle("OPTION SIGNAL");
    lineSignalGroupBox->setLayout(lineSignalLayout);

    QGroupBox *lineDisturbGroupBox = new QGroupBox();
    QGridLayout *lineDisturbLayout = new QGridLayout;

    lineDisturbLayout->addWidget(disturbDataLabel,1,0);
    lineDisturbLayout->addWidget(lineStoredNonStoredDisturb,1,1);
    lineDisturbLayout->addWidget(inputDisturbLabel,2,0);
    lineDisturbLayout->addWidget(lineDisturbSlectionComboBox,2,1);
    lineDisturbLayout->addWidget(disturbLabel,0,0);
    lineDisturbLayout->addWidget(lineDisturbYN,0,1);
    lineDisturbLayout->addWidget(lineTempoSaltoDsiturboLabel,3,0);
    lineDisturbLayout->addWidget(lineTempoSaltoDisturboSpinBox,3,1);

    lineDisturbGroupBox->setStyleSheet(stileGroupBox);
    lineDisturbGroupBox->setTitle("OPTION DISTURB");
    lineDisturbGroupBox->setLayout(lineDisturbLayout);

    lineColorLayout->addWidget(lineBackgroundColorGroupBox,0,0);
    lineColorLayout->addWidget(lineSignalColorGroupBox,0,1);
    lineColorLayout->addWidget(lineDisturbColorGroupBox,0,2);
    lineColorLayout->addWidget(lineTouchColorGroupBox,0,3);
    lineColorLayout->addWidget(lineLimitColorGroupBox,0,4);
    lineBackgroundColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    lineSignalColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    lineDisturbColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    lineTouchColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    lineLimitColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    lineColorGroupBox->setStyleSheet(stileGroupBox);
    lineColorGroupBox->setTitle("COLOR SET-UP");
    lineColorGroupBox->setLayout(lineColorLayout);

    QGroupBox *lineLookGroupBox = new QGroupBox();
    QGridLayout *lineLookLayout = new QGridLayout;
    lineLookLayout->addWidget(lineSignalTargetLabel,0,0);
    lineLookLayout->addWidget(lineSignalTargetDimension,0,1);
    lineLookLayout->addWidget(lineDisturbTargetLabel,1,0);
    lineLookLayout->addWidget(lineDisturbTargetDimension,1,1);
    lineLookLayout->addWidget(lineTouchTargetLabel,2,0);
    lineLookLayout->addWidget(lineTouchTargetDimension,2,1);
    lineLookLayout->addWidget(lineLimitTargetLabel,3,0);
    lineLookLayout->addWidget(lineLimitTargetDimension,3,1);
    lineLookGroupBox->setStyleSheet(stileGroupBox);
    lineLookGroupBox->setTitle("General test look");
    lineLookGroupBox->setLayout(lineLookLayout  );



    // inserire in un una griglia i gruopbox creati
    QGridLayout *optionLayout = new QGridLayout;
    optionLayout->setColumnStretch(0, 1);
    optionLayout->setColumnStretch(0, 1);
    optionLayout->addWidget(lineLookGroupBox,0,0);
    optionLayout->addWidget(lineColorGroupBox,0,1);
    optionLayout->addWidget(lineSignalGroupBox,1,0);
    optionLayout->addWidget(lineDisturbGroupBox,1,1);


    // layout in cui si insrisce l'immagine del test
    QVBoxLayout *lineTestImageLayout = new QVBoxLayout;
    //    lineTestImageLayout->addWidget(imageLabel,0,Qt::AlignCenter);
    lineTestImageLayout->setContentsMargins(100,0,100,0);
    // layout dei pulsanti di start stop e report
    QHBoxLayout *lineLayoutH = new QHBoxLayout;
    lineLayoutH->addWidget(lineStartButton,0,Qt::AlignBottom);
    lineLayoutH->addWidget(lineStopButton,0,Qt::AlignBottom);
    lineLayoutH->addWidget(lineDefaultSettingsButton,0,Qt::AlignBottom);
    lineLayoutH->addWidget(lineSaveSettingsButton,0,Qt::AlignBottom);

    float backGroundColor[3] = {lineBackgroundColorR_SpinBox->value(), lineBackgroundColorG_SpinBox->value(), lineBackgroundColorB_SpinBox->value()};
    float signalColor[3] = {lineSignalColorR_SpinBox->value(), lineSignalColorG_SpinBox->value(), lineSignalColorB_SpinBox->value()};
    float disturbColor[3] = {lineDisturbColorR_SpinBox->value(), lineDisturbColorG_SpinBox->value(), lineDisturbColorB_SpinBox->value()};
    float touchColor[3] = {lineTouchColorR_SpinBox->value(), lineTouchColorG_SpinBox->value(), lineTouchColorB_SpinBox->value()};
    float lineColor[3] = {lineLimitColorR_SpinBox->value(), lineLimitColorG_SpinBox->value(), lineLimitColorB_SpinBox->value()};

    lineSetupGlWindow = new LineSetupGlWindow(backGroundColor,
                                              signalColor,
                                              disturbColor,
                                              touchColor,
                                              lineColor,
                                              lineSignalTargetDimension->value(),
                                              lineDisturbTargetDimension->value(),
                                              lineTouchTargetDimension->value(),
                                              lineLimitTargetDimension->value());

    connect(lineBackgroundColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineBackgroundColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineBackgroundColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineSignalColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineSignalColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineSignalColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineDisturbColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineDisturbColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineDisturbColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineTouchColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineTouchColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineTouchColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineLimitColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineLimitColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineLimitColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineSignalTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineDisturbTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineTouchTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));
    connect(lineLimitTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(lineBackgroundColorSpinBoxChange()));

    connect(this,SIGNAL(lineBackgroungColorChange( float* ,
                                                   float* ,
                                                   float* ,
                                                   float* ,
                                                   float* ,
                                                   int ,
                                                   int ,
                                                   int ,
                                                   int )),lineSetupGlWindow,SLOT(updateColorWidth( float* ,
                                                                                                   float* ,
                                                                                                   float* ,
                                                                                                   float* ,
                                                                                                   float* ,
                                                                                                   int ,
                                                                                                   int ,
                                                                                                   int ,
                                                                                                   int )));
    QGroupBox *lineSetupWindowGroupbox =  new QGroupBox();
    QVBoxLayout *lineSetupWindow = new QVBoxLayout;
    lineSetupWindowGroupbox->setStyleSheet(stileWidgetSetup);
    lineSetupWindow->addWidget(lineSetupGlWindow);
    lineSetupWindowGroupbox->setLayout(lineSetupWindow);

    // layout generale della pagina del test
    QVBoxLayout *lineLayout = new QVBoxLayout;
    lineLayout->addLayout(lineTestImageLayout);
    lineLayout->addWidget(lineSetupWindowGroupbox);
    lineLayout->addLayout(optionLayout);
    lineLayout->addLayout(lineLayoutH);

    linePage->setLayout(lineLayout);
    connect(lineStartButton, SIGNAL(clicked()),
            this, SLOT(newLineTestClicked()));
    connect(lineStartButton, SIGNAL(clicked()), this, SLOT(lineWriteSetting()));
    connect(lineDefaultSettingsButton, SIGNAL(clicked()), this, SLOT(lineDefaultSetting()));
    connect(lineSaveSettingsButton, SIGNAL(clicked()), this, SLOT(lineWriteSetting()));
}

void FindDialog::lineBackgroundColorSpinBoxChange()
{
    //    exit(80);
    float backGroundColor[3] = {lineBackgroundColorR_SpinBox->value(), lineBackgroundColorG_SpinBox->value(), lineBackgroundColorB_SpinBox->value()};
    float signalColor[3] = {lineSignalColorR_SpinBox->value(), lineSignalColorG_SpinBox->value(), lineSignalColorB_SpinBox->value()};
    float disturbColor[3] = {lineDisturbColorR_SpinBox->value(), lineDisturbColorG_SpinBox->value(), lineDisturbColorB_SpinBox->value()};
    float touchColor[3] = {lineTouchColorR_SpinBox->value(), lineTouchColorG_SpinBox->value(), lineTouchColorB_SpinBox->value()};
    float lineColor[3] = {lineLimitColorR_SpinBox->value(), lineLimitColorG_SpinBox->value(), lineLimitColorB_SpinBox->value()};

    emit lineBackgroungColorChange(backGroundColor,
                                   signalColor,
                                   disturbColor,
                                   touchColor,
                                   lineColor,
                                   lineSignalTargetDimension->value(),
                                   lineDisturbTargetDimension->value(),
                                   lineTouchTargetDimension->value(),
                                   lineLimitTargetDimension->value());
}

void FindDialog::setLineSignalActiveDeative(int state_tmp)
{
    if (state_tmp == 2) lineSignalSlectionComboBox->setDisabled(false);
    else lineSignalSlectionComboBox->setDisabled(true);
}

void FindDialog::setLineDisturbActiveDeative(int state_tmp)
{
    if (state_tmp == 2) lineDisturbSlectionComboBox->setDisabled(false);
    else lineDisturbSlectionComboBox->setDisabled(true);
}

void FindDialog::lineMsgBox( double tc_tmp, double tcSignal_tmp, double tcDisturb_tmp)
{
    QMessageBox msgBox;
    msgBox.setText("SAMPLE TIME ERROR");
    msgBox.setInformativeText(QString("Sample time limit : %1 \nSample time signal: %2 \nSample time disturb: %3").arg(tc_tmp).arg(tcSignal_tmp).arg(tcDisturb_tmp));
    msgBox.setStandardButtons(QMessageBox::Ok );
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Ok:
        break;
    case QMessageBox::Discard:
        break;
    case QMessageBox::Cancel:
        break;
    default:
        break;
    }
    msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
}

void FindDialog::lineWriteSetting()
{
    float backGroundColor[3] = {lineBackgroundColorR_SpinBox->value(), lineBackgroundColorG_SpinBox->value(), lineBackgroundColorB_SpinBox->value()};
    float signalColor[3] = {lineSignalColorR_SpinBox->value(), lineSignalColorG_SpinBox->value(), lineSignalColorB_SpinBox->value()};
    float disturbColor[3] = {lineDisturbColorR_SpinBox->value(), lineDisturbColorG_SpinBox->value(), lineDisturbColorB_SpinBox->value()};
    float touchColor[3] = {lineTouchColorR_SpinBox->value(), lineTouchColorG_SpinBox->value(), lineTouchColorB_SpinBox->value()};
    float lineColor[3] = {lineLimitColorR_SpinBox->value(), lineLimitColorG_SpinBox->value(), lineLimitColorB_SpinBox->value()};

    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/line");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "lineTest");
    qDebug()<< settings.fileName();
    settings.setValue("Tc", lineTcSpinBox->value());
    settings.setValue("storedSignal",lineStoredNonStoredSignal->isChecked());
    settings.setValue("storedDisturb", lineStoredNonStoredDisturb->isChecked());
    settings.setValue("durataTest", lineSpinBox->value());
    settings.setValue("colorBackgroundR",backGroundColor[0]);
    settings.setValue("colorBackgroundG",backGroundColor[1]);
    settings.setValue("colorBackgroundB",backGroundColor[2]);
    settings.setValue("signalColorR",signalColor[0]);
    settings.setValue("signalColorG",signalColor[1]);
    settings.setValue("signalColorB",signalColor[2]);
    settings.setValue("disturbColorR",disturbColor[0]);
    settings.setValue("disturbColorG",disturbColor[1]);
    settings.setValue("disturbColorB",disturbColor[2]);
    settings.setValue("touchColorR",touchColor[0]);
    settings.setValue("touchColorG",touchColor[1]);
    settings.setValue("touchColorB",touchColor[2]);
    settings.setValue("lineColorR",lineColor[0]);
    settings.setValue("lineColorG",lineColor[1]);
    settings.setValue("lineColorB",lineColor[2]);
    settings.setValue("signalTargetWidth", lineSignalTargetDimension->value());
    settings.setValue("disturbTargetWidth", lineDisturbTargetDimension->value());
    settings.setValue("touchTargetWidth", lineTouchTargetDimension->value());
    settings.setValue("limitTargetWidth", lineLimitTargetDimension->value());
    settings.setValue("disturbYN", lineDisturbYN->isChecked());
    settings.setValue("tAtteso", lineTempoSaltoSpinBox->value());
    settings.setValue("tAttesoDisturbo", lineTempoSaltoDisturboSpinBox->value());
    settings.setValue("lineLFPmeasuring",lineLFPmeasuring->isChecked());
        qDebug() << "writing line settings";
}

void FindDialog::lineReadSetting()
{

    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/line");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "lineTest");
    if(settings.contains("Tc"))
    {
        qDebug()<< settings.fileName();
        lineTcSpinBox->setValue(settings.value("Tc").toInt());
        lineStoredNonStoredSignal->setChecked(settings.value("storedSignal").toBool());
        lineStoredNonStoredDisturb->setChecked(settings.value("storedDisturb").toBool());
        lineSpinBox->setValue(settings.value("durataTest").toInt());
        lineBackgroundColorR_SpinBox->setValue(settings.value("colorBackgroundR").toInt());
        lineBackgroundColorG_SpinBox->setValue(settings.value("colorBackgroundG").toInt());
        lineBackgroundColorB_SpinBox->setValue(settings.value("colorBackgroundB").toInt());
        lineSignalColorR_SpinBox->setValue(settings.value("signalColorR").toInt());
        lineSignalColorG_SpinBox->setValue(settings.value("signalColorG").toInt());
        lineSignalColorB_SpinBox->setValue(settings.value("signalColorB").toInt());
        lineDisturbColorR_SpinBox->setValue(settings.value("disturbColorR").toInt());
        lineDisturbColorG_SpinBox->setValue(settings.value("disturbColorG").toInt());
        lineDisturbColorB_SpinBox->setValue(settings.value("disturbColorB").toInt());
        lineTouchColorR_SpinBox->setValue(settings.value("touchColorR").toInt());
        lineTouchColorG_SpinBox->setValue(settings.value("touchColorG").toInt());
        lineTouchColorB_SpinBox->setValue(settings.value("touchColorB").toInt());
        lineLimitColorR_SpinBox->setValue(settings.value("lineColorR").toInt());
        lineLimitColorG_SpinBox->setValue(settings.value("lineColorG").toInt());
        lineLimitColorB_SpinBox->setValue(settings.value("lineColorB").toInt());
        lineSignalTargetDimension->setValue(settings.value("signalTargetWidth").toInt());
        lineDisturbTargetDimension->setValue(settings.value("disturbTargetWidth").toInt());
        lineTouchTargetDimension->setValue(settings.value("touchTargetWidth").toInt());
        lineLimitTargetDimension->setValue(settings.value("limitTargetWidth").toInt());
        lineDisturbYN->setChecked(settings.value("disturbYN").toBool());
        lineTempoSaltoSpinBox->setValue(settings.value("tAtteso").toFloat());
        lineTempoSaltoDisturboSpinBox->setValue(settings.value("tAttesoDisturbo").toFloat());
        lineLFPmeasuring->setChecked(settings.value("lineLFPmeasuring").toBool());
            qDebug() << "reading line settings";
    }
    else lineWriteSetting();
}

void FindDialog::lineDefaultSetting()
{
    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/line");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "lineTest");
    qDebug()<< settings.fileName();
    settings.setValue("Tc", 16);
    settings.setValue("storedSignal",false);
    settings.setValue("storedDisturb", false);
    settings.setValue("durataTest", 150);
    settings.setValue("colorBackgroundR",99);
    settings.setValue("colorBackgroundG",99);
    settings.setValue("colorBackgroundB",99);
    settings.setValue("signalColorR",0);
    settings.setValue("signalColorG",99);
    settings.setValue("signalColorB",99);
    settings.setValue("disturbColorR",99);
    settings.setValue("disturbColorG",0);
    settings.setValue("disturbColorB",0);
    settings.setValue("touchColorR",0);
    settings.setValue("touchColorG",0);
    settings.setValue("touchColorB",0);
    settings.setValue("lineColorR",0);
    settings.setValue("lineColorG",0);
    settings.setValue("lineColorB",0);
    settings.setValue("signalTargetWidth", 8);
    settings.setValue("disturbTargetWidth", 8);
    settings.setValue("touchTargetWidth", 2);
    settings.setValue("limitTargetWidth", 2);
    settings.setValue("disturbYN", true);
    settings.setValue("tAtteso", 1.4);
    settings.setValue("tAttesoDisturbo", 1.4);
    settings.setValue("lineLFPmeasuring",false);
        qDebug() << "default line settings";
    lineReadSetting();
}

void FindDialog::newLineTestClicked()
{
    int disturbYN = 0;
    int storedSignalChk = 0;
    int storedDisturbChk = 0;
    int lineLFPmeasuringChk = 0;
    if ( lineStoredNonStoredSignal->isChecked()) storedSignalChk = 1;
    if ( lineStoredNonStoredDisturb->isChecked()) storedDisturbChk = 1;
    if ( lineDisturbYN->isChecked()) disturbYN = 1;
    if ( lineLFPmeasuring->isChecked()) lineLFPmeasuringChk = 1;
    float backGroundColor[3] = {lineBackgroundColorR_SpinBox->value(), lineBackgroundColorG_SpinBox->value(), lineBackgroundColorB_SpinBox->value()};
    float signalColor[3] = {lineSignalColorR_SpinBox->value(), lineSignalColorG_SpinBox->value(), lineSignalColorB_SpinBox->value()};
    float disturbColor[3] = {lineDisturbColorR_SpinBox->value(), lineDisturbColorG_SpinBox->value(), lineDisturbColorB_SpinBox->value()};
    float touchColor[3] = {lineTouchColorR_SpinBox->value(), lineTouchColorG_SpinBox->value(), lineTouchColorB_SpinBox->value()};
    float lineColor[3] = {lineLimitColorR_SpinBox->value(), lineLimitColorG_SpinBox->value(), lineLimitColorB_SpinBox->value()};

    qDebug()<< fileNameOutput + "//User//"+nameLabel->text()+"//line";

    if(!QDir().exists(fileNameOutput + "/User/"+nameLabel->text()+"/line"))QDir().mkdir(fileNameOutput + "/User/"+nameLabel->text()+ "/line");

    newLineTest = new Line((double(lineTcSpinBox->value())/1000),
                           storedSignalChk,
                           storedDisturbChk,
                           lineSpinBox->value(),
                           backGroundColor,
                           signalColor,
                           disturbColor,
                           touchColor,
                           lineColor,
                           lineSignalTargetDimension->value(),
                           lineDisturbTargetDimension->value(),
                           lineTouchTargetDimension->value(),
                           lineLimitTargetDimension->value(),
                           disturbYN,
                           QCoreApplication::applicationDirPath()+ "/Data/"+lineSignalSlectionComboBox->currentText(),
                           QCoreApplication::applicationDirPath()+ "/Data/"+lineDisturbSlectionComboBox->currentText(),
                           fileNameOutput + "/User/"+nameLabel->text()+ "/line",
                           lineTempoSaltoSpinBox->value(),
                           lineTempoSaltoDisturboSpinBox->value(),
                           lineLFPmeasuringChk);
    newLineTest->showFullScreen();
    //    newLineTest->showMaximized();
    connect(newLineTest, SIGNAL(tcError( double, double, double )), this, SLOT(lineMsgBox( double, double, double )) );

}

void FindDialog::createFtForceTestPage(){

    // inizializzazione della immagini da usare nella pagina del test
    QPixmap pixmapStart = QPixmap("Image/General/Play.png");
    QIcon iconStart(pixmapStart);
    QPixmap pixmapStop = QPixmap("Image/General/Stop.png");
    QIcon iconStop(pixmapStop);
    QPixmap pixmapSave = QPixmap("Image/General/Save.png");
    QIcon iconSave(pixmapSave);
    QPixmap pixmapRestore = QPixmap("Image/General/Restore.png");
    QIcon iconRestore(pixmapRestore);

    QPixmap image = QPixmap("Image/FtForceTest/initialImage.png");
    QLabel *imageLabel = new QLabel();
    imageLabel->setStyleSheet("color: blue;"
                              "height = 144px"
                              "background-color: white;"
                              );
    imageLabel->setPixmap(image);

    ftForceTestPage = new QWidget;

    // inizialzzazione di dei pulsanti spinbox etc da usare nella pagina del test
    ftForceTestStartButton = new QToolButton;
    ftForceTestStartButton->setIcon(iconStart);
    ftForceTestStartButton->setIconSize(QSize(80,80));
    ftForceTestStartButton->setEnabled(true);
    ftForceTestStartButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    ftForceTestStartButton->setAutoRaise(true);
    ftForceTestStartButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                          "border-style:none; }"
                                          "QToolButton{font:12pt;color:white}");

    ftForceTestStopButton = new QToolButton;
    ftForceTestStopButton->setIcon(iconStop);
    ftForceTestStopButton->setIconSize(QSize(80,80));
    ftForceTestStopButton->setEnabled(true);
    ftForceTestStopButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    ftForceTestStopButton->setAutoRaise(true);
    ftForceTestStopButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                         "border-style:none; }"
                                         "QToolButton{font:12pt;color:white}");
    ftForceTestStopButton->setVisible(false);

    ftForceTestDefaultSettingsButton = new QToolButton;
    ftForceTestDefaultSettingsButton->setIcon(iconRestore);
    ftForceTestDefaultSettingsButton->setIconSize(QSize(80,80));
    ftForceTestDefaultSettingsButton->setEnabled(true);
    ftForceTestDefaultSettingsButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    ftForceTestDefaultSettingsButton->setAutoRaise(true);
    ftForceTestDefaultSettingsButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                                    "border-style:none; }"
                                                    "QToolButton{font:12pt;color:white}");

    ftForceTestSaveSettingsButton = new QToolButton;
    ftForceTestSaveSettingsButton->setIcon(iconSave);
    ftForceTestSaveSettingsButton->setIconSize(QSize(80,80));
    ftForceTestSaveSettingsButton->setEnabled(true);
    ftForceTestSaveSettingsButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    ftForceTestSaveSettingsButton->setAutoRaise(true);
    ftForceTestSaveSettingsButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                                 "border-style:none; }"
                                                 "QToolButton{font:12pt;color:white}");


    ftForceTestSpinBox = new QSpinBox () ;
    ftForceTestSpinBox->setRange(0, 5000);
    ftForceTestSpinBox->setValue(150);
    //    ftForceTestSpinBox->setPrefix("Total time ( max 5 min )   ");
    ftForceTestSpinBox->setSuffix(" [s]");
    //ftForceTestSpinBox->setMaximumWidth(105);
    QLabel *totalTimeLabel = new QLabel(tr("TOTAL TIME:"));

    ftForceTestTempoSaltoSpinBox = new QDoubleSpinBox () ;
    ftForceTestTempoSaltoSpinBox->setRange(0, 5);
    ftForceTestTempoSaltoSpinBox->setValue(1.4);
    //    ftForceTestSpinBox->setPrefix("Total time ( max 5 min )   ");
    ftForceTestTempoSaltoSpinBox->setSuffix(" [s]");
    //ftForceTestSpinBox->setMaximumWidth(105);
    ftForceTestTempoSaltoSpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTempoSaltoLabel = new QLabel(tr("Mean Jump Time (s):"));

    ftForceTestTempoSaltoDisturboSpinBox = new QDoubleSpinBox () ;
    ftForceTestTempoSaltoDisturboSpinBox->setRange(0, 5);
    ftForceTestTempoSaltoDisturboSpinBox->setValue(1.4);
    //    ftForceTestSpinBox->setPrefix("Total time ( max 5 min )   ");
    ftForceTestTempoSaltoDisturboSpinBox->setSuffix(" [s]");
    //ftForceTestSpinBox->setMaximumWidth(105);
    ftForceTestTempoSaltoDisturboSpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTempoSaltoDsiturboLabel = new QLabel(tr("Mean Jump Time Disturbance (s):"));

    ftForceTestTcSpinBox = new QSpinBox () ;
    ftForceTestTcSpinBox->setValue(16);
    ftForceTestTcSpinBox->setRange(0, 1000);
    //    ftForceTestTcSpinBox->setPrefix("sampling time");
    //ftForceTestTcSpinBox->setMaximumWidth(105);
    ftForceTestTcSpinBox->setSuffix(" [ms]");
    QLabel *tcLabel = new QLabel(tr("SAMPLING TIME:"));

    QLabel *signalLabel = new QLabel(tr("SIGNAL:"));
    QLabel *disturbDataLabel = new QLabel(tr("DISTURB:"));
    ftForceTestSignalSlectionComboBox = new QComboBox();
    ftForceTestDisturbSlectionComboBox = new QComboBox();
    ftForceTestDisturbSlectionComboBox = new QComboBox();
    QDir path("Data");
    QStringList all_dirs = path.entryList(QDir::Files);
    ftForceTestSignalSlectionComboBox->addItems(all_dirs );
    ftForceTestDisturbSlectionComboBox->addItems(all_dirs );

    QLabel *inputSignalLabel = new QLabel(tr("STORED SIGNAL (CHK =TRUE)"));
    ftForceTestStoredNonStoredSignal = new QCheckBox();
    ftForceTestStoredNonStoredSignal->setChecked(false);
    connect(ftForceTestStoredNonStoredSignal, SIGNAL(	stateChanged(int)),
            this, SLOT(setFtForceTestSignalActiveDeative(int)));

    QLabel *inputDisturbLabel = new QLabel(tr("STORED SIGNAL (CHK =TRUE)"));
    ftForceTestStoredNonStoredDisturb = new QCheckBox();
    ftForceTestStoredNonStoredDisturb->setChecked(false);
    connect(ftForceTestStoredNonStoredDisturb, SIGNAL(	stateChanged(int)),
            this, SLOT(setFtForceTestDisturbActiveDeative(int)));

    QLabel *disturbLabel = new QLabel(tr("DISTURB (CHK =TRUE)"));
    ftForceTestDisturbYN = new QCheckBox();
    ftForceTestDisturbYN->setChecked(true);

    QLabel *ftForceTestLFPmeasuringLabel = new QLabel(tr("LFP measuring (CHK =TRUE)"));
    ftForceTestLFPmeasuring = new QCheckBox();
    ftForceTestLFPmeasuring->setChecked(false);

    ftForceTestSignalTargetDimension = new QSpinBox () ;
    ftForceTestSignalTargetDimension->setRange(2, 50);
    ftForceTestSignalTargetDimension->setValue(20);
    ftForceTestSignalTargetDimension->setSuffix(" [px]");
    //ftForceTestSignalTargetDimension->setMaximumWidth(105);
    ftForceTestSignalTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestSignalTargetLabel = new QLabel(tr("Signal target dimension:"));

    ftForceTestDisturbTargetDimension = new QSpinBox () ;
    ftForceTestDisturbTargetDimension->setRange(2, 50);
    ftForceTestDisturbTargetDimension->setValue(20);
    ftForceTestDisturbTargetDimension->setSuffix(" [px]");
    //ftForceTestDisturbTargetDimension->setMaximumWidth(105);
    ftForceTestDisturbTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestDisturbTargetLabel = new QLabel(tr("Disturb target dimension:"));

    ftForceTestTouchTargetDimension = new QSpinBox () ;
    ftForceTestTouchTargetDimension->setRange(2, 50);
    ftForceTestTouchTargetDimension->setValue(2);
    ftForceTestTouchTargetDimension->setSuffix(" [px]");
    //ftForceTestTouchTargetDimension->setMaximumWidth(105);
    ftForceTestTouchTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTouchTargetLabel = new QLabel(tr("Touch target dimension:"));

    ftForceTestLimitTargetDimension = new QSpinBox () ;
    ftForceTestLimitTargetDimension->setRange(2, 50);
    ftForceTestLimitTargetDimension->setValue(50);
    ftForceTestLimitTargetDimension->setSuffix(" [px]");
    //ftForceTestLimitTargetDimension->setMaximumWidth(105);
    ftForceTestLimitTargetDimension->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestLimitTargetLabel = new QLabel(tr("Limit target dimension:"));

    ftForceTestBackgroundColorR_SpinBox = new QSpinBox () ;
    ftForceTestBackgroundColorR_SpinBox->setValue(100);
    ftForceTestBackgroundColorR_SpinBox->setRange(0, 100);
    //ftForceTestBackgroundColorR_SpinBox->setMaximumWidth(105);
    ftForceTestBackgroundColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestBackgroundColorRLabel = new QLabel(tr("R:"));

    ftForceTestBackgroundColorG_SpinBox = new QSpinBox () ;
    ftForceTestBackgroundColorG_SpinBox->setValue(100);
    ftForceTestBackgroundColorG_SpinBox->setRange(0, 100);
    //ftForceTestBackgroundColorG_SpinBox->setMaximumWidth(105);
    ftForceTestBackgroundColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestBackgroundColorGLabel = new QLabel(tr("G:"));

    ftForceTestBackgroundColorB_SpinBox = new QSpinBox () ;
    ftForceTestBackgroundColorB_SpinBox->setValue(100);
    ftForceTestBackgroundColorB_SpinBox->setRange(0, 100);
    //ftForceTestBackgroundColorB_SpinBox->setMaximumWidth(105);
    ftForceTestBackgroundColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestBackgroundColorBLabel = new QLabel(tr("B:"));

    ftForceTestBackgroundColorA_SpinBox = new QSpinBox () ;
    ftForceTestBackgroundColorA_SpinBox->setValue(100);
    ftForceTestBackgroundColorA_SpinBox->setRange(0, 100);
    //ftForceTestBackgroundColorA_SpinBox->setMaximumWidth(105);
    ftForceTestBackgroundColorA_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestBackgroundColorALabel = new QLabel(tr("Alpha:"));

    ftForceTestSignalColorR_SpinBox = new QSpinBox () ;
    ftForceTestSignalColorR_SpinBox->setValue(0);
    ftForceTestSignalColorR_SpinBox->setRange(0, 100);
    //ftForceTestSignalColorR_SpinBox->setMaximumWidth(105);
    ftForceTestSignalColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestSignalColorRLabel = new QLabel(tr("R:"));

    ftForceTestSignalColorG_SpinBox = new QSpinBox () ;
    ftForceTestSignalColorG_SpinBox->setValue(100);
    ftForceTestSignalColorG_SpinBox->setRange(0, 100);
    //ftForceTestSignalColorG_SpinBox->setMaximumWidth(105);
    ftForceTestSignalColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestSignalColorGLabel = new QLabel(tr("G:"));

    ftForceTestSignalColorB_SpinBox = new QSpinBox () ;
    ftForceTestSignalColorB_SpinBox->setValue(100);
    ftForceTestSignalColorB_SpinBox->setRange(0, 100);
    //ftForceTestSignalColorB_SpinBox->setMaximumWidth(105);
    ftForceTestSignalColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestSignalColorBLabel = new QLabel(tr("B:"));

    ftForceTestSignalColorA_SpinBox = new QSpinBox () ;
    ftForceTestSignalColorA_SpinBox->setValue(50);
    ftForceTestSignalColorA_SpinBox->setRange(0, 100);
    //ftForceTestSignalColorA_SpinBox->setMaximumWidth(105);
    ftForceTestSignalColorA_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestSignalColorALabel = new QLabel(tr("Alpha:"));

    ftForceTestDisturbColorR_SpinBox = new QSpinBox () ;
    ftForceTestDisturbColorR_SpinBox->setValue(100);
    ftForceTestDisturbColorR_SpinBox->setRange(0, 100);
    //ftForceTestDisturbColorR_SpinBox->setMaximumWidth(105);
    ftForceTestDisturbColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestDisturbColorRLabel = new QLabel(tr("R:"));

    ftForceTestDisturbColorG_SpinBox = new QSpinBox () ;
    ftForceTestDisturbColorG_SpinBox->setValue(0);
    ftForceTestDisturbColorG_SpinBox->setRange(0, 100);
    //ftForceTestDisturbColorG_SpinBox->setMaximumWidth(105);
    ftForceTestDisturbColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestDisturbColorGLabel = new QLabel(tr("G:"));

    ftForceTestDisturbColorB_SpinBox = new QSpinBox () ;
    ftForceTestDisturbColorB_SpinBox->setValue(0);
    ftForceTestDisturbColorB_SpinBox->setRange(0, 100);
    //ftForceTestDisturbColorB_SpinBox->setMaximumWidth(105);
    ftForceTestDisturbColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestDisturbColorBLabel = new QLabel(tr("B:"));

    ftForceTestDisturbColorA_SpinBox = new QSpinBox () ;
    ftForceTestDisturbColorA_SpinBox->setValue(50);
    ftForceTestDisturbColorA_SpinBox->setRange(0, 100);
    //ftForceTestDisturbColorA_SpinBox->setMaximumWidth(105);
    ftForceTestDisturbColorA_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestDisturbColorALabel = new QLabel(tr("Alpha:"));

    ftForceTestTouchColorR_SpinBox = new QSpinBox () ;
    ftForceTestTouchColorR_SpinBox->setValue(0);
    ftForceTestTouchColorR_SpinBox->setRange(0, 100);
    //ftForceTestTouchColorR_SpinBox->setMaximumWidth(105);
    ftForceTestTouchColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTouchColorRLabel = new QLabel(tr("R:"));

    ftForceTestTouchColorG_SpinBox = new QSpinBox () ;
    ftForceTestTouchColorG_SpinBox->setValue(0);
    ftForceTestTouchColorG_SpinBox->setRange(0, 100);
    //ftForceTestTouchColorG_SpinBox->setMaximumWidth(105);
    ftForceTestTouchColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTouchColorGLabel = new QLabel(tr("G:"));

    ftForceTestTouchColorB_SpinBox = new QSpinBox () ;
    ftForceTestTouchColorB_SpinBox->setValue(0);
    ftForceTestTouchColorB_SpinBox->setRange(0, 100);
    //ftForceTestTouchColorB_SpinBox->setMaximumWidth(105);
    ftForceTestTouchColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTouchColorBLabel = new QLabel(tr("B:"));

    ftForceTestTouchColorA_SpinBox = new QSpinBox () ;
    ftForceTestTouchColorA_SpinBox->setValue(100);
    ftForceTestTouchColorA_SpinBox->setRange(0, 100);
    //ftForceTestTouchColorA_SpinBox->setMaximumWidth(105);
    ftForceTestTouchColorA_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestTouchColorALabel = new QLabel(tr("Alpha:"));

    ftForceTestLimitColorR_SpinBox = new QSpinBox () ;
    ftForceTestLimitColorR_SpinBox->setValue(0);
    ftForceTestLimitColorR_SpinBox->setRange(0, 100);
    //ftForceTestLimitColorR_SpinBox->setMaximumWidth(105);
    ftForceTestLimitColorR_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestLimitColorRLabel = new QLabel(tr("R:"));

    ftForceTestLimitColorG_SpinBox = new QSpinBox () ;
    ftForceTestLimitColorG_SpinBox->setValue(0);
    ftForceTestLimitColorG_SpinBox->setRange(0, 100);
    //ftForceTestLimitColorG_SpinBox->setMaximumWidth(105);
    ftForceTestLimitColorG_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestLimitColorGLabel = new QLabel(tr("G:"));

    ftForceTestLimitColorB_SpinBox = new QSpinBox () ;
    ftForceTestLimitColorB_SpinBox->setValue(0);
    ftForceTestLimitColorB_SpinBox->setRange(0, 100);
    //ftForceTestLimitColorB_SpinBox->setMaximumWidth(105);
    ftForceTestLimitColorB_SpinBox->setStyleSheet(stileSpinBox);
    QLabel *ftForceTestLimitColorBLabel = new QLabel(tr("B:"));

    QGroupBox *ftForceTestColorGroupBox = new QGroupBox();
    QGridLayout *ftForceTestColorLayout = new QGridLayout;

    QGroupBox *ftForceTestBackgroundColorGroupBox = new QGroupBox();
    QGridLayout *ftForceTestBackgroundColorLayout = new QGridLayout;
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorRLabel,0,0);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorR_SpinBox,0,1);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorGLabel,1,0);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorG_SpinBox,1,1);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorBLabel,2,0);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorB_SpinBox,2,1);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorALabel,3,0);
    ftForceTestBackgroundColorLayout->addWidget(ftForceTestBackgroundColorA_SpinBox,3,1);

    ftForceTestBackgroundColorGroupBox->setTitle("Background color");
    ftForceTestBackgroundColorGroupBox->setLayout(ftForceTestBackgroundColorLayout);

    QGroupBox *ftForceTestSignalColorGroupBox = new QGroupBox();
    QGridLayout *ftForceTestSignalColorLayout = new QGridLayout;
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorRLabel,0,0);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorR_SpinBox,0,1);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorGLabel,1,0);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorG_SpinBox,1,1);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorBLabel,2,0);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorB_SpinBox,2,1);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorALabel,3,0);
    ftForceTestSignalColorLayout->addWidget(ftForceTestSignalColorA_SpinBox,3,1);

    ftForceTestSignalColorGroupBox->setTitle("Signal color");
    ftForceTestSignalColorGroupBox->setLayout(ftForceTestSignalColorLayout);

    QGroupBox *ftForceTestDisturbColorGroupBox = new QGroupBox();
    QGridLayout *ftForceTestDisturbColorLayout = new QGridLayout;
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorRLabel,0,0);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorR_SpinBox,0,1);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorGLabel,1,0);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorG_SpinBox,1,1);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorBLabel,2,0);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorB_SpinBox,2,1);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorALabel,3,0);
    ftForceTestDisturbColorLayout->addWidget(ftForceTestDisturbColorA_SpinBox,3,1);

    ftForceTestDisturbColorGroupBox->setTitle("Disturb color");
    ftForceTestDisturbColorGroupBox->setLayout(ftForceTestDisturbColorLayout);

    QGroupBox *ftForceTestTouchColorGroupBox = new QGroupBox();
    QGridLayout *ftForceTestTouchColorLayout = new QGridLayout;
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorRLabel,0,0);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorR_SpinBox,0,1);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorGLabel,1,0);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorG_SpinBox,1,1);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorBLabel,2,0);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorB_SpinBox,2,1);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorALabel,3,0);
    ftForceTestTouchColorLayout->addWidget(ftForceTestTouchColorA_SpinBox,3,1);

    ftForceTestTouchColorGroupBox->setTitle("Touch color");
    ftForceTestTouchColorGroupBox->setLayout(ftForceTestTouchColorLayout);

    QGroupBox *ftForceTestLimitColorGroupBox = new QGroupBox();
    QGridLayout *ftForceTestLimitColorLayout = new QGridLayout;
    ftForceTestLimitColorLayout->addWidget(ftForceTestLimitColorRLabel,0,0);
    ftForceTestLimitColorLayout->addWidget(ftForceTestLimitColorR_SpinBox,0,1);
    ftForceTestLimitColorLayout->addWidget(ftForceTestLimitColorGLabel,1,0);
    ftForceTestLimitColorLayout->addWidget(ftForceTestLimitColorG_SpinBox,1,1);
    ftForceTestLimitColorLayout->addWidget(ftForceTestLimitColorBLabel,2,0);
    ftForceTestLimitColorLayout->addWidget(ftForceTestLimitColorB_SpinBox,2,1);

    ftForceTestLimitColorGroupBox->setTitle("Limit color");
    ftForceTestLimitColorGroupBox->setLayout(ftForceTestLimitColorLayout);

    // raccogliere le opzioni simili in gruop box
    QGroupBox *ftForceTestSignalGroupBox = new QGroupBox();
    QGridLayout *ftForceTestSignalLayout = new QGridLayout;

    ftForceTestSignalLayout->addWidget(signalLabel,2,0);
    ftForceTestSignalLayout->addWidget(ftForceTestStoredNonStoredSignal,2,1);
    //    ftForceTestSpinBoxLayout->addWidget(tcLabel,1,0);
    //    ftForceTestSpinBoxLayout->addWidget(ftForceTestTcSpinBox,1,1);
    ftForceTestSignalLayout->addWidget(totalTimeLabel,0,0);
    ftForceTestSignalLayout->addWidget(ftForceTestSpinBox,0,1);
    ftForceTestSignalLayout->addWidget(inputSignalLabel,3,0);
    ftForceTestSignalLayout->addWidget(ftForceTestSignalSlectionComboBox,3,1);
    ftForceTestSignalLayout->addWidget(ftForceTestTempoSaltoLabel,4,0);
    ftForceTestSignalLayout->addWidget(ftForceTestTempoSaltoSpinBox,4,1);
    ftForceTestSignalLayout->addWidget(ftForceTestLFPmeasuringLabel,5,0);
    ftForceTestSignalLayout->addWidget(ftForceTestLFPmeasuring,5,1);
    ftForceTestSignalGroupBox->setStyleSheet(stileGroupBox);
    ftForceTestSignalGroupBox->setTitle("OPTION SIGNAL");
    ftForceTestSignalGroupBox->setLayout(ftForceTestSignalLayout);

    QGroupBox *ftForceTestDisturbGroupBox = new QGroupBox();
    QGridLayout *ftForceTestDisturbLayout = new QGridLayout;

    ftForceTestDisturbLayout->addWidget(disturbDataLabel,1,0);
    ftForceTestDisturbLayout->addWidget(ftForceTestStoredNonStoredDisturb,1,1);
    ftForceTestDisturbLayout->addWidget(inputDisturbLabel,2,0);
    ftForceTestDisturbLayout->addWidget(ftForceTestDisturbSlectionComboBox,2,1);
    ftForceTestDisturbLayout->addWidget(disturbLabel,0,0);
    ftForceTestDisturbLayout->addWidget(ftForceTestDisturbYN,0,1);
    ftForceTestDisturbLayout->addWidget(ftForceTestTempoSaltoDsiturboLabel,3,0);
    ftForceTestDisturbLayout->addWidget(ftForceTestTempoSaltoDisturboSpinBox,3,1);
    ftForceTestDisturbGroupBox->setStyleSheet(stileGroupBox);
    ftForceTestDisturbGroupBox->setTitle("OPTION DISTURB");
    ftForceTestDisturbGroupBox->setLayout(ftForceTestDisturbLayout);

    ftForceTestColorLayout->addWidget(ftForceTestBackgroundColorGroupBox,0,0);
    ftForceTestColorLayout->addWidget(ftForceTestSignalColorGroupBox,0,1);
    ftForceTestColorLayout->addWidget(ftForceTestDisturbColorGroupBox,0,2);
    ftForceTestColorLayout->addWidget(ftForceTestTouchColorGroupBox,0,3);
    //ftForceTestColorLayout->addWidget(ftForceTestLimitColorGroupBox,0,4);
    ftForceTestBackgroundColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    ftForceTestSignalColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    ftForceTestDisturbColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    ftForceTestTouchColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    ftForceTestLimitColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    ftForceTestColorGroupBox->setStyleSheet(stileGroupBox);
    ftForceTestColorGroupBox->setTitle("COLOR SET-UP");
    ftForceTestColorGroupBox->setLayout(ftForceTestColorLayout);

    QGroupBox *ftForceTestLookGroupBox = new QGroupBox();
    QGridLayout *ftForceTestLookLayout = new QGridLayout;
    ftForceTestLookLayout->addWidget(ftForceTestSignalTargetLabel,0,0);
    ftForceTestLookLayout->addWidget(ftForceTestSignalTargetDimension,0,1);
    ftForceTestLookLayout->addWidget(ftForceTestDisturbTargetLabel,1,0);
    ftForceTestLookLayout->addWidget(ftForceTestDisturbTargetDimension,1,1);
    ftForceTestLookLayout->addWidget(ftForceTestTouchTargetLabel,2,0);
    ftForceTestLookLayout->addWidget(ftForceTestTouchTargetDimension,2,1);
    //ftForceTestLookLayout->addWidget(ftForceTestLimitTargetLabel,3,0);
    //ftForceTestLookLayout->addWidget(ftForceTestLimitTargetDimension,3,1);
    ftForceTestLookGroupBox->setStyleSheet(stileGroupBox);
    ftForceTestLookGroupBox->setTitle("General test look");
    ftForceTestLookGroupBox->setLayout(ftForceTestLookLayout  );



    // inserire in un una griglia i gruopbox creati
    QGridLayout *optionLayout = new QGridLayout;
    optionLayout->setColumnStretch(0, 1);
    optionLayout->setColumnStretch(0, 1);
    optionLayout->addWidget(ftForceTestLookGroupBox,0,0);
    optionLayout->addWidget(ftForceTestColorGroupBox,0,1);
    optionLayout->addWidget(ftForceTestSignalGroupBox,1,0);
    optionLayout->addWidget(ftForceTestDisturbGroupBox,1,1);


    // layout in cui si insrisce l'immagine del test
    QVBoxLayout *ftForceTestTestImageLayout = new QVBoxLayout;
    //    ftForceTestTestImageLayout->addWidget(imageLabel,0,Qt::AlignCenter);
    ftForceTestTestImageLayout->setContentsMargins(100,0,100,0);
    // layout dei pulsanti di start stop e report
    QHBoxLayout *ftForceTestLayoutH = new QHBoxLayout;
    ftForceTestLayoutH->addWidget(ftForceTestStartButton,0,Qt::AlignBottom);
    ftForceTestLayoutH->addWidget(ftForceTestStopButton,0,Qt::AlignBottom);
    ftForceTestLayoutH->addWidget(ftForceTestDefaultSettingsButton,0,Qt::AlignBottom);
    ftForceTestLayoutH->addWidget(ftForceTestSaveSettingsButton,0,Qt::AlignBottom);

    float backGroundColor[4] = {ftForceTestBackgroundColorR_SpinBox->value(), ftForceTestBackgroundColorG_SpinBox->value(), ftForceTestBackgroundColorB_SpinBox->value(),ftForceTestBackgroundColorA_SpinBox->value()};
    float signalColor[4] = {ftForceTestSignalColorR_SpinBox->value(), ftForceTestSignalColorG_SpinBox->value(), ftForceTestSignalColorB_SpinBox->value(), ftForceTestSignalColorA_SpinBox->value()};
    float disturbColor[4] = {ftForceTestDisturbColorR_SpinBox->value(), ftForceTestDisturbColorG_SpinBox->value(), ftForceTestDisturbColorB_SpinBox->value(), ftForceTestDisturbColorA_SpinBox->value()};
    float touchColor[4] = {ftForceTestTouchColorR_SpinBox->value(), ftForceTestTouchColorG_SpinBox->value(), ftForceTestTouchColorB_SpinBox->value(), ftForceTestTouchColorA_SpinBox->value()};
    float ftForceTestColor[4] = {ftForceTestLimitColorR_SpinBox->value(), ftForceTestLimitColorG_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value()};

    ftForceTestSetupGlWindow = new FtForceTestSetupGlWindow(backGroundColor,
                                                            signalColor,
                                                            disturbColor,
                                                            touchColor,
                                                            ftForceTestColor,
                                                            ftForceTestSignalTargetDimension->value(),
                                                            ftForceTestDisturbTargetDimension->value(),
                                                            ftForceTestTouchTargetDimension->value(),
                                                            ftForceTestLimitTargetDimension->value());

    connect(ftForceTestBackgroundColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestBackgroundColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestBackgroundColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestBackgroundColorA_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestSignalColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestSignalColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestSignalColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestSignalColorA_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestDisturbColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestDisturbColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestDisturbColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestDisturbColorA_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestTouchColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestTouchColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestTouchColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestTouchColorA_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestLimitColorR_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestLimitColorG_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestLimitColorB_SpinBox,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestSignalTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestDisturbTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestTouchTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));
    connect(ftForceTestLimitTargetDimension,SIGNAL(valueChanged(int)),this,SLOT(ftForceTestBackgroundColorSpinBoxChange()));

    connect(this,SIGNAL(ftForceTestBackgroungColorChange( float* ,
                                                          float* ,
                                                          float* ,
                                                          float* ,
                                                          float* ,
                                                          int ,
                                                          int ,
                                                          int ,
                                                          int )),ftForceTestSetupGlWindow,SLOT(updateColorWidth( float* ,
                                                                                                                 float* ,
                                                                                                                 float* ,
                                                                                                                 float* ,
                                                                                                                 float* ,
                                                                                                                 int ,
                                                                                                                 int ,
                                                                                                                 int ,
                                                                                                                 int )));
    QGroupBox *ftForceTestSetupWindowGroupbox =  new QGroupBox();
    QVBoxLayout *ftForceTestSetupWindow = new QVBoxLayout;
    ftForceTestSetupWindowGroupbox->setStyleSheet(stileWidgetSetup);
    ftForceTestSetupWindow->addWidget(ftForceTestSetupGlWindow);
    ftForceTestSetupWindowGroupbox->setLayout(ftForceTestSetupWindow);

    // layout generale della pagina del test
    QVBoxLayout *ftForceTestLayout = new QVBoxLayout;
    ftForceTestLayout->addLayout(ftForceTestTestImageLayout);
    ftForceTestLayout->addWidget(ftForceTestSetupWindowGroupbox);
    ftForceTestLayout->addLayout(optionLayout);
    ftForceTestLayout->addLayout(ftForceTestLayoutH);

    //    ftForceTestPage->setStyleSheet("QLabel { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #E0E0E0, stop: 1 #FFFFFF);}");


    ftForceTestPage->setLayout(ftForceTestLayout);
    connect(ftForceTestStartButton, SIGNAL(clicked()),
            this, SLOT(newFtForceTestClicked()));
    connect(ftForceTestStartButton, SIGNAL(clicked()), this, SLOT(ftForceTestWriteSetting()));
    connect(ftForceTestDefaultSettingsButton, SIGNAL(clicked()), this, SLOT(ftForceTestDefaultSetting()));
    connect(ftForceTestSaveSettingsButton, SIGNAL(clicked()), this, SLOT(ftForceTestWriteSetting()));

}

void FindDialog::ftForceTestBackgroundColorSpinBoxChange()
{
    //    exit(80);
    float backGroundColor[4] = {ftForceTestBackgroundColorR_SpinBox->value(), ftForceTestBackgroundColorG_SpinBox->value(), ftForceTestBackgroundColorB_SpinBox->value(),ftForceTestBackgroundColorA_SpinBox->value()};
    float signalColor[4] = {ftForceTestSignalColorR_SpinBox->value(), ftForceTestSignalColorG_SpinBox->value(), ftForceTestSignalColorB_SpinBox->value(), ftForceTestSignalColorA_SpinBox->value()};
    float disturbColor[4] = {ftForceTestDisturbColorR_SpinBox->value(), ftForceTestDisturbColorG_SpinBox->value(), ftForceTestDisturbColorB_SpinBox->value(), ftForceTestDisturbColorA_SpinBox->value()};
    float touchColor[4] = {ftForceTestTouchColorR_SpinBox->value(), ftForceTestTouchColorG_SpinBox->value(), ftForceTestTouchColorB_SpinBox->value(), ftForceTestTouchColorA_SpinBox->value()};
    float lineColor[4] = {ftForceTestLimitColorR_SpinBox->value(), ftForceTestLimitColorG_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value()};

    emit ftForceTestBackgroungColorChange(backGroundColor,
                                          signalColor,
                                          disturbColor,
                                          touchColor,
                                          lineColor,
                                          ftForceTestSignalTargetDimension->value(),
                                          ftForceTestDisturbTargetDimension->value(),
                                          ftForceTestTouchTargetDimension->value(),
                                          ftForceTestLimitTargetDimension->value());
}

void FindDialog::setFtForceTestSignalActiveDeative(int state_tmp)
{
    if (state_tmp == 2) ftForceTestSignalSlectionComboBox->setDisabled(false);
    else ftForceTestSignalSlectionComboBox->setDisabled(true);
}

void FindDialog::setFtForceTestDisturbActiveDeative(int state_tmp)
{
    if (state_tmp == 2) ftForceTestDisturbSlectionComboBox->setDisabled(false);
    else ftForceTestDisturbSlectionComboBox->setDisabled(true);
}

void FindDialog::ftForceTestMsgBox( double tc_tmp, double tcSignal_tmp, double tcDisturb_tmp)
{
    QMessageBox msgBox;
    msgBox.setText("SAMPLE TIME ERROR");
    msgBox.setInformativeText(QString("Sample time limit : %1 \nSample time signal: %2 \nSample time disturb: %3").arg(tc_tmp).arg(tcSignal_tmp).arg(tcDisturb_tmp));
    msgBox.setStandardButtons(QMessageBox::Ok );
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Ok:
        break;
    case QMessageBox::Discard:
        break;
    case QMessageBox::Cancel:
        break;
    default:
        break;
    }
    msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
}

void FindDialog::ftForceTestWriteSetting()
{
    float backGroundColor[4] = {ftForceTestBackgroundColorR_SpinBox->value(), ftForceTestBackgroundColorG_SpinBox->value(), ftForceTestBackgroundColorB_SpinBox->value(),ftForceTestBackgroundColorA_SpinBox->value()};
    float signalColor[4] = {ftForceTestSignalColorR_SpinBox->value(), ftForceTestSignalColorG_SpinBox->value(), ftForceTestSignalColorB_SpinBox->value(), ftForceTestSignalColorA_SpinBox->value()};
    float disturbColor[4] = {ftForceTestDisturbColorR_SpinBox->value(), ftForceTestDisturbColorG_SpinBox->value(), ftForceTestDisturbColorB_SpinBox->value(), ftForceTestDisturbColorA_SpinBox->value()};
    float touchColor[4] = {ftForceTestTouchColorR_SpinBox->value(), ftForceTestTouchColorG_SpinBox->value(), ftForceTestTouchColorB_SpinBox->value(), ftForceTestTouchColorA_SpinBox->value()};
    float lineColor[4] = {ftForceTestLimitColorR_SpinBox->value(), ftForceTestLimitColorG_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value()};

    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/ftForceTest");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "ftForceTest");
    qDebug()<< settings.fileName();
    settings.setValue("Tc", ftForceTestTcSpinBox->value());
    settings.setValue("storedSignal",ftForceTestStoredNonStoredSignal->isChecked());
    settings.setValue("storedDisturb", ftForceTestStoredNonStoredDisturb->isChecked());
    settings.setValue("durataTest", ftForceTestSpinBox->value());
    settings.setValue("colorBackgroundR",backGroundColor[0]);
    settings.setValue("colorBackgroundG",backGroundColor[1]);
    settings.setValue("colorBackgroundB",backGroundColor[2]);
    settings.setValue("colorBackgroundA",backGroundColor[3]);
    settings.setValue("signalColorR",signalColor[0]);
    settings.setValue("signalColorG",signalColor[1]);
    settings.setValue("signalColorB",signalColor[2]);
    settings.setValue("signalColorA",signalColor[3]);
    settings.setValue("disturbColorR",disturbColor[0]);
    settings.setValue("disturbColorG",disturbColor[1]);
    settings.setValue("disturbColorB",disturbColor[2]);
    settings.setValue("disturbColorA",disturbColor[3]);
    settings.setValue("touchColorR",touchColor[0]);
    settings.setValue("touchColorG",touchColor[1]);
    settings.setValue("touchColorB",touchColor[2]);
    settings.setValue("touchColorA",touchColor[3]);
    settings.setValue("lineColorR",lineColor[0]);
    settings.setValue("lineColorG",lineColor[1]);
    settings.setValue("lineColorB",lineColor[2]);
    //    settings.setValue("lineColorA",lineColor[3]);
    settings.setValue("signalTargetWidth", ftForceTestSignalTargetDimension->value());
    settings.setValue("disturbTargetWidth", ftForceTestDisturbTargetDimension->value());
    settings.setValue("touchTargetWidth", ftForceTestTouchTargetDimension->value());
    settings.setValue("limitTargetWidth", ftForceTestLimitTargetDimension->value());
    settings.setValue("disturbYN", ftForceTestDisturbYN->isChecked());
    settings.setValue("tAtteso", ftForceTestTempoSaltoSpinBox->value());
    settings.setValue("tAttesoDisturbo", ftForceTestTempoSaltoDisturboSpinBox->value());
    settings.setValue("LFPmeasuring",ftForceTestLFPmeasuring->isChecked());
}

void FindDialog::ftForceTestReadSetting()
{

    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/ftForceTest");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "ftForceTest");
    if(settings.contains("Tc"))
    {
        qDebug()<< settings.fileName();
        ftForceTestTcSpinBox->setValue(settings.value("Tc").toInt());
        ftForceTestStoredNonStoredSignal->setChecked(settings.value("storedSignal").toBool());
        ftForceTestStoredNonStoredDisturb->setChecked(settings.value("storedDisturb").toBool());
        ftForceTestSpinBox->setValue(settings.value("durataTest").toInt());
        ftForceTestBackgroundColorR_SpinBox->setValue(settings.value("colorBackgroundR").toInt());
        ftForceTestBackgroundColorG_SpinBox->setValue(settings.value("colorBackgroundG").toInt());
        ftForceTestBackgroundColorB_SpinBox->setValue(settings.value("colorBackgroundB").toInt());
        ftForceTestSignalColorR_SpinBox->setValue(settings.value("signalColorR").toInt());
        ftForceTestSignalColorG_SpinBox->setValue(settings.value("signalColorG").toInt());
        ftForceTestSignalColorB_SpinBox->setValue(settings.value("signalColorB").toInt());
        ftForceTestSignalColorA_SpinBox->setValue(settings.value("signalColorA").toInt());
        ftForceTestDisturbColorR_SpinBox->setValue(settings.value("disturbColorR").toInt());
        ftForceTestDisturbColorG_SpinBox->setValue(settings.value("disturbColorG").toInt());
        ftForceTestDisturbColorB_SpinBox->setValue(settings.value("disturbColorB").toInt());
        ftForceTestDisturbColorA_SpinBox->setValue(settings.value("disturbColorA").toInt());
        ftForceTestTouchColorR_SpinBox->setValue(settings.value("touchColorR").toInt());
        ftForceTestTouchColorG_SpinBox->setValue(settings.value("touchColorG").toInt());
        ftForceTestTouchColorB_SpinBox->setValue(settings.value("touchColorB").toInt());
        ftForceTestTouchColorA_SpinBox->setValue(settings.value("touchColorA").toInt());
        ftForceTestLimitColorR_SpinBox->setValue(settings.value("lineColorR").toInt());
        ftForceTestLimitColorG_SpinBox->setValue(settings.value("lineColorG").toInt());
        ftForceTestLimitColorB_SpinBox->setValue(settings.value("lineColorB").toInt());
        //    ftForceTestLimitColorA_SpinBox->setValue(settings.value("lineColorA").toInt());
        ftForceTestSignalTargetDimension->setValue(settings.value("signalTargetWidth").toInt());
        ftForceTestDisturbTargetDimension->setValue(settings.value("disturbTargetWidth").toInt());
        ftForceTestTouchTargetDimension->setValue(settings.value("touchTargetWidth").toInt());
        ftForceTestLimitTargetDimension->setValue(settings.value("limitTargetWidth").toInt());
        ftForceTestDisturbYN->setChecked(settings.value("disturbYN").toBool());
        ftForceTestTempoSaltoSpinBox->setValue(settings.value("tAtteso").toFloat());
        ftForceTestTempoSaltoDisturboSpinBox->setValue(settings.value("tAttesoDisturbo").toFloat());
        ftForceTestLFPmeasuring->setChecked(settings.value("LFPmeasuring").toBool());
    }
    else ftForceTestWriteSetting();
}

void FindDialog::ftForceTestDefaultSetting()
{
    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/ftForceTest");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "ftForceTest");
    qDebug()<< settings.fileName();
    settings.setValue("Tc", 16);
    settings.setValue("storedSignal",false);
    settings.setValue("storedDisturb", false);
    settings.setValue("durataTest", 150);
    settings.setValue("colorBackgroundR",99);
    settings.setValue("colorBackgroundG",99);
    settings.setValue("colorBackgroundB",99);
    settings.setValue("colorBackgroundA",99);
    settings.setValue("signalColorR",0);
    settings.setValue("signalColorG",99);
    settings.setValue("signalColorB",99);
    settings.setValue("signalColorA",50);
    settings.setValue("disturbColorR",99);
    settings.setValue("disturbColorG",0);
    settings.setValue("disturbColorB",0);
    settings.setValue("disturbColorA",50);
    settings.setValue("touchColorR",0);
    settings.setValue("touchColorG",0);
    settings.setValue("touchColorB",0);
    settings.setValue("touchColorA",99);
    settings.setValue("lineColorR",99);
    settings.setValue("lineColorG",99);
    settings.setValue("lineColorB",99);
    //    settings.setValue("lineColorA",lineColor[3]);
    settings.setValue("signalTargetWidth", 20);
    settings.setValue("disturbTargetWidth", 20);
    settings.setValue("touchTargetWidth", 2);
    settings.setValue("limitTargetWidth", 2);
    settings.setValue("disturbYN", true);
    settings.setValue("tAtteso", 1.40);
    settings.setValue("tAttesoDisturbo", 1.40);
    settings.setValue("LFPmeasuring",false);
    ftForceTestReadSetting();
}

void FindDialog::newFtForceTestClicked()
{
    int disturbYN = 0;
    int storedSignalChk = 0;
    int storedDisturbChk = 0;
    int ftForceTestLFPmeasuringChk = 0;
    if ( ftForceTestStoredNonStoredSignal->isChecked()) storedSignalChk = 1;
    if ( ftForceTestStoredNonStoredDisturb->isChecked()) storedDisturbChk = 1;
    if ( ftForceTestDisturbYN->isChecked()) disturbYN = 1;
    if ( ftForceTestLFPmeasuring->isChecked()) ftForceTestLFPmeasuringChk = 1;
    float backGroundColor[4] = {ftForceTestBackgroundColorR_SpinBox->value(), ftForceTestBackgroundColorG_SpinBox->value(), ftForceTestBackgroundColorB_SpinBox->value(),ftForceTestBackgroundColorA_SpinBox->value()};
    float signalColor[4] = {ftForceTestSignalColorR_SpinBox->value(), ftForceTestSignalColorG_SpinBox->value(), ftForceTestSignalColorB_SpinBox->value(), ftForceTestSignalColorA_SpinBox->value()};
    float disturbColor[4] = {ftForceTestDisturbColorR_SpinBox->value(), ftForceTestDisturbColorG_SpinBox->value(), ftForceTestDisturbColorB_SpinBox->value(), ftForceTestDisturbColorA_SpinBox->value()};
    float touchColor[4] = {ftForceTestTouchColorR_SpinBox->value(), ftForceTestTouchColorG_SpinBox->value(), ftForceTestTouchColorB_SpinBox->value(), ftForceTestTouchColorA_SpinBox->value()};
    float lineColor[4] = {ftForceTestLimitColorR_SpinBox->value(), ftForceTestLimitColorG_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value(), ftForceTestLimitColorB_SpinBox->value()};

    qDebug()<< fileNameOutput + "//User//"+nameLabel->text()+"//ftForceTest";

    if(!QDir().exists(fileNameOutput + "/User/"+nameLabel->text()+"/ftForceTest"))QDir().mkdir(fileNameOutput + "/User/"+nameLabel->text()+ "/ftForceTest");
    newFtForceTest = new FtForceTest((double(ftForceTestTcSpinBox->value())/1000),
                                     storedSignalChk,
                                     storedDisturbChk,
                                     ftForceTestSpinBox->value(),
                                     backGroundColor,
                                     signalColor,
                                     disturbColor,
                                     touchColor,
                                     lineColor,
                                     ftForceTestSignalTargetDimension->value(),
                                     ftForceTestDisturbTargetDimension->value(),
                                     ftForceTestTouchTargetDimension->value(),
                                     ftForceTestLimitTargetDimension->value(),
                                     disturbYN,
                                     QCoreApplication::applicationDirPath()+ "/Data/"+ftForceTestSignalSlectionComboBox->currentText(),
                                     QCoreApplication::applicationDirPath()+ "/Data/"+ftForceTestDisturbSlectionComboBox->currentText(),
                                     fileNameOutput + "/User/"+nameLabel->text()+ "/ftForceTest",
                                     ftForceTestTempoSaltoSpinBox->value(),
                                     ftForceTestTempoSaltoDisturboSpinBox->value(),
                                     ftForceTestLFPmeasuringChk);
    newFtForceTest->showFullScreen();
    //    newFtForceTest->showMaximized();
    connect(newFtForceTest, SIGNAL(tcError( double, double, double )), this, SLOT(ftForceTestMsgBox( double, double, double )) );
}

void FindDialog::createIlluminaLaCittaPage(){

    // inizializzazione della immagini da usare nella pagina del test
    QPixmap pixmapStart = QPixmap("Image/General/Play.png");
    QIcon iconStart(pixmapStart);
    QPixmap pixmapStop = QPixmap("Image/General/Stop.png");
    QIcon iconStop(pixmapStop);
    QPixmap pixmapSave = QPixmap("Image/General/Save.png");
    QIcon iconSave(pixmapSave);
    QPixmap pixmapRestore = QPixmap("Image/General/Restore.png");
    QIcon iconRestore(pixmapRestore);

    QPixmap image = QPixmap("Image/IlluminaLaCitta/initialImage.png");
    QLabel *imageLabel = new QLabel();
    imageLabel->setStyleSheet("color: blue;"
                              "height = 144px"
                              "background-color: white;"
                              );
    imageLabel->setPixmap(image);

    illuminaLaCittaPage = new QWidget;

    // inizialzzazione di dei pulsanti spinbox etc da usare nella pagina del test
    illuminaLaCittaStartButton = new QToolButton;
    illuminaLaCittaStartButton->setIcon(iconStart);
    illuminaLaCittaStartButton->setIconSize(QSize(80,80));
    illuminaLaCittaStartButton->setEnabled(true);
    illuminaLaCittaStartButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    illuminaLaCittaStartButton->setAutoRaise(true);
    illuminaLaCittaStartButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                              "border-style:none; }"
                                              "QToolButton{font:12pt;color:white}");

    illuminaLaCittaStopButton = new QToolButton;
    illuminaLaCittaStopButton->setIcon(iconStop);
    illuminaLaCittaStopButton->setIconSize(QSize(80,80));
    illuminaLaCittaStopButton->setEnabled(true);
    illuminaLaCittaStopButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    illuminaLaCittaStopButton->setAutoRaise(true);
    illuminaLaCittaStopButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                             "border-style:none; }"
                                             "QToolButton{font:12pt;color:white}");
    illuminaLaCittaStopButton->setVisible(false);

    illuminaLaCittaDefaultSettingsButton = new QToolButton;
    illuminaLaCittaDefaultSettingsButton->setIcon(iconRestore);
    illuminaLaCittaDefaultSettingsButton->setIconSize(QSize(80,80));
    illuminaLaCittaDefaultSettingsButton->setEnabled(true);
    illuminaLaCittaDefaultSettingsButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    illuminaLaCittaDefaultSettingsButton->setAutoRaise(true);
    illuminaLaCittaDefaultSettingsButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                                        "border-style:none; }"
                                                        "QToolButton{font:12pt;color:white}");

    illuminaLaCittaSaveSettingsButton = new QToolButton;
    illuminaLaCittaSaveSettingsButton->setIcon(iconSave);
    illuminaLaCittaSaveSettingsButton->setIconSize(QSize(80,80));
    illuminaLaCittaSaveSettingsButton->setEnabled(true);
    illuminaLaCittaSaveSettingsButton->setStatusTip(tr("Selezionare/Creare la cartella del paziente"));
    illuminaLaCittaSaveSettingsButton->setAutoRaise(true);
    illuminaLaCittaSaveSettingsButton->setStyleSheet("QToolButton:enabled:hover { background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #d7cfe0, stop: 1  #a7a1ae   );"
                                                     "border-style:none; }"
                                                     "QToolButton{font:12pt;color:white}");


    QLabel *illuminaLaCittaForceEnabledLabel = new QLabel(tr("Forza Attiva:"));
    illuminaLaCittaForceEnabled_ChkBox = new QCheckBox();
    illuminaLaCittaForceEnabled_ChkBox->setChecked(true);

    illuminaLaCittaFoceMantainingTime_Label = new QLabel(tr("Tempo permanneza nel target:  ( da 0 a 5000 )"));
    illuminaLaCittaFoceMantainingTime_SpinBox = new QSpinBox () ;
    illuminaLaCittaFoceMantainingTime_SpinBox->setRange(0, 5000);
    illuminaLaCittaFoceMantainingTime_SpinBox->setValue(1000);
    illuminaLaCittaFoceMantainingTime_SpinBox->setSuffix(" [ms]");
    illuminaLaCittaFoceMantainingTime_SpinBox->setStyleSheet(stileSpinBox);
    connect(illuminaLaCittaForceEnabled_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaFoceMantainingTime_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaForceEnabled_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaFoceMantainingTime_SpinBox,SLOT(setVisible(bool)));

    illuminaLaCittaUpperThreshold_Label = new QLabel(tr("Limite superiore forza:  ( da 40 a 100 )"));
    illuminaLaCittaUpperThreshold_SpinBox = new QSpinBox () ;
    illuminaLaCittaUpperThreshold_SpinBox->setRange(40, 100);
    illuminaLaCittaUpperThreshold_SpinBox->setValue(80);
    illuminaLaCittaUpperThreshold_SpinBox->setSuffix(" [%]");
    illuminaLaCittaUpperThreshold_SpinBox->setStyleSheet(stileSpinBox);
    connect(illuminaLaCittaForceEnabled_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaUpperThreshold_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaForceEnabled_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaUpperThreshold_SpinBox,SLOT(setVisible(bool)));

    illuminaLaCittaLowerThreshold_Label = new QLabel(tr("Limite inferiore forza:  ( da 20 a 100 )"));
    illuminaLaCittaLowerThreshold_SpinBox = new QSpinBox () ;
    illuminaLaCittaLowerThreshold_SpinBox->setRange(20, 100);
    illuminaLaCittaLowerThreshold_SpinBox->setValue(20);
    illuminaLaCittaLowerThreshold_SpinBox->setSuffix(" [%]");
    illuminaLaCittaLowerThreshold_SpinBox->setStyleSheet(stileSpinBox);
    connect(illuminaLaCittaForceEnabled_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaLowerThreshold_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaForceEnabled_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaLowerThreshold_SpinBox,SLOT(setVisible(bool)));

    QLabel *illuminaLaCittaTargetDimension_Label = new QLabel(tr("Dimensione del target:  ( da 2 a 50 )"));
    illuminaLaCittaTargetDimension_SpinBox = new QSpinBox () ;
    illuminaLaCittaTargetDimension_SpinBox->setRange(2, 50);
    illuminaLaCittaTargetDimension_SpinBox->setValue(20);
    illuminaLaCittaTargetDimension_SpinBox->setSuffix(" [px]");
    illuminaLaCittaTargetDimension_SpinBox->setStyleSheet(stileSpinBox);

    QLabel *illuminaLaCittaStoredNonStoredGrid_Label = new QLabel(tr("Carica griglia:"));
    illuminaLaCittaStoredNonStoredGrid_ChkBox = new QCheckBox();
    illuminaLaCittaStoredNonStoredGrid_ChkBox->setChecked(false);

    illuminaLaCittaStoredNonStoredGrid_ComboBox =  new QComboBox();
    qDebug() << QDir::currentPath();
    QDir path("IlluminaLaCitta");
    QStringList all_dirs = path.entryList(QDir::Files);
    qDebug() << all_dirs;
    illuminaLaCittaStoredNonStoredGrid_ComboBox->addItems(all_dirs );
    illuminaLaCittaStoredNonStoredGrid_ComboBox->setVisible(false);
    connect(illuminaLaCittaStoredNonStoredGrid_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaStoredNonStoredGrid_ComboBox,SLOT(setVisible(bool)));
    connect(illuminaLaCittaStoredNonStoredGrid_ChkBox,SIGNAL(clicked(bool)),this,SLOT(illuminaLaCittaUpdateTotalTargetNumberFromFile( bool )));
    connect(illuminaLaCittaStoredNonStoredGrid_ComboBox,SIGNAL(currentTextChanged(QString)),this,SLOT(illuminaLaCittaUpdateTotalTargetNumberFromFile( QString )));

    illuminaLaCittaTotalNumberTarget_Label = new QLabel(tr("Numero totale di luci ( da 2 a 41 ):"));
    illuminaLaCittaTotalNumberTarget_SpinBox = new QSpinBox () ;
    illuminaLaCittaTotalNumberTarget_SpinBox->setRange(2, 26);
    illuminaLaCittaTotalNumberTarget_SpinBox->setValue(26);
    illuminaLaCittaTotalNumberTarget_SpinBox->setSuffix(" [n°]");
    illuminaLaCittaTotalNumberTarget_SpinBox->setStyleSheet(stileSpinBox);

    QLabel *illuminaLaCittaSimmetry_Label = new QLabel(tr("Luci simmetriche:"));
    illuminaLaCittaSimmetry_ChkBox = new QCheckBox();
    illuminaLaCittaSimmetry_ChkBox->setChecked(false);

    illuminaLaCittaTargetOnLeftSide_Label = new QLabel(tr("Numero luci a sinistra:  ( da 0 a 21 )"));
    illuminaLaCittaTargetOnLeftSide_SpinBox = new QSpinBox () ;
    illuminaLaCittaTargetOnLeftSide_SpinBox->setRange(0, 13);
    illuminaLaCittaTargetOnLeftSide_SpinBox->setValue(13);
    illuminaLaCittaTargetOnLeftSide_SpinBox->setSuffix(" [n°]");
    illuminaLaCittaTargetOnLeftSide_SpinBox->setStyleSheet(stileSpinBox);
    illuminaLaCittaTargetOnLeftSide_SpinBox->setVisible(false);
    illuminaLaCittaTargetOnLeftSide_Label->setVisible(false);
    connect(illuminaLaCittaSimmetry_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaTargetOnLeftSide_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaSimmetry_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaTargetOnLeftSide_SpinBox,SLOT(setVisible(bool)));

    illuminaLaCittaTargetOnRightSide_Label = new QLabel(tr("Numero luci a destra:  ( da 0 a 21 )"));
    illuminaLaCittaTargetOnRightSide_SpinBox = new QSpinBox () ;
    illuminaLaCittaTargetOnRightSide_SpinBox->setRange(0, 13);
    illuminaLaCittaTargetOnRightSide_SpinBox->setValue(13);
    illuminaLaCittaTargetOnRightSide_SpinBox->setSuffix(" [n°]");
    illuminaLaCittaTargetOnRightSide_SpinBox->setStyleSheet(stileSpinBox);
    illuminaLaCittaTargetOnRightSide_Label->setVisible(false);
    illuminaLaCittaTargetOnRightSide_SpinBox->setVisible(false);
    connect(illuminaLaCittaSimmetry_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaTargetOnRightSide_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaSimmetry_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaTargetOnRightSide_SpinBox,SLOT(setVisible(bool)));

    connect(illuminaLaCittaTotalNumberTarget_SpinBox,SIGNAL(valueChanged(int)),this,SLOT (illuminaLaCittaUpdateLeftRightTargetNumber(int)));
    connect(illuminaLaCittaTargetOnLeftSide_SpinBox,SIGNAL(valueChanged(int)),this,SLOT (illuminaLaCittaUpdateRightTargetNumber(int)));
    connect(illuminaLaCittaTargetOnRightSide_SpinBox,SIGNAL(valueChanged(int)),this,SLOT (illuminaLaCittaUpdateLeftTargetNumber(int)));

    illuminaLaCittaTotalTime_Label = new QLabel(tr("Tempo totale:  ( da 1 a 5 )"));
    illuminaLaCittaTotalTime_SpinBox = new QSpinBox () ;
    illuminaLaCittaTotalTime_SpinBox->setRange(1, 5);
    illuminaLaCittaTotalTime_SpinBox->setValue(5);
    illuminaLaCittaTotalTime_SpinBox->setSuffix(" [min]");
    illuminaLaCittaTotalTime_SpinBox->setStyleSheet(stileSpinBox);
    illuminaLaCittaTotalTime_SpinBox->setVisible(false);
    illuminaLaCittaTotalTime_Label->setVisible(false);

    QLabel *illuminaLaCittaInfiniteTime_Chk_Label = new QLabel(tr("Test a tempo:"));
    illuminaLaCittaInfiniteTime_ChkBox = new QCheckBox();
    illuminaLaCittaInfiniteTime_ChkBox->setChecked(false);
    connect(illuminaLaCittaInfiniteTime_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaTotalTime_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaInfiniteTime_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaTotalTime_SpinBox,SLOT(setVisible(bool)));

    QLabel *illuminaLaCittaHideTimeChk_Label = new QLabel(tr("Modalità a scomparsa"));
    illuminaLaCittaHideTime_ChkBox = new QCheckBox();
    illuminaLaCittaHideTime_ChkBox->setChecked(false);

    illuminaLaCittaHideTime_Label = new QLabel(tr("Tempo di scomparsa:  ( da 2 a 5000 )"));
    illuminaLaCittaHideTime_SpinBox = new QSpinBox () ;
    illuminaLaCittaHideTime_SpinBox->setRange(2, 2500);
    illuminaLaCittaHideTime_SpinBox->setValue(1000);
    illuminaLaCittaHideTime_SpinBox->setSuffix(" [ms]");
    illuminaLaCittaHideTime_SpinBox->setStyleSheet(stileSpinBox);
    illuminaLaCittaHideTime_Label->setVisible(false);
    illuminaLaCittaHideTime_SpinBox->setVisible(false);
    connect(illuminaLaCittaHideTime_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaHideTime_Label,SLOT(setVisible(bool)));
    connect(illuminaLaCittaHideTime_ChkBox,SIGNAL(clicked(bool)),illuminaLaCittaHideTime_SpinBox,SLOT(setVisible(bool)));

    QLabel *illuminaLaCittaBackgroundColorRLabel = new QLabel(tr("R:"));
    illuminaLaCittaBackgroundColorR_SpinBox = new QSpinBox () ;
    illuminaLaCittaBackgroundColorR_SpinBox->setValue(100);
    illuminaLaCittaBackgroundColorR_SpinBox->setRange(0, 100);
    illuminaLaCittaBackgroundColorR_SpinBox->setStyleSheet(stileSpinBox);

    QLabel *illuminaLaCittaBackgroundColorGLabel = new QLabel(tr("G:"));
    illuminaLaCittaBackgroundColorG_SpinBox = new QSpinBox () ;
    illuminaLaCittaBackgroundColorG_SpinBox->setValue(100);
    illuminaLaCittaBackgroundColorG_SpinBox->setRange(0, 100);
    illuminaLaCittaBackgroundColorG_SpinBox->setStyleSheet(stileSpinBox);

    QLabel *illuminaLaCittaBackgroundColorBLabel = new QLabel(tr("B:"));
    illuminaLaCittaBackgroundColorB_SpinBox = new QSpinBox () ;
    illuminaLaCittaBackgroundColorB_SpinBox->setValue(100);
    illuminaLaCittaBackgroundColorB_SpinBox->setRange(0, 100);
    illuminaLaCittaBackgroundColorB_SpinBox->setStyleSheet(stileSpinBox);

    QLabel *illuminaLaCittaBackgroundColorALabel = new QLabel(tr("Alpha:"));
    illuminaLaCittaBackgroundColorA_SpinBox = new QSpinBox () ;
    illuminaLaCittaBackgroundColorA_SpinBox->setValue(100);
    illuminaLaCittaBackgroundColorA_SpinBox->setRange(0, 100);
    illuminaLaCittaBackgroundColorA_SpinBox->setStyleSheet(stileSpinBox);

    QLabel *modalitaLabel = new QLabel(tr("Tipo di luce:"));
    illuminaLaCittaModalitaComboBox = new QComboBox();
    QStringList modalitaList;
    modalitaList << "LETTERE" << "NUMERI" << "ENTRAMBI";
    illuminaLaCittaModalitaComboBox->addItems( modalitaList );
    connect(illuminaLaCittaModalitaComboBox,SIGNAL(currentTextChanged(QString)),this,SLOT(illuminaLaCittaUpdateTotalTargetNumber( QString)));

    QGridLayout *illuminaLaCittaColorLayout = new QGridLayout;
    QGroupBox *illuminaLaCittaColorGroupBox = new QGroupBox();

    QGroupBox *illuminaLaCittaBackgroundColorGroupBox = new QGroupBox();
    QGridLayout *illuminaLaCittaBackgroundColorLayout = new QGridLayout;
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorRLabel,0,0);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorR_SpinBox,0,1);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorGLabel,1,0);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorG_SpinBox,1,1);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorBLabel,2,0);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorB_SpinBox,2,1);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorALabel,3,0);
    illuminaLaCittaBackgroundColorLayout->addWidget(illuminaLaCittaBackgroundColorA_SpinBox,3,1);

    illuminaLaCittaBackgroundColorGroupBox->setTitle("Background color");
    illuminaLaCittaBackgroundColorGroupBox->setLayout(illuminaLaCittaBackgroundColorLayout);

    // raccogliere le opzioni simili in gruop box
    QGroupBox *illuminaLaCittaOptionGroupBox = new QGroupBox();
    QGridLayout *illuminaLaCittaOptionLayout = new QGridLayout;
    illuminaLaCittaOptionGroupBox->setStyleSheet(stileGroupBox);
    illuminaLaCittaOptionGroupBox->setTitle("OPZIONI");
    illuminaLaCittaOptionGroupBox->setLayout(illuminaLaCittaOptionLayout);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaForceEnabledLabel,0,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaForceEnabled_ChkBox,0,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaFoceMantainingTime_Label,1,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaFoceMantainingTime_SpinBox,1,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaUpperThreshold_Label,2,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaUpperThreshold_SpinBox,2,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaLowerThreshold_Label,3,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaLowerThreshold_SpinBox,3,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTargetDimension_Label,4,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTargetDimension_SpinBox,4,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaStoredNonStoredGrid_Label,5,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaStoredNonStoredGrid_ChkBox,5,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaStoredNonStoredGrid_ComboBox,6,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTotalNumberTarget_Label,7,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTotalNumberTarget_SpinBox,7,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaSimmetry_Label,8,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaSimmetry_ChkBox,8,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTargetOnLeftSide_Label,9,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTargetOnLeftSide_SpinBox,9,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTargetOnRightSide_Label,10,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTargetOnRightSide_SpinBox,10,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaInfiniteTime_Chk_Label,11,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaInfiniteTime_ChkBox,11,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTotalTime_Label,12,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaTotalTime_SpinBox,12,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaHideTimeChk_Label,13,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaHideTime_ChkBox,13,1);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaHideTime_Label,14,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaHideTime_SpinBox,14,1);
    illuminaLaCittaOptionLayout->addWidget(modalitaLabel,15,0);
    illuminaLaCittaOptionLayout->addWidget(illuminaLaCittaModalitaComboBox,15,1);

    illuminaLaCittaColorLayout->addWidget(illuminaLaCittaBackgroundColorGroupBox,0,0);
    illuminaLaCittaBackgroundColorGroupBox->setStyleSheet("QGroupBox {font-size: 11px;} ");
    illuminaLaCittaColorGroupBox->setStyleSheet(stileGroupBox);
    illuminaLaCittaColorGroupBox->setTitle("COLOR SET-UP");
    illuminaLaCittaColorGroupBox->setLayout(illuminaLaCittaColorLayout);

    // inserire in un una griglia i gruopbox creati
    QGridLayout *optionLayout = new QGridLayout;
    optionLayout->setColumnStretch(0, 1);
    optionLayout->setColumnStretch(0, 1);
    optionLayout->addWidget(illuminaLaCittaOptionGroupBox,0,0);
//    optionLayout->addWidget(illuminaLaCittaColorGroupBox,0,1);


    // layout in cui si insrisce l'immagine del test
    QVBoxLayout *illuminaLaCittaTestImageLayout = new QVBoxLayout;
    //    illuminaLaCittaTestImageLayout->addWidget(imageLabel,0,Qt::AlignCenter);
    illuminaLaCittaTestImageLayout->setContentsMargins(100,0,100,0);
    // layout dei pulsanti di start stop e report
    QHBoxLayout *illuminaLaCittaLayoutH = new QHBoxLayout;
    illuminaLaCittaLayoutH->addWidget(illuminaLaCittaStartButton,0,Qt::AlignBottom);
    illuminaLaCittaLayoutH->addWidget(illuminaLaCittaStopButton,0,Qt::AlignBottom);
    illuminaLaCittaLayoutH->addWidget(illuminaLaCittaDefaultSettingsButton,0,Qt::AlignBottom);
    illuminaLaCittaLayoutH->addWidget(illuminaLaCittaSaveSettingsButton,0,Qt::AlignBottom);

    // layout generale della pagina del test
    QVBoxLayout *illuminaLaCittaLayout = new QVBoxLayout;
    illuminaLaCittaLayout->addLayout(illuminaLaCittaTestImageLayout);
    illuminaLaCittaLayout->addLayout(optionLayout);
    illuminaLaCittaLayout->addLayout(illuminaLaCittaLayoutH);

    illuminaLaCittaPage->setLayout(illuminaLaCittaLayout);

    connect(illuminaLaCittaStartButton, SIGNAL(clicked()), this, SLOT(illuminaLaCittaWriteSetting()));
    connect(illuminaLaCittaDefaultSettingsButton, SIGNAL(clicked()), this, SLOT(illuminaLaCittaDefaultSetting()));
    connect(illuminaLaCittaSaveSettingsButton, SIGNAL(clicked()), this, SLOT(illuminaLaCittaWriteSetting()));
    connect(illuminaLaCittaStartButton, SIGNAL(clicked()),
            this, SLOT(newIlluminaLaCittaClicked()));
}

void FindDialog::illuminaLaCittaUpdateLeftRightTargetNumber( int newValue_tmp )
{
    illuminaLaCittaTargetOnLeftSide_SpinBox->setValue( newValue_tmp/2 );
    illuminaLaCittaTargetOnRightSide_SpinBox->setValue( newValue_tmp -  newValue_tmp/2 );
    if ( newValue_tmp > 21 )
    {
        illuminaLaCittaTargetOnLeftSide_SpinBox->setRange(newValue_tmp-21,21);
        illuminaLaCittaTargetOnRightSide_SpinBox->setRange(newValue_tmp-21,21);
        illuminaLaCittaTargetOnLeftSide_Label->setText(tr("Numero luci a sinistra: ( da %1 a %2 )").arg(newValue_tmp-21).arg(21));
        illuminaLaCittaTargetOnRightSide_Label->setText(tr("Numero luci a destra:  ( da %1 a %2 )").arg(newValue_tmp-21).arg(21));
    }
    else
    {
    illuminaLaCittaTargetOnLeftSide_SpinBox->setRange(0,newValue_tmp);
    illuminaLaCittaTargetOnRightSide_SpinBox->setRange(0,newValue_tmp);
    illuminaLaCittaTargetOnLeftSide_Label->setText(tr("Numero luci a sinistra: ( da %1 a %2 )").arg(0).arg(newValue_tmp));
    illuminaLaCittaTargetOnRightSide_Label->setText(tr("Numero luci a destra:  ( da %1 a %2 )").arg(0).arg(newValue_tmp));
    }
    illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a %1 ):").arg(newValue_tmp));
}

void FindDialog::illuminaLaCittaUpdateTotalTargetNumberFromFile( bool gridSelectionState_tmp )
{
    if (gridSelectionState_tmp)
    {
        QFile file(QCoreApplication::applicationDirPath()+ "/IlluminaLaCitta/"+illuminaLaCittaStoredNonStoredGrid_ComboBox->currentText());
        if (!file.open(QIODevice::ReadOnly ))
        {
            QMessageBox msgBox;
            msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
            msgBox.setInformativeText("");
            msgBox.setStandardButtons(QMessageBox::Ok );
            msgBox.setDefaultButton(QMessageBox::Ok);
        }
        QTextStream in(&file);
        QString line = in.readLine();
        int totalLineNumber = 0;
        while (!line.isNull()) {
            totalLineNumber++;
            line = in.readLine();
        }
        qDebug() << "total line " << totalLineNumber;
        file.close();
        if (totalLineNumber > 26) illuminaLaCittaModalitaComboBox->setCurrentIndex(2);
        illuminaLaCittaTotalNumberTarget_SpinBox->setValue(totalLineNumber);

    }
    else if (illuminaLaCittaModalitaComboBox->currentText() == "LETTERE")
    {
        illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a 26 ):"));
        if ( illuminaLaCittaTotalNumberTarget_SpinBox->value() > 26 )
        {
            illuminaLaCittaTotalNumberTarget_SpinBox->setValue(26);
        }
        illuminaLaCittaTotalNumberTarget_SpinBox->setRange(0,26);
    }
    else
    {
        illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a 41 ):"));
        illuminaLaCittaTotalNumberTarget_SpinBox->setRange(0,41);
    }
}

void FindDialog::illuminaLaCittaUpdateTotalTargetNumberFromFile( QString fileNameSelected_tmp )
{
        QFile file(QCoreApplication::applicationDirPath()+ "/IlluminaLaCitta/"+fileNameSelected_tmp);
        if (!file.open(QIODevice::ReadOnly ))
        {
            QMessageBox msgBox;
            msgBox.setText(QString("non e possibile aprire il file %1").arg(file.fileName()));
            msgBox.setInformativeText("");
            msgBox.setStandardButtons(QMessageBox::Ok );
            msgBox.setDefaultButton(QMessageBox::Ok);
        }
        QTextStream in(&file);
        QString line = in.readLine();
        int totalLineNumber = 0;
        while (!line.isNull()) {
            totalLineNumber++;
            line = in.readLine();
        }
        qDebug() << "total line da file" << totalLineNumber;
        file.close();

        illuminaLaCittaTotalNumberTarget_SpinBox->setValue(totalLineNumber);

        if (illuminaLaCittaModalitaComboBox->currentText() == "LETTERE")
    {
        if ( illuminaLaCittaTotalNumberTarget_SpinBox->value() >= 26 )
        {
            illuminaLaCittaTotalNumberTarget_SpinBox->setValue(26);
            illuminaLaCittaTotalNumberTarget_SpinBox->setRange(0,26);
            illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a 26 ):"));
            qDebug() << "setRange";
        }
        else
        {
            illuminaLaCittaTotalNumberTarget_SpinBox->setRange(0,totalLineNumber);
            illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a %1 ):").arg(totalLineNumber));
        }

    }
        else
        {
            illuminaLaCittaTotalNumberTarget_SpinBox->setRange(0,totalLineNumber);
            illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a %1 ):").arg(totalLineNumber));
        }

}

void FindDialog::illuminaLaCittaUpdateRightTargetNumber( int newValue_tmp )
{
    illuminaLaCittaTargetOnRightSide_SpinBox->setValue( illuminaLaCittaTotalNumberTarget_SpinBox->value() - newValue_tmp );
}

void FindDialog::illuminaLaCittaUpdateLeftTargetNumber( int newValue_tmp )
{
    illuminaLaCittaTargetOnLeftSide_SpinBox->setValue( illuminaLaCittaTotalNumberTarget_SpinBox->value() - newValue_tmp );
}

void FindDialog::illuminaLaCittaUpdateTotalTargetNumber( QString newValue_tmp )
{
//    if (newValue_tmp == "LETTERE" )
//    {

//        if ( illuminaLaCittaTotalNumberTarget_SpinBox->value() > 26 )
//        {
//            illuminaLaCittaTotalNumberTarget_SpinBox->setRange(0,26);
//            illuminaLaCittaTotalNumberTarget_SpinBox->setValue(26);
//            illuminaLaCittaTotalNumberTarget_Label->setText(tr("Numero totale di luci ( da 2 a 26 ):"));
//    }
//    }
    illuminaLaCittaUpdateTotalTargetNumberFromFile( illuminaLaCittaStoredNonStoredGrid_ComboBox->currentText() );
}

void FindDialog::illuminaLaCittaWriteSetting()
{
    float backGroundColor[4] = {illuminaLaCittaBackgroundColorR_SpinBox->value(), illuminaLaCittaBackgroundColorG_SpinBox->value(), illuminaLaCittaBackgroundColorB_SpinBox->value(),illuminaLaCittaBackgroundColorA_SpinBox->value()};

    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/illuminaLaCitta");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "illuminaLaCitta");
    qDebug()<< settings.fileName();
    settings.setValue("forceModality",illuminaLaCittaForceEnabled_ChkBox->isChecked());
    settings.setValue("forceMantainingTime", illuminaLaCittaFoceMantainingTime_SpinBox->value());
    settings.setValue("upperThreshold", illuminaLaCittaUpperThreshold_SpinBox->value());
    settings.setValue("lowerThreshold", illuminaLaCittaLowerThreshold_SpinBox->value());
    settings.setValue("targetDimension", illuminaLaCittaTargetDimension_SpinBox->value());
    settings.setValue("storedGrid",illuminaLaCittaStoredNonStoredGrid_ChkBox->isChecked());
    settings.setValue("fileName", illuminaLaCittaStoredNonStoredGrid_ComboBox->currentText());
    settings.setValue("totalNumberTarget", illuminaLaCittaTotalNumberTarget_SpinBox->value());
    settings.setValue("simmetry",illuminaLaCittaSimmetry_ChkBox->isChecked());
    settings.setValue("leftNumberTarget", illuminaLaCittaTargetOnLeftSide_SpinBox->value());
    settings.setValue("rightNumberTarget", illuminaLaCittaTargetOnRightSide_SpinBox->value());
    settings.setValue("hideTimeBool",illuminaLaCittaHideTime_ChkBox->isChecked() );
    settings.setValue("hideTime", illuminaLaCittaHideTime_SpinBox->value());
    settings.setValue("colorBackgroundR",backGroundColor[0]);
    settings.setValue("colorBackgroundG",backGroundColor[1]);
    settings.setValue("colorBackgroundB",backGroundColor[2]);
    settings.setValue("colorBackgroundA",backGroundColor[3]);
    settings.setValue("modalita",illuminaLaCittaModalitaComboBox->currentIndex());
    settings.setValue("totalTime",illuminaLaCittaTotalTime_SpinBox->value());
    settings.setValue("tempoFinito",illuminaLaCittaInfiniteTime_ChkBox->isChecked());
        qDebug()<<"usere settings written";
}

void FindDialog::illuminaLaCittaReadSetting()
{

    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/illuminaLaCitta");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "illuminaLaCitta");
    if(settings.contains("forceModality"))
    {
        qDebug()<< settings.fileName();
        illuminaLaCittaForceEnabled_ChkBox->setChecked(settings.value("forceModality").toBool());
        illuminaLaCittaFoceMantainingTime_SpinBox->setValue(settings.value("forceMantainingTime").toInt());
        illuminaLaCittaFoceMantainingTime_SpinBox->setVisible(illuminaLaCittaForceEnabled_ChkBox->isChecked());
        illuminaLaCittaFoceMantainingTime_Label->setVisible(illuminaLaCittaForceEnabled_ChkBox->isChecked());
        illuminaLaCittaUpperThreshold_SpinBox->setValue(settings.value("upperThreshold").toInt());
        illuminaLaCittaUpperThreshold_SpinBox->setVisible(illuminaLaCittaForceEnabled_ChkBox->isChecked());
        illuminaLaCittaUpperThreshold_Label->setVisible(illuminaLaCittaForceEnabled_ChkBox->isChecked());
        illuminaLaCittaLowerThreshold_SpinBox->setValue(settings.value("lowerThreshold").toInt());
        illuminaLaCittaLowerThreshold_SpinBox->setVisible(illuminaLaCittaForceEnabled_ChkBox->isChecked());
        illuminaLaCittaLowerThreshold_Label->setVisible(illuminaLaCittaForceEnabled_ChkBox->isChecked());
        illuminaLaCittaTargetDimension_SpinBox->setValue(settings.value("targetDimension").toInt());
        illuminaLaCittaStoredNonStoredGrid_ChkBox->setChecked(settings.value("storedGrid").toBool());
        illuminaLaCittaStoredNonStoredGrid_ComboBox->insertItem(0,settings.value("fileName").toString(),Qt::DisplayRole);
        illuminaLaCittaStoredNonStoredGrid_ComboBox->setVisible(illuminaLaCittaStoredNonStoredGrid_ChkBox->isChecked());
        illuminaLaCittaTotalNumberTarget_SpinBox->setValue(settings.value("totalNumberTarget").toInt());
        illuminaLaCittaSimmetry_ChkBox->setChecked(settings.value("simmetry").toBool());
        illuminaLaCittaTargetOnLeftSide_SpinBox->setValue(settings.value("leftNumberTarget").toInt());
        illuminaLaCittaTargetOnLeftSide_SpinBox->setVisible(illuminaLaCittaSimmetry_ChkBox->isChecked());
        illuminaLaCittaTargetOnLeftSide_Label->setVisible(illuminaLaCittaSimmetry_ChkBox->isChecked());
        illuminaLaCittaTargetOnRightSide_SpinBox->setValue(settings.value("rightNumberTarget").toInt());
        illuminaLaCittaTargetOnRightSide_SpinBox->setVisible(illuminaLaCittaSimmetry_ChkBox->isChecked());
        illuminaLaCittaTargetOnRightSide_Label->setVisible(illuminaLaCittaSimmetry_ChkBox->isChecked());
        illuminaLaCittaHideTime_ChkBox->setChecked(settings.value("hideTimeBool").toBool());
        illuminaLaCittaHideTime_SpinBox->setValue(settings.value("hideTime").toInt());
        illuminaLaCittaHideTime_SpinBox->setVisible(illuminaLaCittaHideTime_ChkBox->isChecked());
        illuminaLaCittaHideTime_Label->setVisible(illuminaLaCittaHideTime_ChkBox->isChecked());
        illuminaLaCittaBackgroundColorR_SpinBox->setValue(settings.value("colorBackgroundR").toInt());
        illuminaLaCittaBackgroundColorG_SpinBox->setValue(settings.value("colorBackgroundG").toInt());
        illuminaLaCittaBackgroundColorB_SpinBox->setValue(settings.value("colorBackgroundB").toInt());
        illuminaLaCittaBackgroundColorA_SpinBox->setValue(settings.value("colorBackgroundA").toInt());
        illuminaLaCittaModalitaComboBox->setCurrentIndex(settings.value("modalita").toInt());
        illuminaLaCittaTotalTime_SpinBox->setValue(settings.value("totalTime").toInt());
        illuminaLaCittaInfiniteTime_ChkBox->setChecked(settings.value("tempoFinito").toBool());
        illuminaLaCittaTotalTime_SpinBox->setVisible(illuminaLaCittaInfiniteTime_ChkBox->isChecked());
        illuminaLaCittaTotalTime_Label->setVisible(illuminaLaCittaInfiniteTime_ChkBox->isChecked());
        qDebug()<<"usere settings readen";
    }
    else illuminaLaCittaWriteSetting();
}

void FindDialog::illuminaLaCittaDefaultSetting()
{
    QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,fileNameOutput + "/User/"+nameLabel->text()+ "/illuminaLaCitta");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                       "UserSettings", "illuminaLaCitta");
    qDebug()<< settings.fileName();
    settings.setValue("forceModality",true);
    settings.setValue("forceMantainingTime", 1000);
    settings.setValue("upperThreshold", 80);
    settings.setValue("lowerThreshold", 0);
    settings.setValue("targetDimension", 20);
    settings.setValue("storedGrid",true);
    settings.setValue("fileName", "base.txt");
    settings.setValue("totalNumberTarget", 10);
    settings.setValue("simmetry", false);
    settings.setValue("leftNumberTarget", 5);
    settings.setValue("rightNumberTarget", 5);
    settings.setValue("hideTimeBool",false);
    settings.setValue("hideTime", 1000);
    settings.setValue("colorBackgroundR",99);
    settings.setValue("colorBackgroundG",99);
    settings.setValue("colorBackgroundB",99);
    settings.setValue("colorBackgroundA",99);
    settings.setValue("modalita",0);
    settings.setValue("totalTime",5);
    settings.setValue("tempoFinito",false);
        qDebug()<<"usere settings defalut written";
    illuminaLaCittaReadSetting();
}

void FindDialog::newIlluminaLaCittaClicked()
{
    float backGroundColor[4] = {illuminaLaCittaBackgroundColorR_SpinBox->value(), illuminaLaCittaBackgroundColorG_SpinBox->value(), illuminaLaCittaBackgroundColorB_SpinBox->value(),illuminaLaCittaBackgroundColorA_SpinBox->value()};

    qDebug()<< fileNameOutput + "//User//"+nameLabel->text()+"//illuminaLaCitta";

    if(!QDir().exists(fileNameOutput + "/User/"+nameLabel->text()+"/illuminaLaCitta"))QDir().mkdir(fileNameOutput + "/User/"+nameLabel->text()+ "/illuminaLaCitta");
    newIlluminaLaCittaTest = new IlluminaLaCitta(illuminaLaCittaForceEnabled_ChkBox->isChecked(),
                                                 illuminaLaCittaTargetDimension_SpinBox->value(),
                                                 illuminaLaCittaStoredNonStoredGrid_ChkBox->isChecked(),
                                                 QCoreApplication::applicationDirPath()+ "/IlluminaLaCitta/"+illuminaLaCittaStoredNonStoredGrid_ComboBox->currentText(),
                                                 illuminaLaCittaTotalNumberTarget_SpinBox->value(),
                                                 illuminaLaCittaTotalTime_SpinBox->value(),
                                                 illuminaLaCittaHideTime_ChkBox->isChecked(),
                                                 illuminaLaCittaHideTime_SpinBox->value(),
                                                 backGroundColor,
                                                 illuminaLaCittaModalitaComboBox->currentIndex(),
                                                 fileNameOutput + "/User/"+nameLabel->text()+ "/illuminaLaCitta",
                                                 illuminaLaCittaInfiniteTime_ChkBox->isChecked(),
                                                 illuminaLaCittaTargetOnLeftSide_SpinBox->value(),
                                                 illuminaLaCittaUpperThreshold_SpinBox->value(),
                                                 illuminaLaCittaLowerThreshold_SpinBox->value(),
                                                 illuminaLaCittaFoceMantainingTime_SpinBox->value(),
                                                 illuminaLaCittaSimmetry_ChkBox->isChecked());


     window = new Window();
     //window->setForceRemainingtime(illuminaLaCittaFoceMantainingTime_SpinBox->value());
     //window->setForceUpperThresholdLabel(illuminaLaCittaUpperThreshold_SpinBox->value());
     //window->setForceLowerThresholdLabel(illuminaLaCittaLowerThreshold_SpinBox->value());

     window->setWindowLayout(newIlluminaLaCittaTest);
     window->showMaximized();

     window->showFullScreen();
     QObject::connect(this, SIGNAL(touchRead(int,int,double)), newIlluminaLaCittaTest, SLOT(setTouchPos(int,int,double)));
     //QObject::connect(window, SIGNAL(disconnectTouch()), this, SLOT(disconnectIlluminaLaCitta()));
     //QObject::connect(newIlluminaLaCittaTest, SIGNAL(pressureRealChange(double,bool)), window, SLOT(setForceBar(double,bool)));
     //QObject::connect(window, SIGNAL(forceMantainingTimeOk_sng()), newIlluminaLaCittaTest, SLOT(setActualId()));
     QObject::connect(newIlluminaLaCittaTest, SIGNAL(endTest(QString )), this, SLOT(setBarChart(QString )));
     QObject::connect(this, SIGNAL(setBarChartSignal(BarChart*, BarChart*,BarChart*)), window, SLOT(setBarChart(BarChart*,BarChart*, BarChart*)));
     QObject::connect(newIlluminaLaCittaTest, SIGNAL(closeTest_signal()), window, SLOT(close()));


//exit(109872);
   // newIlluminaLaCittaTest->showFullScreen();
    //    newIlluminaLaCittaTest->showMaximized();
}

void FindDialog::disconnectIlluminaLaCitta()
{
    QObject::disconnect(this, SIGNAL(touchRead(int,int,double)), newIlluminaLaCittaTest, SLOT(setTouchPos(int,int,double)));
    qDebug() << "disconnected";
}

bool FindDialog::activateButton()
{
    lineStartButton->setEnabled(true);
    lineStartButton->setVisible(true);
    lineStopButton->setVisible(false);
    ftForceTestStartButton->setEnabled(true);
    ftForceTestStartButton->setVisible(true);
    ftForceTestStopButton->setVisible(false);
    return 0;
}

bool FindDialog::deactivateButton()
{
    lineStartButton->setEnabled(false);
    lineStartButton->setVisible(false);
    lineStopButton->setVisible(true);
    ftForceTestStartButton->setEnabled(false);
    ftForceTestStartButton->setVisible(false);
    ftForceTestStopButton->setVisible(true);

    return 0;
}

void FindDialog::open()
{
    int pathLength = QDir::currentPath().length()+1;
    fileNameOutput = QFileDialog::getExistingDirectory(this);
    setWindowTitle(fileNameOutput);
    QString newUserLabel = fileNameOutput.right(fileNameOutput.length()-pathLength);
    fileNameOutput.remove(fileNameOutput.right(fileNameOutput.length()-pathLength));
    updateUserLabel( newUserLabel);
    userLabel->setText(QDir::currentPath().right(QDir::currentPath().length()-pathLength));
    printf("lenghezza %i --- %i \n", pathLength, QDir::currentPath().length());
}

void FindDialog::createFolder()
{
    NewUserFolder *newFolder = new NewUserFolder();
    newFolder->show();
    //    connect(newFolder,SIGNAL(closeFolder(QString)),this,SLOT(updateUserLabel(QString)));
    connect(newFolder,SIGNAL(updateFolder(QString)),this,SLOT(updateOnlyUserLabel(QString)));
    newUserButton->hide();
    connect(newFolder,SIGNAL(closeFolder()),this,SLOT(showNewUserButton()));
    //    connect(newFolder,SIGNAL(updateFolder ( QString )),this,SLOT(updateUserLabel( QString )));
    //    fileNameOutput = QFileDialog::getExistingDirectory(this);
    //    setWindowTitle(fileNameOutput);
}

void FindDialog::showNewUserButton()
{
    newUserButton->show();
}

void FindDialog::updateUserLabel( QString newPath)
{
    if ( QDir::setCurrent( "/User/" + newPath) == true )
    {
        nameLabel->setText(newPath);
        m_pTableWidget->setRowCount(m_pTableWidget->rowCount()+1);
        m_pTableWidget->setItem( m_pTableWidget->rowCount()-1, 0, new QTableWidgetItem(newPath));
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText(QString("La directory di salvataggi non � stata aggiornata. La directory corrente � %1").arg(QDir::currentPath()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }
}

void FindDialog::updateOnlyUserLabel( QString newPath)
{
    nameLabel->setText(newPath);
    qDebug() << "row" << m_pTableWidget->rowCount();
    //    if (m_pTableWidget->rowCount() < 3 ) m_pTableWidget->setItem( m_pTableWidget->rowCount()-2, 0, new QTableWidgetItem(newPath));
    m_pTableWidget->setItem( m_pTableWidget->rowCount()-1, 0, new QTableWidgetItem(newPath));
    m_pTableWidget->setRowCount(m_pTableWidget->rowCount()+1);
    lineDefaultSetting();
    ftForceTestDefaultSetting();
    illuminaLaCittaDefaultSetting();
}

void FindDialog::updateUserSelected( int row, int column )
{
    QDir::setCurrent(QCoreApplication::applicationDirPath());
    if ( QDir::setCurrent(  "User/" + m_pTableWidget->item(row, column )->text()) == true && !m_pTableWidget->item(row, column )->text().isEmpty() )
    {
        nameLabel->setText(m_pTableWidget->item(row, column )->text());
        lineReadSetting();
        ftForceTestReadSetting();
        illuminaLaCittaReadSetting();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText(QString("La directory di salvataggi non � stata aggiornata. La directory corrente � %1").arg(QDir::currentPath()));
        msgBox.setInformativeText("");
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
    }

}

void FindDialog::readData()
{
    static int ii = 0;
    if ( serial->canReadLine())
    {
        QByteArray data = serial->readLine();
        //        qDebug() << "data" << data;
        if ( ii < 200)
        {
            if ( data.contains("|") && data.contains("<") )
            {
                char buff [ 5 ];                // buffer dove memorizzo il valore esadecimale in formato ASCII di ax che uso per calcolare il CRC lato PC e che poi confronto con quello restituito dall'arduino
                data.remove(0,1);
                data.remove(data.length()-3,3);
                QList<QByteArray> lista;
                lista = data.split('\t');
                if ( lista.length() == 7 )
                {
                    itoa ( lista[0].toInt() + lista[1].toInt(), buff, 16);
                    if( crcFast ( buff, strlen(buff)) != lista[6].toInt() )
                    {
                        touch.f1 = lista[2].toFloat();
                        touch.f2 = lista[3].toFloat();
                        touch.f3 = lista[4].toFloat();
//                        qDebug()<<"ii : =" << ii << "  " << touch.f1 << "  " << touch.f2 << "  " <<touch.f3  ;

                        touch.tara_1 += touch.f1;
                        touch.tara_2 += touch.f2;
                        touch.tara_3 += touch.f3;
                        ii++;
//                        qDebug() << "tara1 : " << touch.tara_1<< "tara2 : " << touch.tara_2<< "tara3 : " << touch.tara_3;
//                                    system("PAUSE");
                    }
                }

            }
        }
        else if ( ii == 200 )
        {
            touch.tara_1 = touch.tara_1/(ii);
            touch.tara_2 = touch.tara_2/(ii);
            touch.tara_3 = touch.tara_3/(ii);
            qDebug() << "tara1 : " << touch.tara_1<< "tara2 : " << touch.tara_2<< "tara3 : " << touch.tara_3;
//            system("PAUSE");
            ii++;
        }
        else
        {
            if ( data.contains("|") && data.contains("<") )
            {
                char buff [ 5 ];                // buffer dove memorizzo il valore esadecimale in formato ASCII di ax che uso per calcolare il CRC lato PC e che poi confronto con quello restituito dall'arduino
                data.remove(0,1);
                data.remove(data.length()-3,3);
                QList<QByteArray> lista;
                lista = data.split('\t');
                if ( lista.length() == 7 )
                {
                    itoa ( lista[0].toInt() + lista[1].toInt(), buff, 16);
                    if( crcFast ( buff, strlen(buff)) != lista[6].toInt() )
                    {

                        touch.x = (lista[0].toFloat()*A_x+B_x);
                        if ( touch.x > 200 && touch.x <= 600 ) touch.x = touch.x +20;
                        else if ( touch.x > 600 && touch.x <= 800) touch.x = touch.x +10;
                        else if ( touch.x > 800) touch.x = touch.x;
                        else touch.x = touch.x + 40 ;
//                        qDebug() << A_x << " " << B_x;
                        touch.y = (lista[1].toFloat()*A_y+B_y);
                        touch.f1 = lista[2].toFloat();
                        touch.f2 = lista[3].toFloat();
                        touch.f3 = lista[4].toFloat();
                        touchLabel->move(QPoint(touch.x, touch.y));
                        touch.fTot = ((A_f + B_f * touch.x + C_f * touch.y + D_f * touch.x*touch.x + E_f *touch.x*touch.y + F_f * touch.y*touch.y + G_f * (touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) + H_f * (touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) * touch.x + I_f * (touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) * touch.y + J_f *(touch.f3-touch.tara_3+touch.f2-touch.tara_2+touch.f1-touch.tara_1) * touch.y * touch.x)  )  / 18 ;
                        emit touchRead(touch.x,touch.y,touch.fTot);
//                        qDebug() << touch.f1 << "  " << touch.f2 << "  " <<touch.f3 << "  \n" ;
                    }
                }
            }
        }
    }
}
//! [7]

//! [8]
void FindDialog::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}
//! [8]

void FindDialog::closeSerialPort()
{
    serial->close();
}

void FindDialog::openSerialPort()
{
    if ( ( stream_Coeff_touch = fopen("TaraturaTocco/coefficenti.txt","r" ) ) == NULL ) exit(100);//QMessageBox::warning(this, tr("Warning"),tr("File dei coefficenti del touch non trovato."));
        while ( !feof ( stream_Coeff_touch ) )
        {
            char str [100] ;
            fgets ( str, 100, stream_Coeff_touch) ;
            sscanf ( str, "%f\t%f\t%f\t%f\n", &A_x, &B_x, &A_y, &B_y ) ;
            qDebug() << A_x << " " << B_x;
        }

        if ( ( stream_Coeff_forza = fopen ( "TaraturaForze/coefficienti.txt", "r" ) ) == NULL ) exit(101);//QMessageBox::warning(this, tr("Warning"),tr("File dei coefficenti delle celle di carico non trovato."));
        while ( !feof ( stream_Coeff_forza ) )
        {
            char str [1000] ;
            fgets ( str, 1000, stream_Coeff_forza) ;
            sscanf ( str, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", &A_f, &B_f, &C_f, &D_f, &E_f, &F_f, &G_f, &H_f, &I_f, &J_f ) ;
        }
   serial->setPortName("COM4");
    if (serial->open(QIODevice::ReadOnly)) {
        if (serial->setBaudRate(QSerialPort::Baud9600)
                && serial->setDataBits(QSerialPort::Data8)
                && serial->setParity(QSerialPort::NoParity)
                && serial->setStopBits(QSerialPort::TwoStop)
                && serial->setFlowControl(QSerialPort::NoFlowControl)) {
        touchLabel = new QLabel(this);
        touchLabel->setStyleSheet("background-color:red;");
        touchLabel->move(QPoint(100,100));
        touch.tara_1 = 0;
        touch.tara_2 = 0;
        touch.tara_3 = 0;
        } else {
            serial->close();
            QMessageBox::critical(this, tr("Error"), serial->errorString());
        }
    } else {
        QMessageBox::critical(this, tr("Error"), serial->errorString());
    }
}
//! [4]

void FindDialog::setBarChart( QString fileName_tmp )
{
    newIlluminaLaCittaTest->hide();

    barChart = new BarChart( this, fileName_tmp,5,"Tempo di selezione","Target","Tempo [ms]");
    barChart1 = new BarChart( this,fileName_tmp,6,"Errori","Target","n° Errori");

    barChart2 = new BarChart( this,fileName_tmp,0,7 ,"Completamento test","Target vs Totali","n° Target");

    emit setBarChartSignal( barChart, barChart1, barChart2 );
}

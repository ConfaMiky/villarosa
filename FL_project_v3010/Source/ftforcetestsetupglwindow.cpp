#include "ftforcetestsetupglwindow.h"
#include <QKeyEvent>
#include <QDebug>
#include <QDateTime>

// Constructor
FtForceTestSetupGlWindow::FtForceTestSetupGlWindow( float* colorBackground_tmp,
                                      float* signalColor_tmp,
                                      float* disturbColor_tmp,
                                      float* touchColor_tmp,
                                      float* limitColor_tmp,
                                      int signalWidth_tmp,
                                      int disturbWidth_tmp,
                                      int touchWidth_tmp,
                                      int limitWidth_tmp)
{
    for (  int ii = 0; ii <= 3; ii++ )
    {
        backgroundColor[ii] = float(colorBackground_tmp[ii])/100;
        signalColor[ii] = float(signalColor_tmp[ii])/100;
        disturbColor[ii] = float(disturbColor_tmp[ii])/100;
        touchColor[ii] = float(touchColor_tmp[ii])/100;
        limitColor[ii] = float(limitColor_tmp[ii])/100;
    }
//    backgroundColor[3] = 1;
//    signalColor[3]= 0.5;
//    disturbColor[3] = 1;
//    touchColor[3] = 1;
//    limitColor[3] = 1;
    signalWidth = signalWidth_tmp;
    disturbWidth = disturbWidth_tmp;
    touchWidth = touchWidth_tmp;
    limitWidth = limitWidth_tmp;
    setWindowTitle("HTF");
}

// Empty destructor
FtForceTestSetupGlWindow::~FtForceTestSetupGlWindow() {

}

// Initialize OpenGL
void FtForceTestSetupGlWindow::initializeGL() {

    glShadeModel(GL_SMOOTH); // Enable smooth shading
    glClearColor(1,0,1,0); // Set the clear color to a black background

    glClearDepth(1.0f); // Depth buffer setup
    glEnable(GL_DEPTH_TEST); // Enable depth LineSetupGlWindowing
    glDepthFunc(GL_LEQUAL); // Set type of depth LineSetupGlWindow
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);			// Enable Alpha Blending (disable alpha LineSetupGlWindowing)
    glEnable(GL_BLEND);											// Enable Blending       (disable alpha LineSetupGlWindowing)
    glEnable(GL_LIGHTING);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really nice perspective calculations

    glEnable(GL_CULL_FACE);										// Remove Back Face
}

// This is called when the OpenGL window is resized
void FtForceTestSetupGlWindow::resizeGL(int width, int height) {
    // Prevent divide by zero (in the gluPerspective call)
    if (height == 0)
        height = 1;

    glViewport(0, 0, width, height); // Reset current viewport

    glMatrixMode(GL_PROJECTION); // Select projection matrix
    glLoadIdentity(); // Reset projection matrix

    //      gluPerspective(45.0f, static_cast<GLfloat>(width)/height, 0.1f, 100.0f); // Calculate aspect ratio
    glOrtho(0,static_cast<GLfloat>(width),0,static_cast<GLfloat>(height),-1,100);				// Set Up An Ortho Screen

    glMatrixMode(GL_MODELVIEW); // Select modelview matrix
    glLoadIdentity(); // Reset modelview matrix
    GLint	viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
}

// OpenGL painting code goes here
void FtForceTestSetupGlWindow::paintGL() {
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear Screen And Depth Buffer

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);			// Enable Alpha Blending (disable alpha LineSetupGlWindowing)
    glEnable(GL_BLEND);											// Enable Blending       (disable alpha LineSetupGlWindowing)

    glClearColor(backgroundColor[0],backgroundColor[1],backgroundColor[2],backgroundColor[3]);
    GLint	viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glLoadIdentity();											// Reset The Modelview Matrix


//    glDisable(GL_DEPTH_TEST);
//    glDisable(GL_CULL_FACE);

    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glColor4fv( signalColor );
    glPushMatrix();
    glTranslatef(viewport[2]/4,viewport[3]/2,0);   //carico la posizione
    Object(viewport[2]/2 -10, viewport[2]/2 -10, 0, viewport[3], signalWidth, viewport[3], 0);
    glPopMatrix();
    glPushMatrix();
    glColor4fv( disturbColor );
    glTranslatef(viewport[2]/2,viewport[3]/2,0);   //carico la posizione
    Object(viewport[2]/2 -10, viewport[2]/2 -10, 0, viewport[3], disturbWidth, viewport[3], 0);
    glPopMatrix();
    glColor4fv( touchColor );
    glPushMatrix();
    glTranslatef(viewport[2] - viewport[2]/4,viewport[3]/2,0);   //carico la posizione
    Object(viewport[2]/2 -10, viewport[2]/2 -10, 0, viewport[3], touchWidth, viewport[3], 0);
    glPopMatrix();
    glPushMatrix();
//    glColor4fv( limitColor );
//    glTranslatef(viewport[2] - viewport[2]/4,viewport[3]/2,0);   //carico la posizione
//    Object(viewport[2]/2 -10, viewport[2]/2 -10, 0, viewport[3], limitWidth, viewport[3], 0);
//    glPopMatrix();

}

void FtForceTestSetupGlWindow::updateColorWidth( float* colorBackground_tmp,
                                          float* signalColor_tmp,
                                          float* disturbColor_tmp,
                                          float* touchColor_tmp,
                                          float* limitColor_tmp,
                                          int signalWidth_tmp,
                                          int disturbWidth_tmp,
                                          int touchWidth_tmp,
                                          int limitWidth_tmp)
{
    for (  int ii = 0; ii <= 3; ii++ )
    {
        backgroundColor[ii] = float(colorBackground_tmp[ii])/100;
        signalColor[ii] = float(signalColor_tmp[ii])/100;
        disturbColor[ii] = float(disturbColor_tmp[ii])/100;
        touchColor[ii] = float(touchColor_tmp[ii])/100;
        limitColor[ii] = float(limitColor_tmp[ii])/100;
    }
//    backgroundColor[3] = 0;
//    signalColor[3]= 0.5;
//    disturbColor[3] = 1;
//    touchColor[3] = 1;
//    limitColor[3] = 1;
    signalWidth = signalWidth_tmp;
    disturbWidth = disturbWidth_tmp;
    touchWidth = touchWidth_tmp;
    limitWidth = limitWidth_tmp;

    updateGL();
    //    exit(900);
}

void FtForceTestSetupGlWindow::Object(float x0_texture,float x1_texture,float y0_texture,float y1_texture,float width,float height,GLuint texid)				// Draw Object Using Requested Width, Height And Texture
{
    glBegin(GL_QUADS);											// Start Drawing A Quad
    glVertex3f(-float(width)/2,-float(height)/2,0.0f);	// Bottom Left
    glVertex3f( float(width)/2,-float(height)/2,0.0f);	// Bottom Right
    glVertex3f( float(width)/2, float(height)/2,0.0f);	// Top Right
    glVertex3f(-float(width)/2, float(height)/2,0.0f);	// Top Left
    glEnd();													// Done Drawing Quad
}

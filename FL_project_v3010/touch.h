#ifndef TOUCH_H
#define TOUCH_H
struct Touch
{
    float x;
    float y;
    float f1;
    float f2;
    float f3;
    int mDito;
    int ID_Arduino;
    float tara_1;
    float tara_2;
    float tara_3;
    float fTot;
    float fTotPrev;
};
#endif // TOUCH_H

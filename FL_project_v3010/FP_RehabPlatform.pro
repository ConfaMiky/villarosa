#-------------------------------------------------
#
# Project created by QtCreator 2014-01-18T11:47:08
#
#-------------------------------------------------

QT       += core gui multimedia serialport
QT += opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FP_RehabPlatform
CONFIG   += console
CONFIG += qwt

TEMPLATE = app

INCLUDEPATH += Header

    soundFiles.sources = resources\sounds\*.wav
    DEPLOYMENT += soundFiles

SOURCES += main.cpp \
    Source/target.cpp \
    Source/newuserfoder.cpp \
    Source/finddialog.cpp \
    Source/line.cpp \
    Source/linesetupglwindow.cpp \
    Source/ftforcetest.cpp \
    Source/ftforcetestsetupglwindow.cpp \
    Source/illuminalacitta.cpp \
    Source/shapedclock.cpp \
    Source/window.cpp \
    Source/crc.cpp \
    Source/barchart.cpp \
    Source/forcebar.cpp

HEADERS += \
    Header/textureImage.h \
    Header/target.h \
    Header/newuserfoder.h \
    Header/finddialog.h \
    Header/line.h \
    Header/linesetupglwindow.h \
    Header/ftforcetest.h \
    Header/ftforcetestsetupglwindow.h \
    Header/illuminalacitta.h \
    Header/shapedclock.h \
    Header/window.h \
    Header/crc.h \
    Header/barchart.h \
    touch.h \
    Header/forcebar.h

RESOURCES += \
    Sound.qrc


FORMS +=

#include <QCoreApplication>

#include "Header/finddialog.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName("Trolltech");
    app.setApplicationName("NoTremor PRJ");
    qDebug()<<QDir::currentPath();

    // Check for OpenGL support
    if (!QGLFormat::hasOpenGL()) {
        qDebug("Your system does not seem to support OpenGL. Cannot run this example.");
        //system("PAUSE");
        return EXIT_FAILURE;
    }
    QDir::setCurrent(QCoreApplication::applicationDirPath());
    FindDialog *dialog = new FindDialog;
    dialog->setStyleSheet("background-color: #f0f0f0 ");
    dialog->showMaximized();
//    dialog->show();

    return app.exec();
}


